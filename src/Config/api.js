import axios from 'axios';
import BASE_API_URL from './config';

// export const postApi = (apiUrl, data) => {
//   const promise = new Promise((resolve, reject) => {
//     axios
//       .post(BASE_API_URL + apiUrl, data)
//       .then(response => {
//         // debugger
//         resolve(response);
//       })
//       .catch(err => {
//         // debugger
//         reject(err);
//       });
//   });

//   return promise;
// };

export const postApi = (apiUrl, data, headers) => {
  const promise = new Promise((resolve, reject) => {
    axios({
      method: 'post',
      params: data,
      url: apiUrl,
      headers,
    })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
  return promise;
}

export const getApi = (apiUrl, data, headers) => {
  const promise = new Promise((resolve, reject) => {
    axios({
      method: 'get',
      params: data,
      url: apiUrl,
      headers,
    })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
  return promise;
}

export const getApiWithToken = (apiUrl, data, headers) => {
  let token = 'Token ' + localStorage.getItem('userToken')
  console.log('Token ' + localStorage.getItem('userToken'));
  const promise = new Promise((resolve, reject) => {
    axios({
      method: 'get',
      params: data,
      url: apiUrl,
      headers: {
        "Authorization":token,
        "Content-Type":"application/json",
      }
    })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
  return promise;
}

export const postApiCallWithToken = (url, method, data) => {
  let token = 'Token ' + localStorage.getItem('userToken')
  console.log('data: ', data);
  console.log('Token ' + localStorage.getItem('userToken'));
  const promise = new Promise((resolve, reject) => {
    axios({
        url: url,
        method: method,
        data: data,
        headers: {
          "Authorization":token,
          "Content-Type":"application/json",
        }
      })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });

  return promise;
}

export const putApiCallWithToken = (url, method, data) => {
  let token = 'Token ' + localStorage.getItem('userToken')
  console.log('data: ', data);
  console.log('Token ' + localStorage.getItem('userToken'));
  const promise = new Promise((resolve, reject) => {
    axios({
        url: url,
        method: method,
        data: data,
        headers: {
          "Authorization":token,
          "Content-Type":"application/json",
        }
      })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });

  return promise;
}

export const apiCallWithToken = (url, method, data) => {
  const promise = new Promise((resolve, reject) => {
    axios({
        url: url,
        method: method,
        data: data,
        headers: {
          "Content-Type":"application/json",
        }
      })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });

  return promise;
}

//fetching cms details

export const getCMSDetails = (apiUrl) => {
  const promise = new Promise((resolve, reject) => {
    axios
      .get(BASE_API_URL + apiUrl)
      .then(response => {
        // debugger
        resolve(response);
      })
      .catch(err => {
        // debugger
        reject(err);
      });
  });

  return promise;
};

//getting feature details for home page

export const getFeatureDetailsForHomePage = (apiUrl) => {
  const promise = new Promise((resolve , reject) => {
    axios
      .get(BASE_API_URL + apiUrl)
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
  return promise;
}

export const gotoReserving = (apiUrl , payLoad) => {
  
  const promise = new Promise((resolve, reject) => {
    axios({
        url: BASE_API_URL + apiUrl,
        method: "post",
        data: payLoad,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(response => {        
        resolve(response);
      })
      .catch(err => {        
        reject(err);
      });
  });

  return promise;
}

export const gotoSendQueries = (apiUrl , payLoad) => {
  
  const promise = new Promise((resolve, reject) => {
    axios({
        url: BASE_API_URL + apiUrl,
        method: "post",
        data: payLoad,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(response => {        
        resolve(response);
      })
      .catch(err => {        
        reject(err);
      });
  });

  return promise;
}

export const getBookingList = (apiUrl , userId) => {
  const promise = new Promise((resolve, reject) => {
    axios({
        url: `${BASE_API_URL}${apiUrl}/${userId}`,
        method: "get",
        headers: {
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(response => {        
        resolve(response);
      })
      .catch(err => {        
        reject(err);
      });
  });
  return promise;
}


export const getBookingDetails = (apiUrl , bookingId) => {
  const promise = new Promise((resolve, reject) => {
    axios({
        url: `${BASE_API_URL}${apiUrl}/${bookingId}`,
        method: "get",
        headers: {
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(response => {        
        resolve(response);
      })
      .catch(err => {        
        reject(err);
      });
  });
  return promise;
}
