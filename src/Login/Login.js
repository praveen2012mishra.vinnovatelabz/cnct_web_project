import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Input, FormGroup, Label, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faLock, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import {ImagePath} from './../ImagePath';
import validator from 'validator';
import {apiCallWithToken} from '../Config/api';
import BASE_API_URL from '../Config/config';
import Popup from '../Dashboard/PopUp'

const Login = () =>{

  const [loginDetails, setLoginDetails] = useState({
    email:"",
    password:"",
    latitude: '',
    longitude: ''
  });

  const handleLoginDetails = (e) => {
    setLoginDetails({
      ...loginDetails,
      [e.target.name]: e.target.value
    });
  }

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(function(position) {
      loginDetails.latitude = position.coords.latitude;
      loginDetails.longitude = position.coords.longitude;
      console.log("Latitude is :", position.coords.latitude);
      console.log("Longitude is :", position.coords.longitude);
    });
}, [])

const submitPressed = () => {

  if (loginDetails.email === '') {
    alert("Please enter mailid")
  }else if (!validator.isEmail(loginDetails.email)) {
    alert("Please enter valid mailid")
  }else if (loginDetails.password === '') {
    alert("Please enter password")
  }else {
    console.log('loginDetails:  ', loginDetails);
    //http://111.93.169.90:2030/web/login/

    apiCallWithToken(BASE_API_URL+ 'web/login/', 
    'post',
    loginDetails)
    .then(response => {
      console.log("login response: ",response.data.user);
      localStorage.setItem('userToken', response.data.token);
      localStorage.setItem('userId', response.data.user['id']);
      localStorage.setItem('userDetails', JSON.stringify(response.data.user));
      window.location.assign('/');
    })
    .catch(err => {
      console.log(err);
      alert("Some error occured.")
    });
  }
}

  return(
    <React.Fragment>
      <section className="inner-banner position-relative">
        <img src={ImagePath.contactBanner} alt="slider" className="w-100" />
        <div className="caption">
            <Container>
              <div className="text-center">
                <h1 className="font-weight-exlight">Welcome <span className="text-green">Back</span></h1>
                <p className="m-auto">Find out what's been happening in your value chain</p>
                <p className="mx-auto mt-3">SIGN IN</p>
              </div>
            </Container>
        </div>
      </section>

      <section className="common-padd contact-us position-relative">
        <Container>
          <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
            <h2 className=" text-green">Login Deatils</h2>
            <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
          </div>
        </Container>
      </section>

      <Container className="mb-5">
        <Row>
          <Col sm="6">
            <FormGroup>
              <Label>Email</Label>
              <div className="form-input">
                <Input type="email" className="form-control" placeholder="ex: info@gmail.com" name='email' onChange={handleLoginDetails} value={loginDetails.email}/>
                <span className="icon-wr"><FontAwesomeIcon icon={faEnvelope} /></span>
              </div>
            </FormGroup>
          </Col>
          <Col sm="6">
            <FormGroup>
              <Label>Password</Label>
              <div className="form-input">
                <Input type="password" className="form-control" placeholder="****" name='password' onChange={handleLoginDetails} value={loginDetails.password}/>
                <span className="icon-wr"><FontAwesomeIcon icon={faLock} /></span>
              </div>
            </FormGroup>
          </Col>
        </Row>
        <div className="py-4">
          <Button className="btn-dark d-flex align-items-center" type="submit" onClick={submitPressed}>Login <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 w30loader  rotating" /></Button>
        </div>
      </Container>

    </React.Fragment>
  )
}

export default Login;