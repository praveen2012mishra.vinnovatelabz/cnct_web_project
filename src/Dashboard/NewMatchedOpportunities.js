import React, { useCallback, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import {
    Col,
    Container,
    Row,   
} from 'reactstrap';

import DashboardSideBar from './DashboardSideBar';

import RightSideBar from '../Component/Rightsidebar/rightSidebar';
import GreenRightSideBar from '../Component/Rightsidebar/rightbarGreen';
import Cform from '../Component/Centerpart/cform';
import Cform2 from '../Component/Centerpart/cform2';
import Cform3 from '../Component/Centerpart/cform3';
import Greenform from '../Component/Centerpart/Greenform';
import { ImagePath } from '../ImagePath';
import './style.css';


const NewMatchedOpportunities = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');

    const toggle = () => setIsOpen(!isOpen);

    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        } 
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={9} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">
                                        <Greenform />
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col lg={3}>
                            <div className="right-sidebar">
                                <GreenRightSideBar />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default NewMatchedOpportunities;