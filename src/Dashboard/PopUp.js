import React, { useState, useEffect } from 'react';
import './style.css';
import {
  Col,
  Container,
  Row,
  Button,
  Label,
  FormGroup,
  Input,
  Card,
  CardBody,
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faCheck, faSortNumericDownAlt, faTimesCircle, faTimes, faBookOpen, faPoundSign, faUser } from '@fortawesome/free-solid-svg-icons';
import { ImagePath } from '../ImagePath';

const Popup = (props) => {
  const [capabilityType, setText] = useState('')
  const [peopleCapabilityData, setPeopleCapabilityData] = useState({})
  const [machineCapabilityData, setMachineCapabilityData] = useState({})

  useEffect(() => {
    setText(props.capType)
    setPeopleCapabilityData(props.selectedPeopleData)
    setMachineCapabilityData(props.selectedMachineData)
  }, [])

  return (
    <div className='popup'>
      <div className='popup_inner p-4'>
        <button className="popupclose" onClick={props.closePopup}><FontAwesomeIcon icon={faTimes} /></button>
        {capabilityType === 'People' ?
          <div>
            <div className="border-bottom mb-3">
              <h2 className="text-green">People <span class="font-weight-exlight">Capabilities</span></h2>
            </div>

            <div className="d-flex mb-3">
              <Label className="form-label text-nowrap p-0">How are you interested to work? :</Label>
              <Label className="ml-5"><span className="tags green-bg">{peopleCapabilityData.relocation} </span></Label>
            </div>
            {peopleCapabilityData.radius !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Radius :</Label>
                <Label className="ml-5"><span className="tags green-bg">{peopleCapabilityData.radius} </span></Label>
              </div> :
              <div></div>
            }
            <div className="d-flex mb-3">
              <Label className="form-label text-nowrap p-0">Skill :</Label>
              <Label className="ml-5"><span className="tags green-bg">{peopleCapabilityData.skill}</span></Label>
            </div>
            {peopleCapabilityData.specialismName !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Specialism :</Label>
                <Label className="ml-5"><span className="tags green-bg">{peopleCapabilityData.specialismName}</span></Label>
              </div> :
              <div></div>
            }
            {peopleCapabilityData.softwareNames !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Design Software :</Label>
                <Label className="ml-5"><span className="tags green-bg">{peopleCapabilityData.softwareNames}</span></Label>
              </div> :
              <div></div>
            }

          </div> :
          <div>
            <div className="border-bottom mb-3">
              <h2 className="text-green">Machine <span class="font-weight-exlight">Capabilities</span></h2>
            </div>
            {machineCapabilityData.machine_capability_type !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Select :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.machine_capability_type}</span></Label>
              </div> :
              <div></div>
            }

            <div className="d-flex mb-3">
              <Label className="form-label text-nowrap p-0">Hardware ClassNameification :</Label>
              <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.selectedClassificationName}</span></Label>
            </div>
            {'x' in machineCapabilityData && machineCapabilityData.x !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">X :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.x}</span></Label>
              </div> : <div></div>
            }
            {'y' in machineCapabilityData && machineCapabilityData.y !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Y :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.y}</span></Label>
              </div> : <div></div>
            }
            {'z' in machineCapabilityData && machineCapabilityData.z !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Z :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.z}</span></Label>
              </div> : <div></div>
            }
            {'max_component_diameter' in machineCapabilityData && machineCapabilityData.max_component_diameter !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Max Component Diameter :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.max_component_diameter}</span></Label>
              </div> : <div></div>
            }
            {'max_component_length' in machineCapabilityData && machineCapabilityData.max_component_length !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Max Component Length :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.max_component_length}</span></Label>
              </div> : <div></div>
            }
            {'Gauge' in machineCapabilityData && machineCapabilityData.Gauge !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Gauge :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.Gauge}</span></Label>
              </div> : <div></div>
            }
            {'manufacturer' in machineCapabilityData && machineCapabilityData.manufacturer !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Manufacturer :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.manufacturer_name}</span></Label>
              </div> : <div></div>
            }
            {'model_description' in machineCapabilityData && machineCapabilityData.model_description !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Model :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.model_description}</span></Label>
              </div> : <div></div>
            }
            {'material_capability' in machineCapabilityData && machineCapabilityData.material_capability !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Material Suitability :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.material_capability_name}</span></Label>
              </div> : <div></div>
            }
            {'controller_list' in machineCapabilityData && machineCapabilityData.controller_list !== '' ?
              <div className="d-flex mb-3">
                <Label className="form-label text-nowrap p-0">Controller :</Label>
                <Label className="ml-5"><span className="tags green-bg">{machineCapabilityData.controller_name}</span></Label>
              </div> : <div></div>
            }
          </div>
        }
        <div className="py-4 d-flex justify-content-end">
          <Button variant="btn btn-dark" type="submit" onClick={props.closeAndSubmitPopUp}>Continue <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating" /></Button>
        </div>
      </div>
    </div>
  );
}

export default Popup;