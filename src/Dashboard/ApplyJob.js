import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock, faUser, faBookOpen, faCalendar, faCaretRight, faPoundSign, } from '@fortawesome/free-solid-svg-icons';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,
    Card,
    CardBody,
} from 'reactstrap';
import DashboardSideBar from './DashboardSideBar';
import { ImagePath } from '../ImagePath';
import './style.css';
import BASE_API_URL from '../Config/config';
import { getApiWithToken, postApiCallWithToken } from '../Config/api';
import moment from 'moment'
import { withRouter } from "react-router-dom";


const ApplyJob = (props) => {

    const [jobDetails, getJobDetails] = useState({});
    const [specialism, getSpecialism] = useState([]);
    const [softwareList, getSoftwareList] = useState([]);
    const [machineCapabilities, getMachineCapabilities] = useState({});
    const [isLoading, setLoading] = useState(false);
    const [newJobDetails, setJobDetails] = useState({ 
        job_title:"",
        job_summary:"",
        budget:""
    });

    useEffect(() => {
        //alert(props);
         console.log('Job details: ', props.match.params.Id);
        get_jobDetails(props.match.params.Id);
    }, [])

    const get_jobDetails = (jobId) => {
        let userid = localStorage.getItem('userId');
        //http://111.93.169.90:2030/user/17/job/details/6/
        getApiWithToken(BASE_API_URL + 'user/' + userid + '/job/details/' + jobId + '/')
            .then(response => {
                console.log('job response: ', response.data.job_details);
                setJobDetails(response.data.job_details);
                getJobDetails(response.data.job_details)
                getSpecialism(response.data.spcl_skill_list)
                getSoftwareList(response.data.software_list)
                getMachineCapabilities(response.data.capabilities)
            })
            .catch(err => {
                console.log(err);
            });
    }

    const buttonUpdatePressed = () => {
        if (newJobDetails.job_title === '') {
            alert('Please enter job title')
        }else if (newJobDetails.job_summary === '') {
            alert('Please enter job summary')
        }else if (newJobDetails.budget === '') {
            alert('Please enter budget')
        }else {
            let userid = localStorage.getItem('userId');
            let jobId = props.match.params.Id;
            //http://111.93.169.90:2030/user/[user_id]/job/edit/[job_id]/
            postApiCallWithToken(BASE_API_URL + 'user/' + userid + '/job/edit/' + jobId + '/', 
              'post',
              newJobDetails)
              .then(response => {
                console.log(response);
                alert(response.data.msg);
                setLoading(!isLoading);
                get_jobDetails(props.match.params.Id);
              })
              .catch(err => {
              console.log(err);
              })
        }
        
    }

    const formatInput = (e) => {
        // Prevent characters that are not numbers ("e", ".", "+" & "-") ✨
        let checkIfNum;
        if (e.key !== undefined) {
          // Check if it's a "e", ".", "+" or "-"
          checkIfNum = e.key === "e" || e.key === "." || e.key === "+" || e.key === "-" ;
        }
        else if (e.keyCode !== undefined) {
          // Check if it's a "e" (69), "." (190), "+" (187) or "-" (189)
          checkIfNum = e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 187 || e.keyCode === 189;
        }
        return checkIfNum && e.preventDefault();
      }

    const buttonEditPressed = (e) => {
        setLoading(!isLoading);
    }
    const setCommonFieldData = (e) => {
        setJobDetails({
            ...newJobDetails,
            [e.target.name]: e.target.value
          });
    }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className="sideNavOpen">
                                    <div className="centercontent">

                                        <Container fluid>
                                            <div className="filter-green gray-title">
                                                {/* <h6 className="mb-0">{jobDetails.job_title}</h6> */}
                                                {isLoading ?
                                                    <h6 className="mb-0 w-50"><input type="text" readOnly={false} name='job_title'  placeholder="Evoque Door Tooling Project" className="readonlyInput px-2" onChange={setCommonFieldData} /> </h6> :
                                                    <h6 className="mb-0 w-50"><input type="text" readOnly={true} value={jobDetails.job_title} placeholder="Evoque Door Tooling Project" className="readonlyInput px-2" /> </h6>
                                                }
                                                <div>
                                                <Button className="red-bg" color="w-auto rounded py-0" onClick={buttonEditPressed}>Edit</Button>
                                                {isLoading?
                                                    <Button className="red-bg ml-2" color="w-auto rounded py-0" onClick={buttonUpdatePressed}>Update</Button>
                                                    : null
                                                }
                                                
                                                </div>
                                                
                                            </div>
                                            <Row>
                                            <Card className="w-100">
                                            <CardBody className="redformpart">
                                                <div className="mb-3">
                                                    <Label className="form-label text-nowrap p-0">Job <span className="font-weight-exlight">Summary</span></Label>
                                                    {isLoading ?
                                                        <textarea readOnly className="readonlyInput" readOnly={false} name='job_summary' onChange={setCommonFieldData}></textarea> :
                                                        <textarea readOnly className="readonlyInput" readOnly={true} name='job_summary' value={jobDetails.job_summary} ></textarea>
                                                    }
                                                    
                                                </div>
                                                <div className="d-flex align-items-center">
                                                    <Label className="form-label text-nowrap p-0">Budget</Label>
                                                    {isLoading ?
                                                        <Label><span className="tags red-bg d-flex align-items-center ml-4"><FontAwesomeIcon icon={faPoundSign} className="mr-2" /> <input type="number" readOnly={false} name='budget' className="readonlyInput budgetFild pl-2" placeholder="Amount" onChange={setCommonFieldData} onKeyDown={formatInput}/></span></Label> :
                                                        <Label><span className="tags red-bg d-flex align-items-center ml-4"><FontAwesomeIcon icon={faPoundSign} className="mr-2" /> <input type="number" readOnly={true} className="readonlyInput budgetFild pl-2" value={jobDetails.budget} placeholder="258.25" /></span></Label>
                                                    }
                                                    
                                                </div>
                                            </CardBody>
                                            </Card>
                                        </Row>
                                            <div className="subcon">
                                                <div className="d-md-flex d-lg-flex align-items-center justify-content-between py-3 px-2 title border-bottom mb-4">
                                                    <div className="d-flex">
                                                        <span className="text-red"><FontAwesomeIcon icon={faClock} /></span>
                                                        <span className="d-block ml-1"><strong>Posted </strong>{moment(jobDetails.created_on).format("DD-MM-yyyy, hh:mm a")}</span>
                                                    </div>
                                                    <div className="btn-tags mr-3">
                                                        <span className="tags red-bg">Flexible</span>
                                                    </div>
                                                </div>

                                                <Card>
                                                    <CardBody className="redformpart">
                                                        <div className="d-md-flex mb-3">
                                                            <div className="d-flex mr-3 mb-3 mb-md-0">
                                                                <span className="text-red mr-2"><FontAwesomeIcon icon={faBookOpen} /></span>
                                                                {jobDetails.job_type === 'T' ?
                                                                    <span className="text-black">Time</span> :
                                                                    <span className="text-black">Project</span>
                                                                }


                                                            </div>
                                                            {jobDetails.job_type === 'T' ?
                                                                <Label><span className="tags red-bg ml-md -5 ml-lg-5"><strong>Delivery Date : </strong>{moment(jobDetails.end_date).format("DD-MM-yyyy, hh:mm a")}</span></Label> :
                                                                <Label><span className="tags red-bg ml-md -5 ml-lg-5"><strong>Delivery Date : </strong>{moment(jobDetails.completion_date).format("DD-MM-yyyy, hh:mm a")}</span></Label>
                                                            }

                                                        </div>
                                                        <div className="d-md-flex mb-3">
                                                            <div className="d-flex mr-3 mb-3 mb-md-0">
                                                                <span className="text-red mr-2"><FontAwesomeIcon icon={faUser} /></span>
                                                                <span className="text-black">Toolmaker Ltd</span>
                                                            </div>
                                                        </div>
                                                        {/* <div className="mb-3">
                                                            <Label className="form-label text-nowrap p-0">Job <span className="font-weight-exlight">Summary</span></Label>
                                                            <Label>{jobDetails.job_summary}</Label>
                                                        </div> */}
                                                        {jobDetails.is_flexible !== 'R' ?
                                                            <div className="d-flex mb-3">
                                                                <Label className="form-label text-nowrap p-0">Location :</Label>
                                                                {jobDetails.location === 'UK' ?
                                                                    <Label className="ml-5">Uk <span className="tags red-bg">{jobDetails.postcode}</span></Label> :
                                                                    <Label className="ml-5">{jobDetails.location}</Label>
                                                                }

                                                            </div> : null
                                                        }

                                                        <div className="d-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Industry :</Label>
                                                            <Label className="ml-5">{jobDetails.industry}</Label>
                                                        </div>

                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Certificates :</Label>
                                                            <div className="btn-tags mr-3 ml-md-5 ml-lg-5">
                                                                <span className="tags red-bg">Certificate 1</span>
                                                                <span className="tags ">Required</span>
                                                            </div>
                                                            <div className="btn-tags mr-3">
                                                                <span className="tags red-bg">Certificate 2</span>
                                                                <span className="tags ">Preferred</span>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Preferred Payment Method :</Label>
                                                            <div className="btn-tags mr-3 ml-md-5 ml-lg-5">
                                                                {jobDetails.preferred_payment_method === 'I' ?
                                                                    <span className="tags red-bg">Instant CNCT</span> :
                                                                    <span className="tags red-bg">Special Terms</span>
                                                                }

                                                                <span className="tags ">Download <img src={ImagePath.loaderred} alt="loader" className="img-fluid ml-2 rotating" /></span>
                                                            </div>
                                                        </div>w3
                                                <div className="d-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">NDA :</Label>
                                                            {jobDetails.is_nda === true ?
                                                                <Label className="ml-5">Yes</Label> :
                                                                <Label className="ml-5">No</Label>
                                                            }
                                                        </div>
                                                        {/* <div className="d-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Budget</Label>
                                                            <Label><span className="tags red-bg ml-4"><FontAwesomeIcon icon={faPoundSign} />{jobDetails.budget}</span></Label>
                                                        </div> */}
                                                    </CardBody>
                                                </Card>
                                                <Card>
                                                    <CardBody className="redformpart">
                                                        <div className="d-flex justify-content-between border-bottom  mb-4">
                                                            <Label className="form-label p-0 text-nowrap">Capability</Label>
                                                            <div><img src={ImagePath.Redcircle} alt="loader" className="img-fluid ml-2 rotating" /></div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Capability Type :</Label>
                                                            <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">{jobDetails.capabilities_type}</span> <FontAwesomeIcon icon={faCaretRight} /> <span className="tags red-bg">{jobDetails.skill_name}</span></Label>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Resource must be located in :</Label>
                                                            {jobDetails.is_flexible === 'R' ?
                                                                <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">Remote</span></Label> :
                                                                jobDetails.is_flexible === 'O' ?
                                                                    <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">On-Site</span></Label> :
                                                                    <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">Flexible</span></Label>
                                                            }
                                                        </div>
                                                        {jobDetails.capabilities_type === 'People' ?
                                                            <div>
                                                                <div className="d-md-flex d-lg-flex mb-3">
                                                                    <Label className="form-label text-nowrap p-0">Specialism :</Label>
                                                                    <Label className="ml-md-5 ml-lg-5">
                                                                        {specialism != null ?
                                                                            specialism.map((speciality, index) => (
                                                                                <span className="tags red-bg mb-2 mb-md-0 mb-lg-0 mr-0 mr-md-2 mr-lg-2" key={index}>{speciality.specialism_name}</span>
                                                                            )) : null
                                                                        }

                                                                    </Label>
                                                                </div>
                                                                <div className="d-md-flex d-lg-flex mb-3">
                                                                    <Label className="form-label text-nowrap p-0">Design Software:</Label>
                                                                    {softwareList != null ?
                                                                        softwareList.map((software, index) => (
                                                                            <div className="btn-tags mr-3 ml-md-5 ml-lg-5">
                                                                                <span className="tags red-bg" key={index}>{software.software_name}</span>
                                                                                {software.is_required === true ?
                                                                                    <span className="tags ">Required</span> :
                                                                                    <span className="tags ">Preferred</span>
                                                                                }
                                                                            </div>
                                                                        )) : null
                                                                    }
                                                                </div>
                                                            </div> :
                                                            <div>
                                                                {machineCapabilities.x !== '' || machineCapabilities.x !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">x: </Label>
                                                                        <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">{machineCapabilities.x}</span></Label>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.y !== '' || machineCapabilities.y !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Y :</Label>
                                                                        <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">{machineCapabilities.y}</span></Label>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.z !== '' || machineCapabilities.z !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Z :</Label>
                                                                        <Label className="ml-md-5 ml-lg-5">
                                                                            <span className="tags red-bg mb-2 mb-md-0 mb-lg-0 mr-0 mr-md-2 mr-lg-2">{machineCapabilities.z}</span>
                                                                        </Label>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.gauge !== '' || machineCapabilities.gauge !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">gauge</Label>
                                                                        <div className="btn-tags mr-3 ml-md-5 ml-lg-5">
                                                                            <span className="tags red-bg">{machineCapabilities.gauge}</span>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.max_component_length !== '' || machineCapabilities.max_component_length !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Max Component Length</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.max_component_length}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.max_component_diameter !== '' || machineCapabilities.max_component_diameter !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Max Component Dmeter</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.max_component_diameter}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.max_drill_depth !== '' || machineCapabilities.max_drill_depth !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Max Drill Depth</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.max_drill_depth}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.material_capability !== '' || machineCapabilities.material_capability !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Material Capability</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.material_capability}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.quantity !== '' || machineCapabilities.quantity !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Quantity</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.quantity}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.quality !== '' || machineCapabilities.quality !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Quality</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.quality}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.stock_material !== '' || machineCapabilities.stock_material !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Stock Material</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.stock_material}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }
                                                                {machineCapabilities.additional_process !== '' || machineCapabilities.additional_process !== null ?
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Additional Process</Label>
                                                                        <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                            <Label>{machineCapabilities.additional_process}</Label>
                                                                        </div>
                                                                    </div> : null
                                                                }

                                                                {/* <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Added on</Label>
                                                                        <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">2020-08-04T12:48:33.051339Z</span></Label>
                                                                    </div>
                                                                    <div className="d-md-flex d-lg-flex mb-3">
                                                                        <Label className="form-label text-nowrap p-0">Color Classification</Label>
                                                                        <Label className="ml-md-5 ml-lg-5"><span className="tags green">green</span></Label>
                                                                    </div> */}
                                                            </div>
                                                        }

                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Accompanying Data:</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>Post Processor/Digital Twin</Label>
                                                                <span className="tags ml-2">Download <img src={ImagePath.loaderred} alt="loader" className="img-fluid ml-2 rotating" /></span>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0 w-25 ml-md-5 ml-lg-5"></Label>
                                                            <div className="btn-tags ml-md-5 ml-lg-5">
                                                                <Label className="d-block d-md-inline d-lg-inline">CAD</Label>
                                                                <span className="tags ml-2">Download <img src={ImagePath.loaderred} alt="loader" className="img-fluid ml-2 rotating" /></span>
                                                            </div>
                                                        </div>
                                                    </CardBody>
                                                </Card>
                                                {/* <Card>
                                                    <CardBody className="redformpart">
                                                        <div className="d-flex justify-content-between border-bottom  mb-4">
                                                            <Label className="form-label p-0 text-nowrap">Machine Capabilities</Label>
                                                            <div><img src={ImagePath.Redcircle} alt="loader" className="img-fluid ml-2 rotating" /></div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">x: </Label>
                                                            <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">Nul</span></Label>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Y :</Label>
                                                            <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">Nul</span></Label>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Z :</Label>
                                                            <Label className="ml-md-5 ml-lg-5">
                                                                <span className="tags red-bg mb-2 mb-md-0 mb-lg-0 mr-0 mr-md-2 mr-lg-2">Null</span>
                                                            </Label>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">gauge</Label>
                                                            <div className="btn-tags mr-3 ml-md-5 ml-lg-5">
                                                                <span className="tags red-bg">Null</span>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Max Component Length</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>0</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Max Component Dmeter</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>0</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Max Drill Depth</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>0</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Material Capability</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>1</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Quantity</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>1</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Quality</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>1</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Stock Material</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>0</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Additional Process</Label>
                                                            <div className="btn-tags mr-3 mr-3 ml-md-5 ml-lg-5">
                                                                <Label>0</Label>
                                                            </div>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Added on</Label>
                                                            <Label className="ml-md-5 ml-lg-5"><span className="tags red-bg">2020-08-04T12:48:33.051339Z</span></Label>
                                                        </div>
                                                        <div className="d-md-flex d-lg-flex mb-3">
                                                            <Label className="form-label text-nowrap p-0">Color Classification</Label>
                                                            <Label className="ml-md-5 ml-lg-5"><span className="tags green">green</span></Label>
                                                        </div>
                                                    </CardBody>
                                                </Card> */}

                                            </div>
                                        </Container>

                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default withRouter(ApplyJob);