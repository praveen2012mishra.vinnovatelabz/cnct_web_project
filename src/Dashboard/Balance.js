import React, { useCallback, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPoundSign, } from '@fortawesome/free-solid-svg-icons';
import {
    Col,
    Container,
    Row, 
    Button,
    Label,
    FormGroup,
    Input,    
} from 'reactstrap';

import DashboardSideBar from './DashboardSideBar';
import { ImagePath } from '../ImagePath';
import './style.css';


const Balance = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');

    const toggle = () => setIsOpen(!isOpen);

    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        } 
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">
                                        
                                    <Container fluid>
                                        <div className="filter-green">
                                            <h6 className="text-green mb-0">Balance</h6>
                                        </div>
                                        <div className="subcon">
                                            <Row>
                                                <Col xs={12} sm={12} md={12} lg={8}>
                                                    <FormGroup>
                                                    <Label className="form-label">Industry </Label>
                                                    <div className="d-flex align-items-center">
                                                        <div className="form-input bdr-gray w-75 sm py-2">
                                                            <Input type="text" className="py-0 h-auto bold" placeholder="0.00" />
                                                            <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faPoundSign} /></span>
                                                        </div>
                                                        <span><Button className="graybtn ml-2">Withdraw</Button></span>
                                                    </div>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Container>
                                        
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default Balance;