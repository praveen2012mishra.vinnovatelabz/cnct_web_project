import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock, faUser, faBookOpen, faCalendar, faCaretRight, faPoundSign, faFilter, faBriefcase, faCloudUploadAlt, faPaperclip, faPaperPlane, } from '@fortawesome/free-solid-svg-icons';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,
    Card,
    CardBody,
} from 'reactstrap';
import DashboardSideBar from './DashboardSideBar';
import { ImagePath } from '../ImagePath';
import './style.css';
import { withRouter } from "react-router-dom";
import moment from 'moment';
import BASE_API_URL from '../Config/config';
import { getApiWithToken, postApiCallWithToken } from '../Config/api';


const SubconductingApplyJob = (props) => {
    const [jobDetails, getJobDetails] = useState({});
    const [specialism, getSpecialism] = useState([]);
    const [softwareList, getSoftwareList] = useState([]);
    const [machineCapabilities, getMachineCapabilities] = useState({});
    const [applyBtnStatus, setApplyButtonStatus] = useState(false);
    const [bidDetails, setBidDetails] = useState({
        bid_value: "",
        bid_terms: "",
        bid_term_details: "",
        payment_type: ""
    })

    useEffect(() => {
        console.log('Job details: ', props.match.params.Id);
        get_jobDetails(props.match.params.Id);
    }, [])

    const formatInput = (e) => {
        // Prevent characters that are not numbers ("e", ".", "+" & "-") ✨
        let checkIfNum;
        if (e.key !== undefined) {
            // Check if it's a "e", ".", "+" or "-"
            checkIfNum = e.key === "e" || e.key === "." || e.key === "+" || e.key === "-";
        }
        else if (e.keyCode !== undefined) {
            // Check if it's a "e" (69), "." (190), "+" (187) or "-" (189)
            checkIfNum = e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 187 || e.keyCode === 189;
        }
        return checkIfNum && e.preventDefault();
    }

    const get_jobDetails = (jobId) => {
        let userid = localStorage.getItem('userId');
        //http://111.93.169.90:2030/user/17/job/details/6/
        getApiWithToken(BASE_API_URL + 'user/' + userid + '/job/details/' + jobId + '/')
            .then(response => {
                console.log('job response: ', response.data.job_details);
                getJobDetails(response.data.job_details)
                getSpecialism(response.data.spcl_skill_list)
                getSoftwareList(response.data.software_list)
                getMachineCapabilities(response.data.capabilities)
            })
            .catch(err => {
                console.log(err);
            });
    }
    const applyButtonPressed = () => {
        setApplyButtonStatus(!applyBtnStatus)
    }

    const setBidValue = (e) => {
        setBidDetails({
            ...bidDetails,
            [e.target.name]: e.target.value
        });
    }

    const submitBiddingInformation = () => {
        if (bidDetails.bid_value === '') {
            alert('Please enter bidding amount')
        } else if (bidDetails.payment_type === '') {
            alert('Please choose payment option')
        } else {
            let userid = localStorage.getItem('userId');
            //http://111.93.169.90:2030/user/[user_id]/job/placebid/[job_id]/
            postApiCallWithToken(BASE_API_URL + 'user/' + userid + '/job/placebid/' + jobDetails.id + '/',
                'post',
                bidDetails)
                .then(response => {
                    console.log(response);
                    alert(response.data.msg);
                    get_jobDetails(jobDetails.id);
                })
                .catch(err => {
                    console.log(err);
                })
        }
    }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className="sideNavOpen">
                                    <div className="centercontent">

                                        <Container fluid>

                                            <div className="filter-green">
                                                <h6 className="text-green mb-0">{jobDetails.job_title}</h6>
                                            </div>
                                            <div className="subcon">
                                                <Row>
                                                    <Card className="w-100">
                                                        <CardBody className="greenformpart">
                                                            <div className="d-md-flex d-lg-flex align-items-center justify-content-between py-3 px-2 title border-bottom mb-4">
                                                                <div className="d-flex align-items-center">
                                                                    <span className="text-green"><FontAwesomeIcon icon={faClock} /></span>
                                                                    <span className="d-block ml-3">Posted {moment(jobDetails.created_on).format("DD/MM/yyyy, h:mm a")}</span>
                                                                </div>
                                                                <div className="btn-tags mt-2 mt-md-0 mt-lg-0">

                                                                    {jobDetails.is_flexible === 'R' ?
                                                                        <span className="tags green">Remote</span> :
                                                                        jobDetails.is_flexible === 'O' ?
                                                                            <span className="tags green">On-Site</span> :
                                                                            <span className="tags green">Flexible</span>
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className="d-sm-flex justify-content-end mb-4">
                                                                <div className="d-flex align-items-center rating">
                                                                    <span>Supplier Rating</span>
                                                                    <span className="greenrate">7.5</span>
                                                                </div>
                                                            </div>
                                                            <div className="name-comp d-flex mb-3">
                                                                <div className="d-md-flex d-lg-flex mr-3">

                                                                    {jobDetails.job_type === 'T' ?
                                                                        <span><FontAwesomeIcon icon={faBookOpen} className="text-green mr-2" /> Time</span> :
                                                                        <span><FontAwesomeIcon icon={faBookOpen} className="text-green mr-2" /> Project</span>
                                                                    }
                                                                    <div><span className="tags green-bg ml-md-5 ml-lg-5">Delivery Date : {moment(jobDetails.end_date).format("DD/MM/yyyy")}</span></div>
                                                                </div>
                                                            </div>
                                                            <div className="name-comp d-md-flex mb-3">
                                                                <div className="d-flex mr-3 mb-3 mb-md-0">
                                                                    <span className="text-green mr-2"><FontAwesomeIcon icon={faUser} /></span>
                                                                    <span>{jobDetails.company_name}</span>
                                                                </div>
                                                            </div>
                                                            <div className="mb-3">
                                                                <div className="mr-3 mb-2">
                                                                    <span><span className="text-black">Job</span>  Summary</span>
                                                                </div>
                                                                <p>{jobDetails.job_summary}</p>
                                                            </div>


                                                            <Row className="mb-3">
                                                                <Col xs={12} sm={12} md={2} lg={2}>
                                                                    <span><span className="text-black">Location</span>  Type</span>
                                                                </Col>
                                                                <Col xs={12} sm={12} md={10} lg={10}>
                                                                    {jobDetails.is_flexible === 'R' ?
                                                                        <span>Remote</span> :
                                                                        jobDetails.is_flexible === 'O' ?
                                                                            <span>On-Site</span> :
                                                                            <span>Flexible</span>
                                                                    }

                                                                </Col>
                                                            </Row>
                                                            <Row className="mb-3">
                                                                <Col xs={12} sm={12} md={2} lg={2}>
                                                                    <span><span className="text-black">Location :</span></span>
                                                                </Col>
                                                                <Col xs={12} sm={12} md={10} lg={10}>
                                                                    {jobDetails.is_flexible === 'R' ?
                                                                    <div></div> :
                                                                    <span>{jobDetails.location} <span className="tags green-bg">{jobDetails.postcode}</span></span>
                                                                    }
                                                                    
                                                                </Col>
                                                            </Row>
                                                            <Row className="mb-3">
                                                                <Col xs={12} sm={12} md={2} lg={2}>
                                                                    <span><span className="text-black">Industry :</span></span>
                                                                </Col>
                                                                <Col xs={12} sm={12} md={10} lg={10}>
                                                                    <span>{jobDetails.industry}</span>
                                                                </Col>
                                                            </Row>
                                                            <Row className="mb-3">
                                                                <Col xs={12} sm={12} md={2} lg={2}>
                                                                    <span><span className="text-black">Certificates :</span></span>
                                                                </Col>
                                                                <Col xs={12} sm={12} md={10} lg={10}>
                                                                    <div className="d-sm-flex mb-3">
                                                                        <div className="btn-tags mr-3">
                                                                            <span className="tags green">Certificate 1</span>
                                                                            <span className="tags ">Required</span>
                                                                        </div>
                                                                        <div className="btn-tags mr-3">
                                                                            <span className="tags green">Certificate 2</span>
                                                                            <span className="tags ">Preferred</span>
                                                                        </div>
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                            <Row className="mb-3">
                                                                <Col xs={12} sm={12} md={2} lg={2}>
                                                                    <span><span className="text-black">Preferred Payment Method :</span></span>
                                                                </Col>
                                                                <Col xs={12} sm={12} md={10} lg={10}>
                                                                    <div className="d-sm-flex mb-3">
                                                                        <div className="btn-tags mr-3">
                                                                            {jobDetails.preferred_payment_method === "I" ?
                                                                                <span className="tags green">Instant CNCT</span> :
                                                                                <span className="tags green">Special Terms</span>
                                                                            }

                                                                            <span className="tags ">Download <img src={ImagePath.Smloader} alt="loader" className="img-fluid ml-2 rotating" /></span>
                                                                        </div>
                                                                    </div>
                                                                </Col>
                                                            </Row>

                                                            <Row className="mb-3">
                                                                <Col xs={12} sm={12} md={2} lg={2}>
                                                                    <span><span className="text-black">NDA :</span></span>
                                                                </Col>
                                                                <Col xs={12} sm={12} md={10} lg={10}>
                                                                    <div className="d-sm-flex mb-3">
                                                                        <div className="btn-tags mr-3">
                                                                            <span className="tags gray">Sign NDA to view project data</span>
                                                                            <span className="tags ">Download <img src={ImagePath.Smloader} alt="loader" className="img-fluid ml-2 rotating" /></span>
                                                                        </div>
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                        </CardBody>
                                                    </Card>
                                                </Row>

                                                <div className="subcon">
                                                    <Row>
                                                        <Card className="w-100">
                                                            <CardBody>
                                                                <div className="d-flex align-items-center justify-content-between pb-3 border-bottom mb-3">
                                                                    <h6 className="mb-0">Capability</h6>
                                                                    <img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loder" />
                                                                </div>
                                                                <Row className="mb-3">
                                                                    <Col xs={12} sm={12} md={2} lg={2}>
                                                                        <span><span className="text-black">Capability Type :</span></span>
                                                                    </Col>
                                                                    <Col xs={12} sm={12} md={10} lg={10}>
                                                                        <div className="d-sm-flex mb-3">
                                                                            <div className="btn-tags mr-3">
                                                                                <span className="tags green">{jobDetails.capabilities_type}</span>
                                                                            </div>
                                                                        </div>
                                                                    </Col>
                                                                </Row>
                                                                {jobDetails.capabilities_type === 'People' ?
                                                                    <div>
                                                                        <Row className="mb-3">
                                                                            <Col xs={12} sm={12} md={2} lg={2}>
                                                                                <span><span className="text-black">Skill :</span></span>
                                                                            </Col>
                                                                            <Col xs={12} sm={12} md={10} lg={10}>
                                                                                <div className="d-sm-flex mb-3">
                                                                                    <div className="btn-tags mr-3">
                                                                                        <span className="tags green">{jobDetails.skill_name}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row className="mb-3">
                                                                            <Col xs={12} sm={12} md={2} lg={2}>
                                                                                <span><span className="text-black">Specialism :</span></span>
                                                                            </Col>
                                                                            <Col xs={12} sm={12} md={10} lg={10}>
                                                                                <div className="d-sm-flex mb-3">
                                                                                    <div className="btn-tags mr-3">
                                                                                        {/* <span className="tags green">Component Design</span> */}
                                                                                        {specialism != null ?
                                                                                            specialism.map((speciality, index) => (
                                                                                                <span className="tags green mt-2 mt-md-0 mt-lg-0 ml-md-3 ml-lg-3" key={index}>{speciality.specialism_name}</span>
                                                                                            )) : null
                                                                                        }
                                                                                        {/* <span className="tags green mr-2 mt-2 mt-md-0 mt-lg-0 ml-md-3 ml-lg-3">Mould Tool Design</span> */}
                                                                                    </div>
                                                                                </div>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row className="mb-3">
                                                                            <Col xs={12} sm={12} md={2} lg={2}>
                                                                                <span><span className="text-black">Design Software:</span></span>
                                                                            </Col>
                                                                            <Col xs={12} sm={12} md={10} lg={10}>
                                                                                <div className="d-sm-flex mb-3">
                                                                                    {softwareList != null ?
                                                                                        softwareList.map((software, index) => (
                                                                                            <div className="btn-tags mr-3">
                                                                                                <span className="tags green" key={index}>{software.software_name}</span>
                                                                                                {software.is_required === true ?
                                                                                                    <span className="tags ">Required</span> :
                                                                                                    <span className="tags ">Preferred</span>
                                                                                                }
                                                                                            </div>
                                                                                        )) : null
                                                                                    }
                                                                                    {/* <div className="btn-tags mr-3">
                                                                                        <span className="tags green">Autodesk autocad</span>
                                                                                        <span className="tags ">Required</span>
                                                                                    </div>
                                                                                    <div className="btn-tags mr-3">
                                                                                        <span className="tags green">Autodesk fusion360</span>
                                                                                        <span className="tags ">Preferred</span>
                                                                                    </div> */}
                                                                                </div>
                                                                            </Col>
                                                                        </Row>

                                                                    </div> :
                                                                    <div>
                                                                        <Row className="mb-3">
                                                                            <Col xs={12} sm={12} md={2} lg={2}>
                                                                                <span><span className="text-black">Skill :</span></span>
                                                                            </Col>
                                                                            <Col xs={12} sm={12} md={10} lg={10}>
                                                                                <div className="d-sm-flex mb-3">
                                                                                    <div className="btn-tags mr-3">
                                                                                        <span className="tags green">{jobDetails.skill_name}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row className="mb-3">
                                                                            <Col xs={12} sm={12} md={2} lg={2}>
                                                                                <span><span className="text-black">Hardware Classification :</span></span>
                                                                            </Col>
                                                                            <Col xs={12} sm={12} md={10} lg={10}>
                                                                                <div className="d-sm-flex mb-3">
                                                                                    <div className="btn-tags mr-3">
                                                                                        <span className="tags green">{machineCapabilities.classification}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </Col>
                                                                        </Row>
                                                                    </div>
                                                                }
                                                                <Row className="mb-3 position-relative">
                                                                    <div className="layer-lock">
                                                                        <div className="lock-sign">
                                                                            <div className="lockbox"><FontAwesomeIcon icon={faLock} /></div>
                                                                            <span>Please sign in NDA to view details.</span>
                                                                        </div>
                                                                    </div>
                                                                    <Col xs={12} sm={12} md={2} lg={2}>
                                                                        <span className="text-black">Accompanying Data:</span>
                                                                    </Col>
                                                                    <Col xs={12} sm={12} md={5} lg={5}>
                                                                        <div className="d-md-flex d-lg-flex align-items-center justify-content-between mb-3 mt-2 mt-md-0 mt-lg-0">
                                                                            <span>Post Processor/Digital Twin</span>
                                                                            <span className="tags ">Download <img src={ImagePath.Smloader} alt="loader" className="img-fluid ml-2 rotating" /></span>
                                                                        </div>
                                                                        <div className="d-md-flex d-lg-flex align-items-center justify-content-between mb-3">
                                                                            <span className="d-block">CAD</span>
                                                                            <span className="tags ">Download <img src={ImagePath.Smloader} alt="loader" className="img-fluid ml-2 rotating" /></span>
                                                                        </div>
                                                                    </Col>
                                                                </Row>
                                                                {jobDetails.is_bid_by_me ?
                                                                    <div></div> :
                                                                    <div className="d-flex pt-3 justify-content-between align-items-center bttn-wrap">
                                                                        <Button color="success rounded" value={jobDetails.id} onClick={applyButtonPressed}>Apply <img src={ImagePath.Smloader} alt="loader" className="img-fluid ml-2 rotating" /></Button>
                                                                    </div>
                                                                }

                                                            </CardBody>
                                                        </Card>
                                                    </Row>
                                                </div>

                                                {applyBtnStatus ?
                                                    <div className="subcon">
                                                        <Row>
                                                            <Card className="w-100">
                                                                <CardBody>
                                                                    <FormGroup>
                                                                        <Label className="form-label">Bid</Label>
                                                                        <div className="form-input">
                                                                            <Input type="number" className="form-control " placeholder="Enter Your Budget" name='bid_value' onChange={setBidValue} onKeyDown={formatInput} />
                                                                            <span className="icon-wr"><FontAwesomeIcon icon={faPoundSign} /></span>
                                                                        </div>
                                                                        <span className="form-text">* Budget information is not shared with potential providers, this is purely recorded to analyse your target versus actual project costs.</span>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                        <Label className="form-label">Preferred <span className="font-weight-exlight"> payment method</span>
                                                                        </Label>

                                                                        <div className="px-3 border-green rounded py-2">
                                                                            <div className="radio-text pb-3">
                                                                                <div className="custom-control custom-radio">
                                                                                    <Input type="radio" className="custom-control-input" id="r1" name="payment_type" value='A' onChange={setBidValue} />
                                                                                    <Label className="custom-control-label" for="r1">Accept outsourcer payment terms</Label>
                                                                                </div>
                                                                                <p>Sometimes, on larger projects, we appreciate that extended payment terms are needed. Special Terms allows the buyers and seller to agree their own payment terms and even upload terms and conditions of business. When the project is agreed, we take a payment equal to the seller's transaction fee from the buyer (akin to a deposit). When the project is completed, we send a completion note to both the buyer and seller and the seller invoices the buyer independently and directly for the balance (agreed project fee, mins transaction fee).</p>
                                                                            </div>
                                                                            <div className="radio-text pb-3">
                                                                                <div className="custom-control custom-radio">
                                                                                    <Input type="radio" className="custom-control-input" id="r2" name="payment_type" value='I' onChange={setBidValue} />
                                                                                    <Label className="custom-control-label" for="r2">INSTANT CNCT</Label>
                                                                                </div>
                                                                                <p>INSTANT CNCT is our immediate payment method. Electronic payment is taken from the buyer at the point that the project is agreed. Once the project is completed, both parties confirm that it has been completed correctly and payment is automatically sent to the seller's account (minus the transaction fee). Buyer's receive a sales invoice from CNCT and seller's receive a purchase invoice from CNCT</p>
                                                                            </div>
                                                                            <div className="radio-text pb-3">
                                                                                <div className="custom-control custom-radio">
                                                                                    <Input type="radio" className="custom-control-input" id="r3" name="payment_type" value='P' onChange={setBidValue} />
                                                                                    <Label className="custom-control-label" for="r3">Propose own Special Terms</Label>
                                                                                </div>
                                                                                <p>Sometimes, on larger projects, we appreciate that extended payment terms are needed. Special Terms allows the buyers and seller to agree their own payment terms and even upload terms and conditions of business. When the project is agreed, we take a payment equal to the seller's transaction fee from the buyer (akin to a deposit). When the project is completed, we send a completion note to both the buyer and seller and the seller invoices the buyer independently and directly for the.</p>
                                                                            </div>
                                                                            <div className="mb-3">
                                                                                <Input type="file" id="up" className="custom-control-input" />
                                                                                <Label className="btn-radio bg-green radius-360 px-4 text-white text-capitalize" for="up">Upload Terms &nbsp; &nbsp; <FontAwesomeIcon icon={faCloudUploadAlt} /></Label>
                                                                            </div>
                                                                        </div>
                                                                    </FormGroup>

                                                                    <FormGroup>
                                                                        <Label className="form-label">Message Outsourcer <span className="font-weight-exlight"> for Further Info</span>
                                                                        </Label>
                                                                        <Input type="textarea" className="green-textarea" placeholder="Type Message" name='bid_term_details' onChange={setBidValue}></Input>
                                                                    </FormGroup>

                                                                    <Button color="secondary" className="btn-dark py-2" type="submit" onClick={submitBiddingInformation}>submit <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating" /></Button>

                                                                </CardBody>
                                                            </Card>
                                                        </Row>
                                                    </div> : (<div></div>)

                                                }

                                                {jobDetails.is_bid_by_me ?
                                                    <div className="subcon">
                                                        <Row>
                                                            <Card className="w-100">
                                                                <CardBody>
                                                                    <FormGroup className="d-flex align-items-center">
                                                                        <Label className="form-label mr-5">Bid</Label>
                                                                        <div><span className="tags green"><FontAwesomeIcon icon={faPoundSign} /> {jobDetails.bid_amount}</span></div>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                        <Label className="form-label">Payment method</Label>

                                                                        <div className="px-3 py-2">
                                                                            {jobDetails.preferred_payment_method === 'I' ?
                                                                                <div className="radio-text pb-3">
                                                                                    <Label className="form-label pl-0">INSTANT CNCT</Label>
                                                                                    <p className="pl-0">INSTANT CNCT is our immediate payment method. Electronic payment is taken from the buyer at the point that the project is agreed. Once the project is completed, both parties confirm that it has been completed correctly and payment is automatically sent to the seller's account (minus the transaction fee). Buyer's receive a sales invoice from CNCT and seller's receive a purchase invoice from CNCT</p>
                                                                                </div> :
                                                                                jobDetails.preferred_payment_method === 'A' ?
                                                                                    <div className="radio-text pb-3">
                                                                                        <Label className="form-label pl-0">Accept outsourcer payment terms</Label>
                                                                                        <p className="pl-0">Sometimes, on larger projects, we appreciate that extended payment terms are needed. Special Terms allows the buyers and seller to agree their own payment terms and even upload terms and conditions of business. When the project is agreed, we take a payment equal to the seller's transaction fee from the buyer (akin to a deposit). When the project is completed, we send a completion note to both the buyer and seller and the seller invoices the buyer independently and directly for the balance (agreed project fee, mins transaction fee).</p>
                                                                                    </div> :
                                                                                    jobDetails.preferred_payment_method === 'P' ?
                                                                                        <div className="radio-text pb-3">
                                                                                            <Label className="form-label pl-0">Propose own Special Terms</Label>
                                                                                            <p className="pl-0">Sometimes, on larger projects, we appreciate that extended payment terms are needed. Special Terms allows the buyers and seller to agree their own payment terms and even upload terms and conditions of business. When the project is agreed, we take a payment equal to the seller's transaction fee from the buyer (akin to a deposit). When the project is completed, we send a completion note to both the buyer and seller and the seller invoices the buyer independently and directly for the.</p>
                                                                                        </div> : null
                                                                            }

                                                                            <div className="radio-text pb-3">
                                                                                <Label className="form-label pl-0">Message Outsourcer <span className="font-weight-exlight">for Further Info</span></Label>
                                                                                <p className="pl-0">{jobDetails.bid_terms_details}</p>
                                                                            </div>
                                                                        </div>
                                                                    </FormGroup>
                                                                </CardBody>
                                                            </Card>
                                                        </Row>
                                                    </div> : <div></div>

                                                }


                                                <div className="filter-green">
                                                    <h6 className="text-black mb-0">Messages</h6>
                                                    <div>
                                                        <span className="tags green mr-3">Accept Offer</span>
                                                        <span className="tags red-bg">Submit Counter Offer</span>
                                                    </div>
                                                </div>
                                                <div className="subcon">
                                                    <Row>
                                                        <Card className="w-100">
                                                            <CardBody>
                                                                <div className="onechat p-3 border-bottom d-flex align-items-start">
                                                                    <div className="userImg"><img src={ImagePath.avtar1} alt="avtar" /></div>
                                                                    <div className="chatDetails pl-3">
                                                                        <p className="font-weight-bold text-black">Toolmaker Ltd</p>
                                                                        <p>Hello,</p>
                                                                        <p>I like your proposal.</p>
                                                                    </div>
                                                                </div>

                                                                <div className="onechat p-3 border-bottom justify-content-end d-flex align-items-start">
                                                                    <div className="chatDetails text-right pr-3">
                                                                        <p className="font-weight-bold text-black">Watson Goms</p>
                                                                        <p>Ok,</p>
                                                                        <p>It’s great to hear you, thanks.</p>
                                                                    </div>
                                                                    <div className="userImg"><img src={ImagePath.avtar2} alt="avtar" /></div>
                                                                </div>

                                                                <div className="onechat p-3 border-bottom d-flex align-items-start">
                                                                    <div className="userImg"><img src={ImagePath.avtar1} alt="avtar" /></div>
                                                                    <div className="chatDetails pl-3">
                                                                        <p className="font-weight-bold text-black">Toolmaker Ltd</p>
                                                                        <p>Hello,</p>
                                                                        <p>I like your proposal.</p>
                                                                    </div>
                                                                </div>
                                                                <FormGroup className="inputMessage d-flex align-items-center mt-4">
                                                                    <input type="text" placeholder="Type here" className="messageInput" />
                                                                    <Label for="attachemnt" className="attachemnt mb-0">
                                                                        <input type="file" id="attachemnt" />
                                                                        <FontAwesomeIcon icon={faPaperclip} />
                                                                    </Label>
                                                                    <button className="messageSend" type="submit"><FontAwesomeIcon icon={faPaperPlane} /></button>
                                                                </FormGroup>
                                                            </CardBody>
                                                        </Card>
                                                    </Row>
                                                </div>



                                            </div>

                                        </Container>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default withRouter(SubconductingApplyJob);