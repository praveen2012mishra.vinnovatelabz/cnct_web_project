import React, { useCallback, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,    
} from 'reactstrap';

import DashboardSideBar from './DashboardSideBar';
import BASE_API_URL from '../Config/config';
import {postApiCallWithToken} from '../Config/api';

import { ImagePath } from '../ImagePath';
import './style.css';


const Balance = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');
    const [allPassword, setPassword] = useState({
        password:"",
        newPassword:""
    });
    const [confirmPssword, setConfirmPassword] = useState('')

    const toggle = () => setIsOpen(!isOpen);

    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        } 
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    const handlePasswordValue = e => {
        setPassword({
          ...allPassword,
          [e.target.name]: e.target.value
        });
      };

      const handleConfirmPassword = e => {
        setConfirmPassword(e.target.value);
      };

      const updatePassword = () => {
        if (allPassword.password === ''){
            alert('Please enter old password');
        }else if (allPassword.newPassword === ''){
            alert('Please enter new password');
        }else if (allPassword.newPassword !== confirmPssword){
            alert('Password mismatch. Please re-enter your password');
        }else{
            //http://111.93.169.90:2030/web/changepassword/
            postApiCallWithToken(BASE_API_URL + 'web/changepassword/', 
          'post',
          allPassword
          )
          .then(response => {
            console.log("response:  ", response);
            if (response.data.success) {
                localStorage.setItem('userToken', response.data.token);
                alert(response.data.msg);
            }else {
                alert(response.data.msg);
            }
            
          })
          .catch(err => {
          console.log(err);
          alert("Some error occured.");
          })
        }
      }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">
                                        
                                    <Container fluid>
                                        <div className="filter-green">
                                            <h6 className="text-green mb-0">Change Password</h6>
                                        </div>
                                        <div className="subcon">
                                            <Row>
                                                <Col xs={12} sm={12} md={12} lg={6}>
                                                    <FormGroup>
                                                    <Label className="form-label">Current Password</Label>
                                                    <div className="form-input bdr-gray ">
                                                        <Input type="password" className="py-0 h-auto bold" placeholder="*****" name='password' onChange={handlePasswordValue} value={allPassword.password}/>
                                                        <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faLock} /></span>
                                                    </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} sm={12} md={12} lg={6}>
                                                    <FormGroup>
                                                    <Label className="form-label">New Password</Label>
                                                    <div className="form-input bdr-gray ">
                                                        <Input type="password" className="py-0 h-auto bold" placeholder="*****" name='newPassword' onChange={handlePasswordValue} value={allPassword.newPassword}/>
                                                        <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faLock} /></span>
                                                    </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} sm={12} md={12} lg={6}>
                                                    <FormGroup>
                                                    <Label className="form-label">Confirm Password</Label>
                                                    <div className="form-input bdr-gray ">
                                                        <Input type="password" className="py-0 h-auto bold" placeholder="*****" onChange={handleConfirmPassword} value={confirmPssword}/>
                                                        <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faLock} /></span>
                                                    </div>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <FormGroup>
                                                <Button color="secondary" className="btn-dark" type="submit" onClick={updatePassword}>submit <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 w30loader  rotating" /></Button>
                                            </FormGroup>
                                        </div>
                                    </Container>
                                        
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default Balance;