import React, { useCallback, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faCheckSquare, faCalendar } from '@fortawesome/free-regular-svg-icons';
import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,
    Table,
} from 'reactstrap';

import DashboardSideBar from './DashboardSideBar';

import { ImagePath } from '../ImagePath';
import './style.css';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import { faCloudDownloadAlt, faPoundSign } from '@fortawesome/free-solid-svg-icons';


const Notification = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');

    const toggle = () => setIsOpen(!isOpen);

    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        }
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    const [startDate, setStartDate] = useState(new Date());

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">

                                        <Container fluid>
                                            <div className="filter-green">
                                                <h6 className="text-green mb-0">Transaction History</h6>
                                            </div>
                                            <div className="subcon">
                                                <div className="d-sm-flex d-md-flex d-lg-flex justify-content-between my-4">
                                                    <div class="date-fram mb-2 mb-sm-0 mb-md-0 mb-lg-0">
                                                        <DatePicker selected={startDate} onChange={date => setStartDate(date)} />
                                                        <DatePicker selected={startDate} onChange={date => setStartDate(date)} />
                                                        {/* <FontAwesomeIcon icon={faCalendar} /> */}
                                                    </div>
                                                    <Button className="download-btn">Download <strong>CSV</strong> <FontAwesomeIcon icon={faCloudDownloadAlt} /></Button>
                                                </div>

                                                <div className="d-sm-flex d-md-flex d-lg-flex justify-content-between my-4">
                                                    <div class="d-sm-flex d-md-flex d-lg-flex mb-2 mb-sm-0 mb-md-0 mb-lg-0">
                                                        <select type="select" name="select" id="" className="select2">
                                                            <option>All Trasactions</option>
                                                        </select>
                                                        <select type="select" name="select" id="" className="select2">
                                                            <option>All Clients</option>
                                                        </select>
                                                    </div>
                                                    <button className="gobtn">Go</button>
                                                </div>


                                                <div className="table-responsive">
                                                    <Table borderless className="th-border">
                                                        <thead>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Type</th>
                                                                <th>Description</th>
                                                                <th>Job Id</th>
                                                                <th>Amount</th>
                                                                <th>Invoice Id</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Nov 14, 2020</td>
                                                                <td>Outsourcing Payment</td>
                                                                <td>Service fee-fixed price- Ref id 1458796A</td>
                                                                <td>E-0258</td>
                                                                <td><FontAwesomeIcon icon={faPoundSign} /> 25789.28<br />
                                                                    <button className="reject-btn">Reject</button>
                                                                </td>
                                                                <td>SI-#######</td>
                                                            </tr>
                                                            <tr className="green-text">
                                                                <td>Oct 22, 2020</td>
                                                                <td>Subcontracting Receipt</td>
                                                                <td>Subscription anual charge</td>
                                                                <td>C-2525</td>
                                                                <td><FontAwesomeIcon icon={faPoundSign} /> 225.00<br />
                                                                    <button className="completed-btn">Completed</button>
                                                                </td>
                                                                <td>PI-#######</td>
                                                            </tr>
                                                            <tr className="inProgress-text">
                                                                <td>Jan 12, 2017</td>
                                                                <td>Subscription renewal charge</td>
                                                                <td>Subscription anual charge</td>
                                                                <td>A-0225</td>
                                                                <td><FontAwesomeIcon icon={faPoundSign} /> 35225.00<br />
                                                                    <button className="inProgress-btn">In Progress</button>
                                                                </td>
                                                                <td>WD-#######</td>
                                                            </tr>
                                                            <tr>
                                                                <td>June 15, 2020</td>
                                                                <td>Subscription Payment</td>
                                                                <td>Service fee-fixed price- Ref id 1458796A</td>
                                                                <td>AC-5698</td>
                                                                <td><FontAwesomeIcon icon={faPoundSign} /> 205789.28<br />
                                                                    <button className="reject-btn">In Progress</button>
                                                                </td>
                                                                <td>SI-#######</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Feb 15, 2020</td>
                                                                <td>Outsourcing Refund</td>
                                                                <td>Service fee-fixed price- Ref id 1458796A</td>
                                                                <td>B-2456</td>
                                                                <td><FontAwesomeIcon icon={faPoundSign} /> 14789.28<br />
                                                                    <button className="completed-btn">In Progress</button>
                                                                </td>
                                                                <td>SC-#######</td>
                                                            </tr>
                                                        </tbody>
                                                    </Table>
                                                </div>



                                            </div>
                                        </Container>

                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default Notification;