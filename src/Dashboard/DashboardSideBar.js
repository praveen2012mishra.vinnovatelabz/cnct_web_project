import React, { useCallback, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { faArrowRight, faBars, faCaretDown, faCaretRight, faFolder, faCalendar, faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Col, Collapse,
  Container,
   DropdownItem, 
   DropdownMenu, 
   DropdownToggle, 
   Nav, 
   Navbar,
  NavbarToggler,
  NavItem,
  Row, 
  UncontrolledDropdown,
  UncontrolledCollapse,
  Card,
  CardBody,
  Button,    
} from 'reactstrap';

import { ImagePath } from './../ImagePath';
import { Scrollbars } from 'react-custom-scrollbars';

const DashboardSideBar = (props) =>{
    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');

    const toggle = () => setIsOpen(!isOpen);

    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        } 
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }
  return(
    <React.Fragment>
      <div id="sidemenu" className={openSideNav} >
        <div className="topbar">
            <NavLink to="/"><img src={ImagePath.blacklogo} className="img-fluid" alt="logo" /></NavLink>

            <button onClick={handleMenuBar} type="button" className="menubar btn btn-link"><FontAwesomeIcon icon={faBars} /></button>
        </div>
        <Scrollbars autoHide >
        <div className={`sidemenu pt-4 ${openSideNav}`}>
            <h5 className="font-weight-light">MENU</h5>
            <ul className="nav list-unstyled nav-list-menu">
                <li>
                    <NavLink to="/">
                        <span className="m-number">1</span>
                            Home
                        </NavLink>
                </li>
                <li className="redmenu tognav">
                    <NavLink to="#" id="outsourcing">
                        <span className="m-number">2</span>
                            Outsourcing
                            <span className="caret"><FontAwesomeIcon icon={faCaretDown} /></span>
                    </NavLink>
                    <UncontrolledCollapse toggler="#outsourcing">
                    <div className="submenu">
                        <Link to="/" className="active"><FontAwesomeIcon icon={faArrowRight} /> <span>Create New Job</span></Link>
                        <Link to="/OutsourcingMyJobs"><FontAwesomeIcon icon={faArrowRight} /> <span>My Jobs</span></Link>
                    </div>
                    </UncontrolledCollapse>
                </li>
                <li className="greenmenu tognav">
                    <NavLink to="#" id="subcontracting">
                        <span className="m-number">3</span>
                            Subcontracting
                            <span className="caret"><FontAwesomeIcon icon={faCaretRight} /></span>
                    </NavLink>
                    <UncontrolledCollapse toggler="#subcontracting">
                    <div className="submenu">
                        <Link to="/NewMatchedOpportunities" className="active"><FontAwesomeIcon icon={faArrowRight} /> <span>New Matched Opportunities</span></Link>
                        <Link to="/SubconductingMyJobs"><FontAwesomeIcon icon={faArrowRight} /> <span>My Jobs</span></Link>
                    </div>
                    </UncontrolledCollapse>
                </li>
                <li className="greenmenu">
                    <NavLink to="#" id="myAccount">
                        <span className="m-number">4</span>
                            My Account
                            <span className="caret"><FontAwesomeIcon icon={faCaretRight} /></span>
                    </NavLink>
                    <UncontrolledCollapse toggler="#myAccount">
                        <div className="submenu tognav">
                            <Link to="/EditProfile"><FontAwesomeIcon icon={faArrowRight} /> <span>Edit Profile</span></Link>
                            <Link to="/ChangePassword"><FontAwesomeIcon icon={faArrowRight} /> <span>Change Password</span></Link>
                            <Link to="/ConnectLinkedin"><FontAwesomeIcon icon={faArrowRight} /> <span>Connect Linkedin Account</span></Link>
                            <Link to="/Notification"><FontAwesomeIcon icon={faArrowRight} /> <span>Notification Setting</span></Link>
                            <Link to="/Balance"><FontAwesomeIcon icon={faArrowRight} /> <span>Balance</span></Link>
                            <Link to="/TransactionHistory"><FontAwesomeIcon icon={faArrowRight} /> <span>Transaction History</span></Link>
                        </div>
                    </UncontrolledCollapse>
                </li>
                <li>
                    <NavLink to="/Capabilities">
                        <span className="m-number">5</span>
                        Capabilities
                        </NavLink>
                </li>
                <li>
                    <NavLink to="#">
                        <span className="m-number">6</span>
                            Upgrade My Plan
                        </NavLink>
                </li>
                <li>
                    <NavLink to="#">
                        <span className="m-number">7</span>
                            Post Processor Exchange
                        </NavLink>
                </li>
                <li>
                    <NavLink to="#">
                        <span className="m-number">8</span>
                            Contact Us
                        </NavLink>
                </li>
            </ul>
        </div>
        </Scrollbars>
        
    </div>
    </React.Fragment>
  )
}

export default DashboardSideBar;