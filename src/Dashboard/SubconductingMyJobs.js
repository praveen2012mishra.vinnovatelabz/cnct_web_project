import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPoundSign } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from "react-router-dom";

import {
    Col,
    Container,
    Row,
    Nav,
    NavLink,
    NavItem,
    TabContent,
    TabPane,
    Table,
} from 'reactstrap';
import classnames from 'classnames';

import DashboardSideBar from './DashboardSideBar';
import './style.css';
import BASE_API_URL from '../Config/config';
import { getApiWithToken } from '../Config/api';
import moment from 'moment';


const SubconductingMyJobs = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');

    const toggle = () => setIsOpen(!isOpen);
    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        }
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    const [activeTab, setActiveTab] = useState('1');
    const [JobsInBid, getJobsInBid] = useState([])
    const toggle1 = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    useEffect(() => {
        get_jobList()
    }, [])

    const get_jobList = () => {
        let userid = localStorage.getItem('userId');
        //http://111.93.169.90:2030/user/[user_id]/job/bidlistsubcontractor/[job_id]
        getApiWithToken(BASE_API_URL + 'user/' + userid + '/job/bidlistsubcontractor/')
            .then(response => {
                console.log('job response: ', response.data.bid);
                getJobsInBid(response.data.bid)
            })
            .catch(err => {
                console.log(err);
            });
    }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">

                                        <Container fluid>
                                            <div className="gray-title mb-4">
                                                <div className="nav-wrap sm">
                                                    <Nav tabs>
                                                        <NavItem>
                                                            <NavLink className={classnames({ active: activeTab === '1' })} onClick={() => { toggle1('1'); }}>
                                                                Jobs In Bid
                                                    </NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink className={classnames({ active: activeTab === '2' })} onClick={() => { toggle1('2'); }}>
                                                                Job in Progress
                                                    </NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink className={classnames({ active: activeTab === '3' })} onClick={() => { toggle1('3'); }}>
                                                                Job Completed
                                                    </NavLink>
                                                        </NavItem>
                                                    </Nav>
                                                </div>
                                            </div>
                                            <div className="subcon">


                                                <TabContent activeTab={activeTab}>
                                                    <TabPane tabId="1">
                                                        <div className="table-responsive">
                                                            <Table striped>
                                                                <thead>
                                                                    <tr>
                                                                        <th>Job name</th>
                                                                        <th>Job type</th>
                                                                        <th>Capability type</th>
                                                                        <th>Bid value</th>
                                                                        <th>Bid posted on</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {JobsInBid.map((jobDetails, index) => (
                                                                        <tr key={index}>
                                                                            <td>{jobDetails.job.job_title}</td>
                                                                            {jobDetails.job.job_type === 'T' ?
                                                                                <td>Time</td> :
                                                                                <td>Project</td>
                                                                            }
                                                                            <td>{jobDetails.job.capabilities_type}</td>
                                                                            <td><FontAwesomeIcon icon={faPoundSign} /> {jobDetails.bid_value}</td>
                                                                            <td>{moment(jobDetails.created_on).format("DD MMM, yyyy, hh:mm a")}</td>
                                                                        </tr>
                                                                    ))}
                                                                </tbody>
                                                            </Table>
                                                        </div>
                                                    </TabPane>

                                                    <TabPane tabId="2">
                                                        <div className="table-responsive">
                                                            <Table striped>
                                                                <thead>
                                                                    <tr>
                                                                        <th>Job name</th>
                                                                        <th>Job type</th>
                                                                        <th>Capability type</th>
                                                                        <th>Budget</th>
                                                                        <th>Terms</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Evoque Door 1</td>
                                                                        <td>Project</td>
                                                                        <td>Machine</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Evoque Door</td>
                                                                        <td>Time</td>
                                                                        <td>People</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Machinary Eqv</td>
                                                                        <td>Project</td>
                                                                        <td>Machine</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Machinary 2</td>
                                                                        <td>Time</td>
                                                                        <td>People</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                </tbody>
                                                            </Table>
                                                        </div>
                                                    </TabPane>

                                                    <TabPane tabId="3">
                                                        <div className="table-responsive">
                                                            <Table striped>
                                                                <thead>
                                                                    <tr>
                                                                        <th>Job name</th>
                                                                        <th>Job type</th>
                                                                        <th>Capability type</th>
                                                                        <th>Budget</th>
                                                                        <th>Terms</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Evoque Door 1</td>
                                                                        <td>Project</td>
                                                                        <td>Machine</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Evoque Door</td>
                                                                        <td>Time</td>
                                                                        <td>People</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Machinary Eqv</td>
                                                                        <td>Project</td>
                                                                        <td>Machine</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Machinary 2</td>
                                                                        <td>Time</td>
                                                                        <td>People</td>
                                                                        <td><FontAwesomeIcon icon={faPoundSign} /> 256.35</td>
                                                                        <td>INSTANT CNCT</td>
                                                                    </tr>
                                                                </tbody>
                                                            </Table>
                                                        </div>
                                                    </TabPane>

                                                </TabContent>


                                            </div>
                                        </Container>

                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default SubconductingMyJobs;