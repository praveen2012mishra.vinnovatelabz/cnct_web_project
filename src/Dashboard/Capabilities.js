import React, { useCallback, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlusCircle,
  faCheck,
  faSortNumericDownAlt,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Container,
  Row,
  Button,
  Label,
  FormGroup,
  Input,
} from "reactstrap";
import { Multiselect } from "multiselect-react-dropdown";

import DashboardSideBar from "./DashboardSideBar";
import BASE_API_URL from "../Config/config";
import { getApi, apiCallWithToken, postApiCallWithToken } from "../Config/api";

import { ImagePath } from "../ImagePath";
import "./style.css";
import Popup from "../Dashboard/PopUp";

import { TagInput } from "reactjs-tag-input";

class Capabilities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      OtherItems: [],
      singleItems: [],
      options: [
        { name: "Component Design", id: 1 },
        { name: "Dummy Ipsum", id: 2 },
      ],
      capabilityType: "",
      showPopup: false,
      howInterested: "",
      radious: "",
      skill_list: [],
      specialisation_list: [],
      designSoftware_list: [],
      inspectionSoftware_list: [],
      programingSoftware_list: [],
      simulationSoftware_list: [],
      selectedSkillId: "",
      selectedMachineCapabilitiesName: "",
      addMorePeople: "",
      selectedSpecialism: [],
      selectedSpecialismName: [],
      selectedDesign: [],
      selectedInspection: [],
      selectedPrograming: [],
      selectedSimulation: [],
      selectedSoftwareName: [],
      allCapabilities: [],
      selectedPeopleCapabilities: {
        capability_type: "People",
        skill: "",
        specialism_skill: "",
        skillName: "",
        relocation: "",
        radius: "",
        software: "",
        softwareNames: "",
      },
      selectedMachineCapabilities: {
        capability_type: "Machine",
        machine_capability_type: "",
        classification: "5",
        // machining_envelopes:"1",
      },

      machine_capabilities: [],
      hardware_classifications: [],
      machine_specialism: [],
      selected_hardware: {
        id: 0,
        classification_name: "",
        color: "",
        title: "",
      },

      manufacturelist: [],
      controllerList: [],
      materialSuitabilityList: [],

      green: {
        position: [
          { id: 1, title: "x", value: "" },
          { id: 2, title: "y", value: "" },
          { id: 3, title: "z", value: "" },
        ],
        inputField: [],
      },
      black: {
        position: [
          { id: 4, title: "x", value: "" },
          { id: 5, title: "y", value: "" },
        ],
        inputField: [{ id: 6, title: "Gauge", value: "" }],
      },
      gray: {
        position: [
          { id: 7, title: "x", value: "" },
          { id: 8, title: "y", value: "" },
          { id: 9, title: "z", value: "" },
        ],
        inputField: [
          {
            id: 10,
            title: "Max Drill Depth",
            value: "",
            key: "max_drill_depth",
          },
        ],
      },
      yellow: {
        position: [],
        inputField: [
          {
            id: 11,
            title: "Max Component Length",
            value: "",
            key: "max_component_length",
          },
          {
            id: 12,
            title: "Max Component Diameter",
            value: "",
            key: "max_component_diameter",
          },
        ],
      },
      red: {
        position: [
          { id: 13, title: "X", value: "" },
          { id: 14, title: "Y", value: "" },
          { id: 15, title: "Z", value: "" },
        ],
        inputField: [
          {
            id: 16,
            title: "Max Component Length",
            value: "",
            key: "max_component_length",
          },
          {
            id: 17,
            title: "Max Component Diameter",
            value: "",
            key: "max_component_diameter",
          },
        ],
      },
    };
  }

  componentDidMount() {
    this.get_skillListItems("people");
    this.get_skillListItems("machine");
  }

  onTagsChanged = (tags) => {};

  onSingleTagsChanged = (tags) => {
    let element = window.document.querySelectorAll(
      ".other-container-manufacture .cNrjqJ >span"
    );
    element.forEach((item, i) => {
      if (element.length - 1 === i) {
        //console.log(item,i);
      } else {
        item.style.display = "none";
      }
    });
    //console.log(tags[tags.length-1], element,"------------------>",tags);
    // if(tags[tags.length-1]){
    //   this.setState({singleItems:Object.assign([],tags[tags.length-1])})
    //   return Object.assign([],tags[tags.length-1]);
    // }
  };

  formatInput = (e) => {
    // Prevent characters that are not numbers ("e", ".", "+" & "-") ✨
    let checkIfNum;
    if (e.key !== undefined) {
      // Check if it's a "e", ".", "+" or "-"
      checkIfNum =
        e.key === "e" || e.key === "." || e.key === "+" || e.key === "-";
    } else if (e.keyCode !== undefined) {
      // Check if it's a "e" (69), "." (190), "+" (187) or "-" (189)
      checkIfNum =
        e.keyCode === 69 ||
        e.keyCode === 190 ||
        e.keyCode === 187 ||
        e.keyCode === 189;
    }
    return checkIfNum && e.preventDefault();
  };

  handleHowInterestedSelection = (e) => {
    let selectinterest = this.state.selectedPeopleCapabilities;
    selectinterest.relocation = e.target.value;
    this.setState({
      howInterested: e.target.value,
      selectedPeopleCapabilities: selectinterest,
    });
  };

  getRadious = (e) => {
    this.setState({
      radious: e.target.value,
    });
  };

  getSelectedSkillItem = (e) => {
    this.clearSelectedSofrwareAndSpecialism();
    let selectedSkill = this.state.selectedPeopleCapabilities;
    selectedSkill.skill = e.target.value;
    this.setState({
      selectedSkillId: e.target.id,
      selectedPeopleCapabilities: selectedSkill,
    });
    this.get_specilizationAndSoftwareList(e.target.id);
  };
  clearSoftwareData() {
    this.setState({
      specialisation_list: [],
      designSoftware_list: [],
      simulationSoftware_list: [],
      programingSoftware_list: [],
      inspectionSoftware_list: [],
    });
  }

  clearSelectedSofrwareAndSpecialism() {
    this.setState({
      selectedSpecialism: [],
      selectedSpecialismName: [],
      selectedSoftwareName: [],
      selectedDesign: [],
      selectedInspection: [],
      selectedPrograming: [],
      selectedSimulation: [],
    });
  }
  clearPeopleData() {
    let peopleCapabilities = this.state.selectedPeopleCapabilities;
    peopleCapabilities.capability_type = "People";
    peopleCapabilities.skill = "";
    peopleCapabilities.specialism_skill = "";
    peopleCapabilities.relocation = "";
    peopleCapabilities.radius = "";
    peopleCapabilities.software = "";
    this.setState({
      selectedPeopleCapabilities: peopleCapabilities,
      howInterested: "",
      radious: "",
      selectedSkillId: "",
    });

    this.get_skillListItems("people");
    this.clearSoftwareData();
    this.clearSelectedSofrwareAndSpecialism();

    var interestsOption = document.getElementsByName("interest");
    for (var i = 0; i < interestsOption.length; i++)
      interestsOption[i].checked = false;
    var allSkills = document.getElementsByName("skills");
    for (var i = 0; i < allSkills.length; i++) allSkills[i].checked = false;
  }
  setAllPeopleData() {
    let allPeopleCapabilities = this.state.selectedPeopleCapabilities;
    let softwares = [
      ...this.state.selectedDesign,
      ...this.state.selectedInspection,
      ...this.state.selectedPrograming,
      ...this.state.selectedSimulation,
    ];
    let allSoftware = softwares.join(","); //JSON.stringify(softwares);
    let specialism = this.state.selectedSpecialism.join(",");
    let specialismName = this.state.selectedSpecialismName.join(", ");
    allPeopleCapabilities.specialism_skill = specialism;
    allPeopleCapabilities.specialismName = specialismName;
    allPeopleCapabilities.softwareNames = this.state.selectedSoftwareName.join(
      ", "
    );
    allPeopleCapabilities.software = allSoftware;
    if (
      this.state.howInterested === "On-Site" ||
      this.state.howInterested === "Flexible"
    ) {
      allPeopleCapabilities.radius = this.state.radious;
    } else {
      allPeopleCapabilities.radius = "";
    }
    this.setState(
      {
        selectedPeopleCapabilities: allPeopleCapabilities,
      },
      () => {
        if (this.state.howInterested === "") {
          alert("Please select your interest to work");
        } else if (this.state.selectedSkillId === "") {
          alert("Please select any skill");
        } else {
          this.setState({
            capabilityType: "People",
            showPopup: !this.state.showPopup,
          });
        }
      }
    );
  }
  callMachineAPI() {
    // this.saveCapabilities(this.state.selectedMachineCapabilities, 'Machine');
    // this.get_skillListItems("machine");
    // var allMachines = document.getElementsByName("machine");
    // for (var i = 0; i < allMachines.length; i++)
    //   allMachines[i].checked = false;
  }
  setAllMachineData = () => {
    console.log(
      "selectedMachineCapabilities: ",
      this.state.selectedMachineCapabilities
    );
    // this.setState({
    //   showPopup: !this.state.showPopup,
    // });
    if (this.state.selectedMachineCapabilitiesName === '') {
      alert('Please choose machine capaniities')
    } else if (this.state.selectedMachineCapabilitiesName === 'Subcontract Inspection' || this.state.selectedMachineCapabilitiesName === 'Subcontract CNC Machining') {
      if (this.state.selected_hardware.id === 0) {
        alert('Please select hardware classification')
      } else {
        this.setState({
          capabilityType: 'Machine',
          showPopup: !this.state.showPopup
        })
      }
    } else {
      this.setState({
        capabilityType: 'Machine',
        showPopup: !this.state.showPopup
      })

    }

    // let allSelectedCapabilities = [];
    // new Promise((resolve, reject)=>{
    //   allSelectedCapabilities = this.state.allCapabilities;
    //   allSelectedCapabilities.push(this.state.selectedMachineCapabilities);
    //   console.log('allSelectedCapabilities: ', allSelectedCapabilities);
    //   resolve(true)
    // })
    // .then(res=>{
    //   this.setState({
    //     allCapabilities: allSelectedCapabilities
    //   }, () => {
    //     this.clearHardwareClassificationData();
    // });
    // });
  };
  addMorePeopleCheck = (e) => {
    this.setAllPeopleData();
  };
  getSelectedMachineCapabilities = (e) => {
    console.log("selectedMachineCapabilitiesName: ", e.target.id);
    let selectedMachineCapabilities = this.state.selectedMachineCapabilities;
    selectedMachineCapabilities.machine_capability_type = e.target.value;
    this.setState({
      selectedMachineCapabilitiesName: e.target.value,
      selectedMachineCapabilities: selectedMachineCapabilities,
    });
    this.clearHardwareClassificationData();
    this.get_manufacturelist(e.target.id);
    if (e.target.id === "7") {
      this.get_specilizationAndSoftwareList(e.target.id);
    } else {
      this.get_hardwareClassification(e.target.id);
    }
    if (e.target.value === "Subcontract CNC Machining") {
      this.get_controllerList();
      this.get_materialSuitability();
    }
    //e.target.value === 'Subcontract Inspection'
  };

  clearXYZValue() {
    if ("gauge" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["gauge"];
    }
    if ("max_drill_depth" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["max_drill_depth"];
    }
    if ("max_component_diameter" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["max_component_diameter"];
    }
    if ("max_component_length" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["max_component_length"];
    }
    if (
      "x" in this.state.selectedMachineCapabilities ||
      "y" in this.state.selectedMachineCapabilities
    ) {
      delete this.state.selectedMachineCapabilities["x"];
      delete this.state.selectedMachineCapabilities["y"];
    }
    if ("z" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["z"];
    }
  }

  clearHardwareClassificationData() {
    this.clearXYZValue();
    let hardware = this.state.selected_hardware;
    hardware.id = "";
    hardware.color = "";
    hardware.classification_name = "";
    hardware.title = "";
    this.setState({
      selected_hardware: hardware,
    });
    if ("controller_list" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["controller_list"];
    }
    if ("material_capability" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["material_capability"];
    }
    if ("model_description" in this.state.selectedMachineCapabilities) {
      delete this.state.selectedMachineCapabilities["model_description"];
    }
  }

  handleClassification = (event) => {
    this.clearXYZValue();
    console.log("total event:  ", event.target.value);
    let index = event.target.value;
    let details = this.state.hardware_classifications[index];
    let machineDetails = this.state.selectedMachineCapabilities;
    machineDetails.classification = details.id;
    machineDetails["selectedClassificationName"] = details.classification_name;
    this.setState({
      selectedMachineCapabilities: machineDetails,
      selected_hardware: this.state.hardware_classifications[index],
    });
  };

  onSelectSpecialism = (selectedList, selectedItem) => {
    let selectedSpecialism = this.state.selectedSpecialism;
    let selectedSpecialismName = this.state.selectedSpecialismName;
    selectedSpecialism.push(selectedItem.id);
    selectedSpecialismName.push(selectedItem.specialism_name);
    this.setState({
      selectedSpecialism: selectedSpecialism,
      selectedSpecialismName: selectedSpecialismName,
    });
  };
  onRemoveSpecialism = (selectedList, removedItem) => {
    let selectedSpecialism = this.state.selectedSpecialism;
    let selectedSpecialismName = this.state.selectedSpecialismName;
    for (let i = 0; i < selectedSpecialism.length; i++) {
      if (selectedSpecialism[i] == removedItem.id) {
        selectedSpecialism.splice(i, 1);
      }
    }
    for (let i = 0; i < selectedSpecialismName.length; i++) {
      if (selectedSpecialismName[i] == removedItem.specialism_name) {
        selectedSpecialismName.splice(i, 1);
      }
    }
    this.setState({
      selectedSpecialism: selectedSpecialism,
      selectedSpecialismName: selectedSpecialismName,
    });
  };

  onSelectDesign = (selectedList, selectedItem) => {
    let selectedDesign = this.state.selectedDesign;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    selectedDesign.push(selectedItem.id);
    selectedSoftwareName.push(selectedItem.software_name);
    this.setState({
      selectedDesign: selectedDesign,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  onRemoveDesign = (selectedList, removedItem) => {
    let selectedDesign = this.state.selectedDesign;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    for (let i = 0; i < selectedDesign.length; i++) {
      if (selectedDesign[i] == removedItem.id) {
        selectedDesign.splice(i, 1);
      }
    }
    for (let i = 0; i < selectedSoftwareName.length; i++) {
      if (selectedSoftwareName[i] == removedItem.software_name) {
        selectedSoftwareName.splice(i, 1);
      }
    }
    this.setState({
      selectedDesign: selectedDesign,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  onSelectInspection = (selectedList, selectedItem) => {
    let selectedInspection = this.state.selectedInspection;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    selectedInspection.push(selectedItem.id);
    selectedSoftwareName.push(selectedItem.software_name);
    this.setState({
      selectedInspection: selectedInspection,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  onRemoveInspection = (selectedList, removedItem) => {
    let selectedInspection = this.state.selectedInspection;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    for (let i = 0; i < selectedInspection.length; i++) {
      if (selectedInspection[i] == removedItem.id) {
        selectedInspection.splice(i, 1);
      }
    }
    for (let i = 0; i < selectedSoftwareName.length; i++) {
      if (selectedSoftwareName[i] == removedItem.software_name) {
        selectedSoftwareName.splice(i, 1);
      }
    }
    this.setState({
      selectedInspection: selectedInspection,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  onSelectPrograming = (selectedList, selectedItem) => {
    let selectedPrograming = this.state.selectedPrograming;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    selectedPrograming.push(selectedItem.id);
    selectedSoftwareName.push(selectedItem.software_name);
    this.setState({
      selectedPrograming: selectedPrograming,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  onRemovePrograming = (selectedList, removedItem) => {
    let selectedPrograming = this.state.selectedPrograming;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    for (let i = 0; i < selectedPrograming.length; i++) {
      if (selectedPrograming[i] == removedItem.id) {
        selectedPrograming.splice(i, 1);
      }
    }
    for (let i = 0; i < selectedSoftwareName.length; i++) {
      if (selectedSoftwareName[i] == removedItem.software_name) {
        selectedSoftwareName.splice(i, 1);
      }
    }
    this.setState({
      selectedPrograming: selectedPrograming,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  onSelectSimulation = (selectedList, selectedItem) => {
    let selectedSimulation = this.state.selectedSimulation;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    selectedSimulation.push(selectedItem.id);
    selectedSoftwareName.push(selectedItem.software_name);
    this.setState({
      selectedSimulation: selectedSimulation,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  onRemoveSimulation = (selectedList, removedItem) => {
    let selectedSimulation = this.state.selectedSimulation;
    let selectedSoftwareName = this.state.selectedSoftwareName;
    for (let i = 0; i < selectedSimulation.length; i++) {
      if (selectedSimulation[i] == removedItem.id) {
        selectedSimulation.splice(i, 1);
      }
    }
    for (let i = 0; i < selectedSoftwareName.length; i++) {
      if (selectedSoftwareName[i] == removedItem.software_name) {
        selectedSoftwareName.splice(i, 1);
      }
    }
    this.setState({
      selectedSimulation: selectedSimulation,
      selectedSoftwareName: selectedSoftwareName,
    });
  };
  setDrillDepth = (e) => {
    let drillDepth = this.state.selectedMachineCapabilities;
    drillDepth.drillDepth = e.target.value;
    this.setState({
      selectedMachineCapabilities: drillDepth,
    });
  };
  setXYZvalue = (e) => {
    let tempCapabilities = { ...this.state.selectedMachineCapabilities };
    tempCapabilities[e.target.name] = e.target.value;
    this.setState({
      selectedMachineCapabilities: tempCapabilities,
    });
  };
  setManufacturerValue = (e) => {
    let tempCapabilities = { ...this.state.selectedMachineCapabilities };
    let details = this.state.manufacturelist[e.target.value];
    tempCapabilities["manufacturer"] = details.id;
    tempCapabilities["manufacturer_name"] = details.manufacturer_name;
    this.setState({
      selectedMachineCapabilities: tempCapabilities,
    });
  };
  setMaterialSuitabilityValue = (e) => {
    let tempCapabilities = { ...this.state.selectedMachineCapabilities };
    let details = this.state.materialSuitabilityList[e.target.value];
    tempCapabilities["material_capability"] = details.id;
    tempCapabilities["material_capability_name"] =
      details.material_capability_name;
    this.setState({
      selectedMachineCapabilities: tempCapabilities,
    });
  };
  setControllerValue = (e) => {
    let tempCapabilities = { ...this.state.selectedMachineCapabilities };
    let details = this.state.controllerList[e.target.value];
    tempCapabilities["controller_list"] = details.id;
    tempCapabilities["controller_name"] = details.controller_name;
    this.setState({
      selectedMachineCapabilities: tempCapabilities,
    });
  };
  setGuageValue = (e) => {
    let gaugeDetails = this.state.selectedMachineCapabilities;
    gaugeDetails.gauge = e.target.value;
    this.setState({
      selectedMachineCapabilities: gaugeDetails,
    });
  };
  setMaxComponentValue = (e) => {
    let maxComponent = { ...this.state.selectedMachineCapabilities };
    maxComponent[e.target.name] = e.target.value;
    this.setState({
      selectedMachineCapabilities: maxComponent,
    });
  };

  submitPressed = () => {
    this.setState({
      showPopup: !this.state.showPopup,
    });
  };
  closeAndSubmitPressed = () => {
    this.setState(
      {
        showPopup: !this.state.showPopup,
      },
      () => {
        if (this.state.capabilityType === "People") {
          alert("People");
          console.log("this.state.selectedPeopleCapabilities:  ", this.state.selectedPeopleCapabilities)
          //this.saveCapabilities(this.state.selectedPeopleCapabilities, 'People');
        } else {
          alert("Machine");
          // this.callMachineAPI();
        }
      }
    );
  };

  // all APIs call here

  get_skillListItems(type) {
    apiCallWithToken(BASE_API_URL + "web/skilllist/", "post", {
      skill_type: type,
    })
      .then((response) => {
        if (type === "people") {
          this.setState({
            skill_list: response.data.allskill,
          });
        } else {
          this.setState({
            machine_capabilities: response.data.allskill,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  get_specilizationAndSoftwareList(parent_id) {
    if (parent_id === "7") {
    } else {
      this.clearSoftwareData();
    }

    apiCallWithToken(BASE_API_URL + "web/peoplefetch/", "post", {
      parent_id: parent_id,
    })
      .then((response) => {
        var allData = response.data.softwares;
        if (allData.length > 0) {
          for (var i = 0; i < allData.length; i++) {
            let details = allData[i];
            if (details.software_type === "Design Software") {
              this.setState({
                designSoftware_list: [
                  ...this.state.designSoftware_list,
                  details,
                ],
              });
            } else if (details.software_type === "Simulation Software") {
              this.setState({
                simulationSoftware_list: [
                  ...this.state.simulationSoftware_list,
                  details,
                ],
              });
            } else if (details.software_type === '"Programming Software') {
              this.setState({
                programingSoftware_list: [
                  ...this.state.programingSoftware_list,
                  details,
                ],
              });
            } else {
              this.setState({
                inspectionSoftware_list: [
                  ...this.state.inspectionSoftware_list,
                  details,
                ],
              });
            }
          }
        }
        if (parent_id === "7") {
          this.setState({
            machine_specialism: response.data.specialism,
          });
        } else {
          this.setState({
            specialisation_list: response.data.specialism,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  get_manufacturelist(id) {
    apiCallWithToken(BASE_API_URL + "web/manufacturelist/", "post", {
      parent_id: id,
    })
      .then((response) => {
        this.setState({
          manufacturelist: response.data.manufacturers,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  get_hardwareClassification(id) {
    this.setState({
      hardware_classifications: [],
    });
    apiCallWithToken(BASE_API_URL + "web/classificationlist/", "post", {
      parent_id: id,
    })
      .then((response) => {
        console.log(
          "hardware_classifications:  ",
          response.data.classifications.length
        );
        this.setState({
          hardware_classifications: response.data.classifications,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  get_controllerList() {
    getApi(BASE_API_URL + "web/controllerlist/")
      .then((response) => {
        this.setState({
          controllerList: response.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  get_materialSuitability() {
    getApi(BASE_API_URL + "web/materiallist/")
      .then((response) => {
        this.setState({
          materialSuitabilityList: response.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  saveCapabilities = (capabilityData, capabilityType) => {
    console.log("capabilityData:  ", capabilityData);
    console.log("capabilityType:  ", capabilityType);

    let details = {
      0: capabilityData,
    };
    postApiCallWithToken(BASE_API_URL + "web/registercapabilities/", "post", {
      user_id: localStorage.getItem("userId"),
      full_data: details,
    })
      .then((response) => {
        console.log(response);
        alert("User capabilities are successfully stored.");
        if (capabilityType === "People") {
          this.clearPeopleData();
        } else {
          this.clearHardwareClassificationData();
          let machineCapabilities = this.state.selectedMachineCapabilities;
          machineCapabilities.capability_type = "Machine";
          machineCapabilities.machine_capability_type = "";
          machineCapabilities.classification = "5";
          // machineCapabilities.machining_envelopes="1";
          this.setState({
            selectedMachineCapabilitiesName: "",
            selectedMachineCapabilities: machineCapabilities,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    const { plainArray, selectedValues } = this.state;
    return (
      <section className="dashboard">
        <div className="gray-body">
          <Container fluid>
            <Row>
              <Col lg={12} className="pl-md-0">
                <div className="center-body" id="centerbody">
                  <DashboardSideBar />
                  <div id="contentcenter" className="sideNavOpen">
                    <div className="centercontent">
                      <Container fluid>
                        <div className="filter-green">
                          <h6 className="text-green mb-0">Capabilities</h6>
                        </div>
                        <div className="subcon">
                          {/* 11 */}
                          <Row className="mt-4">
                            <Col md="6">
                              <FormGroup>
                                <Label className="pl-0">
                                  How are you interested to work?
                                </Label>
                                <div className="btn-flex d-flex">
                                  <div className="pr-2">
                                    <Input
                                      type="radio"
                                      id="cr1"
                                      name="interest"
                                      className="custom-control-input"
                                      onChange={(e) =>
                                        this.handleHowInterestedSelection(e)
                                      }
                                      value="Remote"
                                    />
                                    <Label className="btn-radio" for="cr1">
                                      Remote{" "}
                                    </Label>
                                  </div>
                                  <div className="pr-2">
                                    <Input
                                      type="radio"
                                      id="cr2"
                                      name="interest"
                                      className="custom-control-input"
                                      onChange={(e) =>
                                        this.handleHowInterestedSelection(e)
                                      }
                                      value="On-Site"
                                    />
                                    <Label className="btn-radio" for="cr2">
                                      On-Site
                                    </Label>
                                  </div>
                                  <div className="pr-2">
                                    <Input
                                      type="radio"
                                      id="cr3"
                                      name="interest"
                                      className="custom-control-input"
                                      onChange={(e) =>
                                        this.handleHowInterestedSelection(e)
                                      }
                                      value="Flexible"
                                    />
                                    <Label className="btn-radio" for="cr3">
                                      Flexible
                                    </Label>
                                  </div>
                                </div>
                              </FormGroup>
                            </Col>
                            {this.state.howInterested === "On-Site" ||
                            this.state.howInterested === "Flexible" ? (
                              <Col md="6">
                                <FormGroup>
                                  <Label>Willing-to-Travel Radius</Label>
                                  <div
                                    className="form-input py-0"
                                    style={{ width: "200px" }}
                                  >
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="Radius"
                                      onChange={(e) => this.getRadious(e)}
                                    />
                                    <span className="mm">mm</span>
                                  </div>
                                </FormGroup>
                              </Col>
                            ) : null}
                            <Col md="12">
                              <FormGroup>
                                <label className="pl-0">Skill</label>
                                <div className="btn-flex d-flex">
                                  {this.state.skill_list.map(
                                    (skills, index) => (
                                      <div className="pr-2">
                                        <Input
                                          type="radio"
                                          id={skills.id}
                                          className="custom-control-input"
                                          name="skills"
                                          onChange={(e) =>
                                            this.getSelectedSkillItem(e)
                                          }
                                          value={skills.skill_name}
                                        />
                                        <Label
                                          className="btn-radio"
                                          for={skills.id}
                                        >
                                          {skills.skill_name}{" "}
                                        </Label>
                                      </div>
                                    )
                                  )}
                                </div>
                              </FormGroup>
                            </Col>
                            {this.state.specialisation_list.length > 0 ? (
                              <Col md="12">
                                <FormGroup>
                                  <Label className="pl-0">Specialism </Label>
                                  <Multiselect
                                    options={this.state.specialisation_list}
                                    displayValue="specialism_name"
                                    key="Specialism"
                                    onSelect={this.onSelectSpecialism}
                                    onRemove={this.onRemoveSpecialism}
                                  />
                                </FormGroup>
                              </Col>
                            ) : null}
                            {this.state.designSoftware_list.length > 0 ? (
                              <Col md="12">
                                <FormGroup>
                                  <Label className="pl-0">
                                    Design Software{" "}
                                  </Label>
                                  <Multiselect
                                    options={this.state.designSoftware_list}
                                    displayValue="software_name"
                                    onSelect={this.onSelectDesign}
                                    onRemove={this.onRemoveDesign}
                                  />
                                </FormGroup>
                                <div className="other-container">
                                  <Label className="pl-0">Other </Label>
                                  <TagInput
                                    tags={this.state.OtherItems}
                                    onTagsChanged={this.onTagsChanged}
                                    addTagOnEnterKeyPress={false}
                                    placeholder="Add new item"
                                    tagStyle={`background: #fbfbfb;`}
                                    hideInputPlaceholderTextIfTagsPresent={
                                      false
                                    }
                                  />
                                </div>
                              </Col>
                            ) : null}

                            {this.state.programingSoftware_list.length > 0 ? (
                              <Col md="12">
                                <FormGroup>
                                  <Label className="pl-0">
                                    Programming Software{" "}
                                  </Label>
                                  <Multiselect
                                    options={this.state.programingSoftware_list}
                                    displayValue="software_name"
                                    onSelect={this.onSelectPrograming}
                                    onRemove={this.onRemovePrograming}
                                  />
                                </FormGroup>
                              </Col>
                            ) : null}
                            {this.state.simulationSoftware_list.length > 0 ? (
                              <Col md="12">
                                <FormGroup>
                                  <Label className="pl-0">
                                    Simulation Software{" "}
                                  </Label>
                                  <Multiselect
                                    options={this.state.simulationSoftware_list}
                                    displayValue="software_name"
                                    onSelect={this.onSelectSimulation}
                                    onRemove={this.onRemoveSimulation}
                                  />
                                </FormGroup>
                              </Col>
                            ) : null}
                            {this.state.inspectionSoftware_list.length > 0 ? (
                              <Col md="12">
                                <FormGroup>
                                  <Label className="pl-0">
                                    Inspection Software{" "}
                                  </Label>
                                  <Multiselect
                                    options={this.state.inspectionSoftware_list}
                                    displayValue="software_name"
                                    onSelect={this.onSelectInspection}
                                    onRemove={this.onRemoveInspection}
                                  />
                                </FormGroup>
                              </Col>
                            ) : null}
                          </Row>

                          <div className="bg-white d-flex justify-content-center py-4">
                            <div className="d-md-flex justify-content-center align-items-center">
                              <Label className="h6 mb-0">
                                Add more people capability requirements?
                              </Label>
                              <div className="d-flex ml-3">
                                <Button
                                  className="btn btn-green mr-3"
                                  onClick={this.addMorePeopleCheck}
                                  value="yes"
                                >
                                  Yes
                                </Button>
                                {/* <Button className="btn btn-black" onClick={this.addMorePeopleCheck} value='no'>No</Button> */}
                              </div>
                            </div>
                          </div>

                          <div className="capability-tab common-padd">
                            <div className="in-basic">
                              <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                                <h2 className=" text-green">
                                  Machine{" "}
                                  <span className="font-weight-exlight">
                                    Capabilities
                                  </span>{" "}
                                </h2>
                                <span>
                                  <img
                                    src={ImagePath.loaderCircle}
                                    className="img-fluid rotating"
                                    alt="loader"
                                  />
                                </span>
                              </div>
                              <Row>
                                <Col md="12">
                                  <FormGroup>
                                    <label className="pl-0">Select</label>
                                    <div className="btn-flex d-flex">
                                      {this.state.machine_capabilities.map(
                                        (machine, index) => (
                                          <div className="pr-2">
                                            <input
                                              type="radio"
                                              id={machine.id}
                                              name="machine"
                                              className="custom-control-input"
                                              onChange={(e) =>
                                                this.getSelectedMachineCapabilities(
                                                  e
                                                )
                                              }
                                              value={machine.skill_name}
                                            />
                                            <label
                                              className="btn-radio"
                                              for={machine.id}
                                            >
                                              {machine.skill_name}{" "}
                                            </label>
                                          </div>
                                        )
                                      )}
                                    </div>
                                  </FormGroup>
                                </Col>
                                {this.state.selectedMachineCapabilitiesName ===
                                  "Subcontract Inspection" ||
                                this.state.selectedMachineCapabilitiesName ===
                                  "Subcontract CNC Machining" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Hardware ClassNameification</Label>
                                      <div className="form-input">
                                        <select
                                          className="form-control"
                                          name="hardware"
                                          onChange={this.handleClassification}
                                        >
                                          {this.state.hardware_classifications.map(
                                            (hardwares, index) => (
                                              <>
                                                <option
                                                  value="none"
                                                  selected
                                                  disabled
                                                  hidden
                                                >
                                                  Select
                                                </option>
                                                <option
                                                  key={index}
                                                  value={index}
                                                >
                                                  {
                                                    hardwares.classification_name
                                                  }{" "}
                                                </option>
                                              </>
                                            )
                                          )}
                                        </select>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                ) : (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label className="pl-0">
                                        Specialism{" "}
                                      </Label>
                                      <Multiselect
                                        options={this.state.machine_specialism}
                                        displayValue="specialism_name"
                                      />
                                      {/* <Multiselect options={this.state.specialisation_list} displayValue='specialism_name' key="Specialism" onSelect={this.onSelectSpecialism} onRemove={this.onRemoveSpecialism}/> */}
                                    </FormGroup>
                                  </Col>
                                )}
                                {this.state.selected_hardware.color ===
                                "green" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Milling Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.green.position.map(
                                        (allPositions, index) => (
                                          <Col lg="2" className="col-auto">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  onKeyDown={this.formatInput}
                                                  name={allPositions.title}
                                                  className="form-control"
                                                  placeholder={
                                                    allPositions.title
                                                  }
                                                  onChange={this.setXYZvalue}
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "golden" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Max Component Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.green.position.map(
                                        (allPositions, index) => (
                                          <Col lg="2" className="col-auto">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  name={allPositions.title}
                                                  placeholder={
                                                    allPositions.title
                                                  }
                                                  onChange={(e) =>
                                                    this.setXYZvalue(e)
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "purple" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Max Printing Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.green.position.map(
                                        (allPositions, index) => (
                                          <Col lg="2" className="col-auto">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  name={allPositions.title}
                                                  placeholder={
                                                    allPositions.title
                                                  }
                                                  onChange={(e) =>
                                                    this.setXYZvalue(e)
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "black" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Dimensional Capabilities</Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.black.position.map(
                                        (allPositions, index) => (
                                          <Col lg="2" className="col-auto">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  name={allPositions.title}
                                                  placeholder={
                                                    allPositions.title
                                                  }
                                                  onChange={(e) =>
                                                    this.setXYZvalue(e)
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "black" ? (
                                  <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                      <Label>Gauge </Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.black.inputField.map(
                                        (myInput, index) => (
                                          <Col md="6">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  placeholder={myInput.title}
                                                  name="gauge"
                                                  onChange={this.setGuageValue}
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}

                                {this.state.selected_hardware.color ===
                                "gray" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Component Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.gray.position.map(
                                        (allPositions, index) => (
                                          <Col lg="2" className="col-auto">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  name={allPositions.title}
                                                  placeholder={
                                                    allPositions.title
                                                  }
                                                  onChange={(e) =>
                                                    this.setXYZvalue(e)
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "gray" ? (
                                  <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                      <Label>Drill Depth </Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.gray.inputField.map(
                                        (myInput, index) => (
                                          <Col md="6">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  name="max_drill_depth"
                                                  onKeyDown={this.formatInput}
                                                  placeholder={myInput.title}
                                                  onChange={this.setDrillDepth}
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "yellow" ? (
                                  <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                      <Label>Turning Envelope </Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.yellow.inputField.map(
                                        (myInput, index) => (
                                          <Col md="6">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  placeholder={myInput.title}
                                                  name={myInput.key}
                                                  onChange={
                                                    this.setMaxComponentValue
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}

                                {this.state.selected_hardware.color ===
                                "red" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Milling Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.red.position.map(
                                        (allPositions, index) => (
                                          <Col lg="2" className="col-auto">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  name={allPositions.title}
                                                  placeholder={
                                                    allPositions.title
                                                  }
                                                  onChange={(e) =>
                                                    this.setXYZvalue(e)
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "red" ? (
                                  <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                      <Label>Turning Envelope </Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.red.inputField.map(
                                        (myInput, index) => (
                                          <Col md="6">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  placeholder={myInput.title}
                                                  name={myInput.key}
                                                  onChange={
                                                    this.setMaxComponentValue
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {this.state.selected_hardware.color ===
                                "cyan" ? (
                                  <Col md="12">
                                    <FormGroup>
                                      <Label>Milling Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                      {this.state.green.position.map(
                                        (allPositions, index) => (
                                          <Col lg="2" className="col-auto">
                                            <FormGroup>
                                              <div className="form-input">
                                                <Input
                                                  type="number"
                                                  className="form-control"
                                                  onKeyDown={this.formatInput}
                                                  name={allPositions.title}
                                                  placeholder={
                                                    allPositions.title
                                                  }
                                                  onChange={(e) =>
                                                    this.setXYZvalue(e)
                                                  }
                                                />
                                                <span className="mm">mm</span>
                                              </div>
                                            </FormGroup>
                                          </Col>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {/* <Col md="12">
                                    <FormGroup>
                                        <Label>Milling Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                        <Col lg="2" className="col-auto">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="text" className="form-control" placeholder="X" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col lg="2" className="col-auto">
                                            <FormGroup>
                                                <div className="form-input" >
                                                    <Input type="text" className="form-control" placeholder="Y" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col lg="2" className="col-auto">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="text" className="form-control" placeholder="Z" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col> */}
                                {/* <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                        <Label>Turning Envelope </Label>
                                    </FormGroup>
                                    <Row>
                                        <Col md="6">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="text" className="form-control" placeholder="Max Component Length" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>

                                        <Col md="6">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="text" className="form-control" placeholder="Max Component Diameter" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col> */}
                                {this.state.selectedMachineCapabilitiesName ===
                                "Subcontract Inspection" ? (
                                  <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                      <Label>Manufacturer</Label>
                                    </FormGroup>
                                    <Row>
                                      <Col md="6">
                                        <FormGroup>
                                          <div className="form-input">
                                            <select
                                              className="form-control"
                                              name="manufacturer"
                                              onChange={
                                                this.setManufacturerValue
                                              }
                                            >
                                              {this.state.manufacturelist.map(
                                                (manufacture, index) => (
                                                  <>
                                                    <option
                                                      value="none"
                                                      selected
                                                      disabled
                                                      hidden
                                                    >
                                                      Select
                                                    </option>
                                                    <option
                                                      key={index}
                                                      value={index}
                                                    >
                                                      {
                                                        manufacture.manufacturer_name
                                                      }
                                                    </option>
                                                  </>
                                                )
                                              )}
                                            </select>
                                          </div>
                                        </FormGroup>
                                        <div className="manufacture-box">
                                          <Label className="pl-0">Other </Label>
                                          <div className="manufacture-input-box">
                                            <input
                                              type="text"
                                              className="input-box"
                                              placeholder="Other"
                                              name='manufacture-cnc'
                                            />
                                          </div>
                                        </div>                                        
                                      </Col>
                                      {/* <Col md="8" lg="6">
                                        <FormGroup>
                                        <Row>
                                            <Col sm="5">
                                            <div className="form-input p-0">
                                                <button type="button" className="btn btn-block btn-green-roound">Other <FontAwesomeIcon icon={faPlusCircle} className="mt-1" /></button>
                                            </div>
                                            </Col>
                                            <Col sm="5">
                                            <div className="form-input">
                                                <Input type="text" placeholder="enter here.." />
                                            </div>
                                            </Col>
                                            <Col sm="2" className="pl-0 pr-md-3">
                                            <button className="btn btn-link" type="button"> <span className="icon-wr ml-0 bg-light"><FontAwesomeIcon icon={faCheck} /></span></button>
                                            </Col>
                                        </Row>
                                        </FormGroup>
                                    </Col> */}
                                    </Row>

                                    <Row>
                                      <Col md="6">
                                        <FormGroup>
                                          <Label>Model</Label>
                                          <div className="form-input">
                                            <Input
                                              type="text"
                                              className="form-control "
                                              placeholder="Modal"
                                              name="model_description"
                                              onChange={this.setXYZvalue}
                                            />
                                            <span className="icon-wr">
                                              <FontAwesomeIcon
                                                icon={faSortNumericDownAlt}
                                              />
                                            </span>
                                          </div>
                                        </FormGroup>
                                      </Col>
                                    </Row>
                                  </Col>
                                ) : this.state
                                    .selectedMachineCapabilitiesName ===
                                  "Subcontract CNC Machining" ? (
                                  <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                      <Label>Manufacturer</Label>
                                    </FormGroup>
                                    <Row>
                                      <Col md="6">
                                        <FormGroup>
                                          <div className="form-input">
                                            <select
                                              className="form-control"
                                              name="manufacturer"
                                              onChange={
                                                this.setManufacturerValue
                                              }
                                            >
                                              {this.state.manufacturelist.map(
                                                (manufacture, index) => (
                                                  <>
                                                    <option
                                                      value="none"
                                                      selected
                                                      disabled
                                                      hidden
                                                    >
                                                      Select
                                                    </option>
                                                    <option
                                                      key={index}
                                                      value={index}
                                                    >
                                                      {
                                                        manufacture.manufacturer_name
                                                      }
                                                    </option>
                                                  </>
                                                )
                                              )}
                                            </select>
                                          </div>
                                        </FormGroup>
                                        <div className="manufacture-box">
                                          <Label className="pl-0">Other </Label>
                                          {/* <TagInput
                                            tags={this.state.singleItems}
                                            onTagsChanged={this.onSingleTagsChanged}
                                            addTagOnEnterKeyPress={false}
                                            placeholder="Add new item"
                                            tagStyle={`background: #fbfbfb;`}
                                            hideInputPlaceholderTextIfTagsPresent={
                                              false
                                            }
                                          /> */}
                                          <div className="manufacture-input-box">
                                            <input
                                              type="text"
                                              className="input-box"
                                              placeholder="Other"
                                              name='manufacture-inspection'
                                            />
                                          </div>
                                        </div>
                                      </Col>
                                      {/* <Col md="8" lg="6">
                                        <FormGroup>
                                        <Row>
                                            <Col sm="5">
                                            <div className="form-input p-0">
                                                <button type="button" className="btn btn-block btn-green-roound">Other <FontAwesomeIcon icon={faPlusCircle} className="mt-1" /></button>
                                            </div>
                                            </Col>
                                            <Col sm="5">
                                            <div className="form-input">
                                                <Input type="text" placeholder="enter here.." />
                                            </div>
                                            </Col>
                                            <Col sm="2" className="pl-0 pr-md-3">
                                            <button className="btn btn-link" type="button"> <span className="icon-wr ml-0 bg-light"><FontAwesomeIcon icon={faCheck} /></span></button>
                                            </Col>
                                        </Row>
                                        </FormGroup>
                                    </Col> */}
                                    </Row>
                                    <Row>
                                      <Col md="6">
                                        <FormGroup>
                                          <Label>Model</Label>
                                          <div className="form-input">
                                            <Input
                                              type="text"
                                              className="form-control "
                                              placeholder="Modal"
                                              name="model_description"
                                              onChange={this.setXYZvalue}
                                            />
                                            <span className="icon-wr">
                                              <FontAwesomeIcon
                                                icon={faSortNumericDownAlt}
                                              />
                                            </span>
                                          </div>
                                        </FormGroup>
                                      </Col>
                                      <Col md="6">
                                        <FormGroup>
                                          <label>Material Suitability</label>
                                          <div className="form-input">
                                            <select
                                              className="form-control"
                                              name="Material"
                                              name="material_capability"
                                              onChange={
                                                this.setMaterialSuitabilityValue
                                              }
                                            >
                                              {this.state.materialSuitabilityList.map(
                                                (Material, index) => (
                                                  <>
                                                    <option
                                                      value="none"
                                                      selected
                                                      disabled
                                                      hidden
                                                    >
                                                      Select
                                                    </option>
                                                    <option
                                                      key={index}
                                                      value={index}
                                                    >
                                                      {
                                                        Material.material_capability_name
                                                      }
                                                    </option>
                                                  </>
                                                )
                                              )}
                                            </select>
                                          </div>
                                        </FormGroup>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col xs="12" sm="12" md="12" lg="12">
                                        <FormGroup>
                                          <Label>Controller</Label>
                                        </FormGroup>
                                      </Col>
                                      <Col md="6">
                                        <FormGroup>
                                          <div className="form-input">
                                            <select
                                              className="form-control"
                                              name="controller_list"
                                              onChange={this.setControllerValue}
                                            >
                                              {this.state.controllerList.map(
                                                (Controller, index) => (
                                                  <>
                                                    <option
                                                      value="none"
                                                      selected
                                                      disabled
                                                      hidden
                                                    >
                                                      Select
                                                    </option>
                                                    <option
                                                      key={index}
                                                      value={index}
                                                    >
                                                      {
                                                        Controller.controller_name
                                                      }
                                                    </option>
                                                  </>
                                                )
                                              )}
                                            </select>
                                          </div>
                                        </FormGroup>
                                        {/* <div className="other-container other-container-manufacture">
                                          <Label className="pl-0">Other </Label>
                                          <TagInput
                                            tags={this.state.singleItems}
                                            onTagsChanged={
                                              this.onSingleTagsChanged
                                            }
                                            addTagOnEnterKeyPress={false}
                                            placeholder="Add new item"
                                            tagStyle={`background: #fbfbfb;`}
                                            hideInputPlaceholderTextIfTagsPresent={
                                              false
                                            }
                                          />
                                        </div> */}
                                         <div className="manufacture-box">
                                          <Label className="pl-0">Other </Label>
                                          <div className="manufacture-input-box">
                                            <input
                                            name='controller-cnc'
                                              type="text"
                                              className="input-box"
                                              placeholder="Other"
                                            />
                                          </div>
                                        </div>

                                      </Col>
                                      {/* <Col md="6">
                                        <FormGroup>
                                        <div className="d-flex align-items-center">
                                            <div className="form-input mr-1">
                                            <Input type="text" className="form-control " placeholder="" />
                                            </div>
                                            <button className="btn btn-link" type="button"> <span className="icon-wr"><FontAwesomeIcon icon={faTimesCircle} className="text-black fa-2x" /></span></button>
                                        </div>
                                        </FormGroup>
                                    </Col> */}
                                    </Row>
                                  </Col>
                                ) : null}
                                {/* <Col xs="12" sm="12" md="12" lg="12">
                                    <FormGroup>
                                        <Label>Manufacturer</Label>
                                    </FormGroup>
                                    <Row>
                                        <Col md="6">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="select" className="form-control">
                                                        <option>Hexagon</option>
                                                        <option>Traingle</option>
                                                    </Input>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col md="8" lg="6">
                                            <FormGroup>
                                                <Row>
                                                    <Col sm="5">
                                                        <div className="form-input p-0">
                                                            <button type="button" className="btn btn-block btn-green-roound">Other <FontAwesomeIcon icon={faPlusCircle} className="mt-1" /></button>
                                                        </div>
                                                    </Col>
                                                    <Col sm="5">
                                                        <div className="form-input py-0">
                                                            <Input type="text" placeholder="enter here.." />
                                                        </div>
                                                    </Col>
                                                    <Col sm="2" className="pl-0 pr-md-3">
                                                        <button className="btn btn-link" type="button"> <span className="icon-wr ml-0 bg-light"><FontAwesomeIcon icon={faCheck} /></span></button>
                                                    </Col>
                                                </Row>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md="6">
                                    <FormGroup>
                                        <Label>Model</Label>
                                        <div className="form-input">
                                            <Input type="text" className="form-control " placeholder="Modal" />
                                            <span className="icon-wr"><FontAwesomeIcon icon={faSortNumericDownAlt} /></span>
                                        </div>
                                    </FormGroup>
                                </Col>
                                <Col md="6">
                                    <FormGroup>
                                        <label>Material Suitability</label>
                                        <div className="form-input">
                                            <Input type="select">
                                                <option>Medium (Aluminium, Mild Steel)</option>
                                                <option>Medium (Aluminium, Mild Steel)</option>
                                            </Input>
                                        </div>
                                    </FormGroup>
                                </Col>
                                <Col xs="12" sm="12" md="12" lg="12">
                                    <Row>
                                        <Col xs="12" sm="12" md="12" lg="12">
                                            <FormGroup>
                                                <Label>Controller</Label>
                                            </FormGroup>
                                        </Col>
                                        <Col md="6">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="select">
                                                        <option>Other</option>
                                                        <option>Medium (Aluminium, Mild Steel)</option>
                                                    </Input>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col md="6">
                                            <FormGroup>
                                                <div className="d-flex align-items-center">
                                                    <div className="form-input mr-1">
                                                        <Input type="text" className="form-control " placeholder="" />
                                                    </div>
                                                    <button className="btn btn-link" type="button"> <span className="icon-wr"><FontAwesomeIcon icon={faTimesCircle} className="text-black fa-2x" /></span></button>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col> */}
                              </Row>
                            </div>
                          </div>

                          <div className="bg-white d-flex justify-content-center py-4">
                            <div className="d-md-flex justify-content-center align-items-center">
                              <label className="h6 mb-0">
                                Add more machine capability requirements?
                              </label>
                              <div className="d-flex ml-3">
                                <Button
                                  className="btn btn-green mr-3"
                                  onClick={this.setAllMachineData}
                                >
                                  Yes
                                </Button>
                                {/* <Button className="btn btn-black">No</Button> */}
                              </div>
                            </div>
                          </div>
                          {/* <div className="py-4 d-flex justify-content-end">
                            <Button className="btn btn-dark" type="submit" onClick={this.saveCapabilities}>Continue <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating" /></Button>
                        </div> */}

                          {/* End 11 */}
                        </div>
                      </Container>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
            {this.state.showPopup ? (
              <Popup
                capType={this.state.capabilityType}
                selectedPeopleData={this.state.selectedPeopleCapabilities}
                selectedMachineData={this.state.selectedMachineCapabilities}
                closePopup={this.submitPressed}
                closeAndSubmitPopUp={this.closeAndSubmitPressed}
              />
            ) : null}
          </Container>
        </div>
      </section>
    );
  }
}

export default Capabilities;
