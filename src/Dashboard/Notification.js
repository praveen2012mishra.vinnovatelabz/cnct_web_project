import React, { useCallback, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckSquare } from '@fortawesome/free-regular-svg-icons';
import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,    
} from 'reactstrap';

import DashboardSideBar from './DashboardSideBar';

import { ImagePath } from '../ImagePath';
import './style.css';


const Notification = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');

    const toggle = () => setIsOpen(!isOpen);

    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        } 
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">
                                        
                                    <Container fluid>
                                        <div className="filter-green">
                                            <h6 className="text-green mb-0">Notification</h6>
                                        </div>
                                        <div className="subcon">
                                            <div className="group-notification mb-4">
                                                <h6 className="form-label">Groups & Invitations</h6>
                                                <hr />
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                            </div>
                                            <div className="group-notification mb-4">
                                                <h6 className="form-label">Miscellaneous</h6>
                                                <hr />
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                <p><FontAwesomeIcon icon={faCheckSquare} /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                            </div>
                                        </div>
                                    </Container>
                                        
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default Notification;