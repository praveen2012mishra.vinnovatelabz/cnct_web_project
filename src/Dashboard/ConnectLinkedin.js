import React, { useCallback, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,    
} from 'reactstrap';

import DashboardSideBar from './DashboardSideBar';
import BASE_API_URL from '../Config/config';
import {postApiCallWithToken} from '../Config/api';

import { ImagePath } from '../ImagePath';
import './style.css';



const ConnectLinkedin = (props) => {

    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');
    const [linkedInUrl, setlinkedInUrl] = useState('');

    const toggle = () => setIsOpen(!isOpen);
    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        } 
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    const isUrl = url => {
        var regexp = /(ftp|http|https):\/\/(www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(url);
     }

     const handleLinkedInValue = (e) => {
        setlinkedInUrl(e.target.value)
     }

     const saveLinkedInUrl = () => {
         let userid = localStorage.getItem('userId');
        if (linkedInUrl === '') {
            alert("Please enter URL")
        }else if (!isUrl(linkedInUrl)) {
            alert("Please enter correct address")
        }else {
            //http://111.93.169.90:2030/web/user/[user_id]/updatelinkedin/
            postApiCallWithToken(BASE_API_URL + 'web/user/' + userid + '/updatelinkedin/', 
          'post',
          {'linkedinLink': linkedInUrl}
          )
          .then(response => {
            console.log("updatelinkedin response:  ", response);
            if (response.data.success) {
                alert(response.data.msg);
            }else {
                alert(response.data.msg);
            }
            
          })
          .catch(err => {
          console.log(err);
          alert("Some error occured.");
          })
        }
     }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar  />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">
                                        
                                    <Container fluid>
                                        <div className="filter-green">
                                            <h6 className="text-green mb-0">Connect LinkedIn Account</h6>
                                        </div>
                                        <div className="subcon">
                                            <Row>
                                                <Col xs={12} sm={12} md={12} lg={12}>
                                                    <FormGroup>
                                                    <Label className="form-label">Account Link</Label>
                                                    <div className="form-input bdr-gray ">
                                                        <Input type="text" className="py-0 h-auto" placeholder="Htps : //www. Linkedin.com" onChange={handleLinkedInValue} value={linkedInUrl}/>
                                                        <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faLinkedin} /></span>
                                                    </div>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <FormGroup className="mt-2">
                                                <Button color="secondary" className="btn-dark" type="submit" onClick={saveLinkedInUrl}>Update <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 w30loader  rotating" /></Button>
                                            </FormGroup>
                                        </div>
                                    </Container>
                                        
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default ConnectLinkedin;