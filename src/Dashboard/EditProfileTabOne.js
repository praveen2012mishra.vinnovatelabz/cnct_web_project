import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faCheckSquare } from '@fortawesome/free-regular-svg-icons';
import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,    
    TabContent, TabPane, Nav, NavItem, NavLink, Card,  CardTitle, CardText, Form,
} from 'reactstrap';
import countryList from 'react-select-country-list';
import { ImagePath } from '../ImagePath';
import { faUser, faEnvelope, faAddressBook, faImage, faLock, faBars, faBuilding } from '@fortawesome/free-solid-svg-icons';
import BASE_API_URL from '../Config/config';
import {putApiCallWithToken} from '../Config/api';


const EditProfileTabOne = () => {
const [userDetails, setUserDetails] = useState({});
const [allCountryList, setCountryList] = useState([]);
const [entitySize, setEntitySize] = useState('');
const [checkRegistrationNo, ukRegistrationNoChecking] = useState(false);
const selectedIndusExp = []
const [userImage, updateUserImage] = useState(null);
const [userImageFile, updateUserImageFile] = useState(null);
const [imageName, setImageName] = useState(null);

const allEntitySize = [
    {id: 101, value: 'Individual', shortName: 'IN'},
    {id: 102, value: 'Micro', shortName: 'MI'},
    {id: 103, value: 'Small', shortName: 'SM'},
    {id: 104, value: 'Medium', shortName: 'ME'},
    {id: 105, value: 'Large', shortName: 'LA'},
    {id: 106, value: 'Enterprise', shortName: 'EN'},
]
const allIndusExperience = [
    {id: 110, value: 'Aerospace'},
    {id: 111, value: 'Automotive'},
    {id: 112, value: 'Construction'},
    {id: 113, value: 'Consumer Goods'},
    {id: 114, value: 'Defense'},
    {id: 115, value: 'Electric Vehicles'},
    {id: 116, value: 'Marine'},
    {id: 117, value: 'Medical'},
    {id: 118, value: 'Mining'},
    {id: 119, value: 'Motorsport'},
    {id: 120, value: 'Oil and Gas'},
    {id: 121, value: 'Renewable Energy'},
    {id: 122, value: 'Robotics/AI/Automation'},
]

const handleExpSelection = e => {
    let isChecked = e.target.value;
    console.log('selection check', isChecked);
    selectedIndusExp.push(e.target.value);
    // setselectedIndusExp(e.target.value);
    // do whatever you want with isChecked value
    // if (industries.length > 0){
    //     if (industries.splice(e.target.value));
    // }
};
const handleEntitySelection = e =>{
    setEntitySize(e.target.value);
}

const handleUkRegistrationChecking = async() => { 
    await ukRegistrationNoChecking(!checkRegistrationNo)
  }

const handleInputValue = e => {
    setUserDetails({
      ...userDetails,
      [e.target.name]: e.target.value
    });
  };

  const updateUserBasicInfo = () => {
    let allDetails = {
        name: userDetails.name,
        "avatar":'',
        company_name:userDetails.company_name,
        company_address:userDetails.company_address,
        linkedin_link:userDetails.linkedin_link,
        entity_size:entitySize,
        company_registration_number:userDetails.company_registration_number,
        vat_number:userDetails.vat_number,
        company_bio:userDetails.company_bio,
        customer_experience:userDetails.customer_experience,
        hear_about_us:userDetails.hear_about_us,
        why_us:userDetails.why_us,
        industry_type:selectedIndusExp.join(','),
        company_postcode:userDetails.company_postcode,
        company_country:userDetails.company_country,
      };
      console.log('allDetails:  ', allDetails);
      if (userDetails.companyName === ''){
        alert('Please enter company name');
      }else if (userDetails.name === ''){
          alert('Please enter your name');
      }
      else if (userDetails.companyCountry === ''){
          alert('Please select country');
      }else if (userDetails.postCode === ''){
          alert('Please enter postcode');
      }else if (entitySize === ''){
        alert('Please choose entity size');
      }else {
          //http://111.93.169.90:2030/web/user/[user_id]/editprofile/
          let userid = localStorage.getItem('userId');
          putApiCallWithToken(BASE_API_URL + 'web/user/' + userid + '/editprofile/', 
        'put',
        allDetails
        )
        .then(response => {
          console.log("editprofile response:  ", response);
          if (response.data.success) {
            localStorage.setItem('userToken', response.data.token);
            localStorage.setItem('userId', response.data.user['id']);
            localStorage.setItem('userDetails', JSON.stringify(response.data.user));
            alert(response.data.msg);
          }else {
              alert(response.data.msg);
          }
          
        })
        .catch(err => {
        console.log(err);
        alert("Some error occured.");
        })
      }
  }

useEffect(() => {
    setUserDetails(JSON.parse(localStorage.getItem("userDetails")));
    setCountryList(countryList().getData());
}, [])

const handleFileChange = e => {
    const files = Array.from(e.target.files);
      // data.property_image = files;
      console.log("cheking file type:  ", e.target.file);
      setImageName(e.target.files[0].name)
      Promise.all(
        files.map(file => {
        updateUserImageFile(file);
          return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.addEventListener('load', ev => {
              resolve(ev.target.result);
            });
            reader.addEventListener('error', reject);
            reader.readAsDataURL(file);
          });
        }),
      ).then(
        images => {
            updateUserImage(images);
        },
        error => {
          console.error(error);
        },
      );
  };

    return (
        <React.Fragment>
            <div className="in-basic mt-3">
            <Form>
              <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                <h2 className="fs-20 mb-0 text-green">Headlines</h2>
                <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader"></img></span>
              </div>
              <Row>
              <Col sm="6">
                  <FormGroup>
                      <Label>Company Name </Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Name Here" name='company_name' onChange={handleInputValue} value={userDetails.company_name}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faBuilding} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Name</Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Enter Name" name="name" onChange={handleInputValue} value={userDetails.name}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                    <FormGroup>
                        <Label>Company Country </Label>
                        <div className="form-input">
                            <select className="form-control" name="company_country" onChange={handleInputValue}>
                            {allCountryList.map((country, index) => (
                                <>
                                <option value="none" selected disabled hidden>
                                Select
                              </option>
                              <option key={index} value={country.label}>{country.label}</option>
                                </>
                            ))}                               
                            </select>
                        </div>
                    </FormGroup>
                </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Email </Label>
                      <div className="form-input bg-light">
                          <Input type="email" className="form-control bg-transparent" name="email" placeholder="Enter Email Id" value={userDetails.email}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faEnvelope} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Postcode Lookup</Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Enter Postcode" name="company_postcode" onChange={handleInputValue} value={userDetails.company_postcode}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faAddressBook} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Enter Manually</Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="ex: enter address" name="company_address" onChange={handleInputValue} value={userDetails.company_address}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Company Logo / Profile Photo</Label>
                      <div className="form-input">
                          <Label for="uploadpic" className="btn btn-upload">Upload Image</Label>
                          <Input type="file" id="uploadpic" hidden onChange={e => handleFileChange(e)}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faImage} /></span>
                      </div>
                      
                      <div className="row">
                      
                        <div className="col-md-4">
                        <Label style={{fontSize: 12}} >
                            {imageName}
                            </Label>
                            </div>
                      
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>LinkedIn </Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="http:// www.linkedin.com" name='linkedin_link' onChange={handleInputValue} value={userDetails.linkedin_link}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                      </div>
                  </FormGroup>
              </Col>
              {/* <Col sm="6">
                  <FormGroup>
                      <Label>Password </Label>
                      <div className="form-input bg-light">
                          <Input type="password" disabled className="form-control bg-transparent" placeholder="*******" name="password" value={userDetails.company_name}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faLock} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Re-Enter Password</Label>
                      <div className="form-input bg-light">
                          <Input type="password" disabled className="form-control bg-transparent" placeholder="*******" name="confirmPassword" value={userDetails.company_name}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faLock} /></span>
                      </div>
                  </FormGroup>
              </Col> */}
            </Row>
            <div className="d-flex justify-content-between align-items-center border-bottom pb-3 my-4">
                <h2 className="fs-20 text-green">Legal Entity</h2>
                <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader"></img></span>
            </div>
            
            <Row>
                <Col md="6">
                    <FormGroup>
                        <Label>UK Company Registration Number</Label>
                        <div className="my-4 ml-3">
                            <div className="custom-control custom-checkbox ">
                                <Input type="checkbox" className="custom-control-input" id="ck1" name="vatRegisterChecking" value={checkRegistrationNo} onChange={handleUkRegistrationChecking} defaultChecked={checkRegistrationNo}/>
                                <Label className="custom-control-label" for="ck1">I am not a UK-based limited company, registered with companies house</Label>
                            </div>
                        </div>
                        {checkRegistrationNo===true?
                            <div className="form-input bg-light">
                                <Input type="text" disabled={true} className="form-control bg-transparent" placeholder="Enter reg no." name="company_registration_number" />
                                <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                            </div> : 
                            <div className="form-input">
                                <Input type="text" disabled={false} className="form-control " placeholder="Enter reg no." name="company_registration_number" onChange={handleInputValue} value={userDetails.company_registration_number}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                            </div>
                        }
                        {/* <div className="form-input bg-light">
                            <Input type="text" className="form-control bg-transparent" placeholder="Enter reg no." name="company_registration_number" value={userDetails.company_registration_number}/>
                            <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                        </div> */}
                    </FormGroup>
                </Col>
                <Col md="6">
                    <FormGroup>
                        <Label>VAT Registration Number</Label>
                        <div className="my-4 ml-3">
                            <div className="custom-control custom-checkbox">
                                <Input type="checkbox" className="custom-control-input" id="ck2" name="vatRegisterChecking"/>
                                <Label className="custom-control-label" for="ck2">I am not VAT-registered</Label>
                            </div>
                        </div>
                        <div className="form-input bg-light">
                            <Input type="text" className="form-control bg-transparent" placeholder="Enter reg no." name="vat_number" value={userDetails.vat_number}/>
                            <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                        </div>
                    </FormGroup>
                </Col>
            </Row>
            {/* <div className="my-4 ml-3">
                <div className="custom-control custom-checkbox">
                    <Input type="checkbox" className="custom-control-input" id="ck3" name="checktermsCondition"/>
                    <Label className="custom-control-label" for="ck3">I agree to the terms and conditions of use of the CNCT platform. I will take full responsibility for the appropriate tax declarations relevant to my territory. (These words will be amended to proper legal wording with a pop up to view full Ts and Cs) </Label>
                </div>
            </div> */}
          </Form>
          </div>


          <section className="common-padd bgleft">
                <Container>
                    <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                        <h2 className=" text-green">Details </h2>
                        <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
                    </div>
                    <Row>
                        <Col md="6">
                            <FormGroup>
                                <Label className="pl-0 mb-3">Entity Size </Label>
                                <div>
                                    {allEntitySize.map((entity, index) => (
                                        <div className="custom-control custom-radio">
                                            <Input type="radio" className="custom-control-input" id={entity.id} name="entitySize" value={entity.shortName} onChange={handleEntitySelection}/>
                                            <Label className="custom-control-label" for={entity.id}>{entity.value}</Label>
                                        </div>
                                    ))}
                                </div>
                                {/* <div className="custom-control custom-radio">
                                    <input type="radio" className="custom-control-input" id="r1" name="radio-stacked" />
                                    <label className="custom-control-label" for="r1">Individual</label>
                                  </div>
                                <div className="custom-control custom-radio">
                                    <input type="radio" className="custom-control-input" id="r2" name="radio-stacked" />
                                    <label className="custom-control-label" for="r2">Micro </label>
                                  </div>
                                <div className="custom-control custom-radio">
                                    <input type="radio" className="custom-control-input" id="r3" name="radio-stacked" />
                                    <label className="custom-control-label" for="r3">Small</label>
                                  </div>
                                <div className="custom-control custom-radio">
                                    <input type="radio" className="custom-control-input" id="r4" name="radio-stacked" />
                                    <label className="custom-control-label" for="r4">Medium</label>
                                  </div>
                                <div className="custom-control custom-radio">
                                    <input type="radio" className="custom-control-input" id="r5" name="radio-stacked" />
                                    <label className="custom-control-label" for="r5">Large</label>
                                  </div>
                                <div className="custom-control custom-radio">
                                    <input type="radio" className="custom-control-input" id="r6" name="radio-stacked" />
                                    <label className="custom-control-label" for="r6">Enterprise</label>
                                  </div> */}
                                <span className="d-block text-green">*Company size has no bearing on price </span>
                            </FormGroup>
                        </Col>
                        <Col md="6">
                        <FormGroup>
                            <Label className="pl-0 mb-3">Key Industry Experience </Label>
                            <div className="d-flex flex-wrap">
                            {allIndusExperience.map((experience, index) => (
                                <div className="custom-control custom-checkbox col-xs-12 col-md-6 mb-3">
                                <Input type="checkbox" className="custom-control-input" id={experience.id} value={experience.value} onChange={handleExpSelection}/>
                                <Label className="custom-control-label" for={experience.id}>{experience.value}  </Label>
                            </div>
                            ))}
                            </div>
                                    {/* <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk1" />
                                            <label className="custom-control-label" for="chk1">Automotive </label>
                                          </div>
                                    </div>    
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk2" />
                                            <label className="custom-control-label" for="chk2">Aerospace  </label>
                                          </div>
                                    </div>    
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk3" />
                                            <label className="custom-control-label" for="chk3">Construction  </label>
                                          </div>
                                    </div>  
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk4" />
                                            <label className="custom-control-label" for="chk4">Medical  </label>
                                          </div>
                                    </div>   
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk5" />
                                            <label className="custom-control-label" for="chk5">Consumer Goods   </label>
                                          </div>
                                    </div>   
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk6" />
                                            <label className="custom-control-label" for="chk6">Mining </label>
                                          </div>
                                    </div>   
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk7" />
                                            <label className="custom-control-label" for="chk7">Defense  </label>
                                          </div>
                                    </div>     
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk8" />
                                            <label className="custom-control-label" for="chk8">Motorsport </label>
                                          </div>
                                    </div>     
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk9" />
                                            <label className="custom-control-label" for="chk9">Electric Vehicles </label>
                                          </div>
                                    </div>     
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk10" />
                                            <label className="custom-control-label" for="chk10">Oil and Gas  </label>
                                          </div>
                                    </div>     
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk11" />
                                            <label className="custom-control-label" for="chk11">Marine </label>
                                          </div>
                                    </div>     
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk12" />
                                            <label className="custom-control-label" for="chk12">Renewable Energy  </label>
                                          </div>
                                    </div>     
                                    <div className="col-md-6 mb-3">
                                        <div className="custom-control custom-checkbox ">
                                            <input type="checkbox" className="custom-control-input" id="chk13" />
                                            <label className="custom-control-label" for="chk13">Robotics/AI/Automation </label>
                                          </div>
                                    </div> */}
                            
                            
                        </FormGroup>
                        </Col>
                        
                        
                    </Row>
                    <div className="clearfix">
                        <FormGroup>
                            <Label>Company Bio</Label>
                            <div className="form-input radius70 pl-4">
                                <Input type="textarea" className="form-control" rows="3" placeholder="Write Message Here" name='company_bio' onChange={handleInputValue} value={userDetails.company_bio}/>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label>Blue-Chip Customer Experience</Label>
                            <div className="form-input radius70 pl-4">
                                <Input type="textarea" className="form-control" rows="3" placeholder="Write Message Here" name='customer_experience' onChange={handleInputValue} value={userDetails.customer_experience}/>
                            </div>
                            <span className="text-green d-block pl-3 pt-1">*Please be aware your connections via CNCT may wish for evidence of this work </span>
                        </FormGroup>
                       
                    </div>
                    <div className="py-4 container d-flex justify-content-end">
                        <Button className="btn-dark" type="submit" onClick={updateUserBasicInfo}>Update <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating"/></Button>
                    </div>
                </Container>
            </section>

            <FormGroup className="mt-5">
                <Button color="secondary" className="btn-dark" type="submit">Continue <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 w30loader  rotating" /></Button>
            </FormGroup>

        </React.Fragment>
    )

}

export default EditProfileTabOne;