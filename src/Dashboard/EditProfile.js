import React, { useCallback, useState, useEffect } from 'react';

import {
    Col,
    Container,
    Row,
    Button,
    Label,
    FormGroup,
    Input,    
    TabContent, TabPane, Nav, NavItem, NavLink, Card,  CardTitle, CardText,
} from 'reactstrap';

import DashboardSideBar from './DashboardSideBar';
import EditProfileTabOne from './EditProfileTabOne';


import { ImagePath } from '../ImagePath';
import './style.css';

import classnames from 'classnames';


const EditProfile = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [openSideNav, setOpenSideNav] = useState('sideNavOpen');

    

    const handleMenuBar = () => {
        if (openSideNav === 'sideNavOpen') {
            setOpenSideNav('sideNavClose');
        } 
        if (openSideNav === 'sideNavClose') {
            setOpenSideNav('sideNavOpen');
        }
    }

    const [activeTab, setActiveTab] = useState('1');
    const toggle = tab => {
        if(activeTab !== tab) setActiveTab(tab);
    }

    return (
        <section className="dashboard">
            <div className="gray-body">
                <Container fluid>
                    <Row>
                        <Col lg={12} className="pl-md-0">
                            <div className="center-body" id="centerbody">
                                <DashboardSideBar />
                                <div id="contentcenter" className={openSideNav}>
                                    <div className="centercontent">
                                        
                                    <Container fluid>
                                        <div className="filter-green">
                                            <h6 className="text-green mb-0">Edit Profile</h6>
                                        </div>
                                        <div className="subcon">
                                        <EditProfileTabOne />
                                        </div>
                                    </Container>
                                        
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    )

}

export default EditProfile;