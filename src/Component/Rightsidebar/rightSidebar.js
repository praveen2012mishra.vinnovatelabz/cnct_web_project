import React, { Component, useState, useEffect } from 'react'
import {    
    Container,
    Card,
    CardBody,
    Button
} from 'reactstrap';
import { ImagePath } from '../../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBuilding, faPoundSign } from '@fortawesome/free-solid-svg-icons';
import './barstyle.css'

const RightSidebar = () => {  
        return (
            <section>                
                <div className="right-bar">
                    <Container fluid className="p-0">
                       <div className="mb-4">
                            <Card className="border-0 barcard">
                                <CardBody>
                                    <h6 className="font-weight-light mb-4">JOBS IN BID</h6>
                                    <ul className="list-unstyled">
                                        <li>
                                            <span className="text-jobs-red">Total Number of Jobs in Bid</span>
                                            <span className="numbid">1</span>
                                        </li>
                                        <li>
                                            <span className="text-jobs-red">Total Number of Bids Received</span>
                                            <span className="numbid">30</span>
                                        </li>
                                        <li>
                                            <span className="text-jobs-red">Total Value of Jobs</span>
                                            <span className="numbid"><span class="text-danger mr-1"><FontAwesomeIcon icon={faPoundSign} /></span>3,00,000</span>
                                        </li>
                                    </ul>
                                    <Button className="btn-danger" type="submit">Details <img src={ImagePath.loaderred} alt="loader" className="img-fluid ml-3 rotating" /></Button>
                                </CardBody>
                            </Card>
                       </div>
                       <div className="mb-4">
                            <Card className="border-0 barcard">
                                <CardBody>
                                    <h6 className="font-weight-light mb-4">JOBS IN PROGRESS</h6>
                                    <ul className="list-unstyled">
                                        <li>
                                            <span className="text-jobs-red">Total Number of Jobs in Progress</span>
                                            <span className="numbid">12</span>
                                        </li>                                       
                                        <li>
                                            <span className="text-jobs-red">Total Value of Jobs</span>
                                            <span className="numbid"><span class="text-danger mr-1"><FontAwesomeIcon icon={faPoundSign} /></span>3,00,000</span>
                                        </li>
                                    </ul>
                                    <Button className="btn-danger" type="submit">Details <img src={ImagePath.loaderred} alt="loader" className="img-fluid ml-3 rotating" /></Button>
                                </CardBody>
                            </Card>
                       </div>
                       <div className="mb-4">
                            <Card className="border-0 barcard">
                                <CardBody>
                                    <h6 className="font-weight-light mb-4">JOBS COMPLETED</h6>
                                    <ul className="list-unstyled">
                                        <li>
                                            <span className="text-jobs-red">Total Number of Jobs completed</span>
                                            <span className="numbid">10</span>
                                        </li>                                       
                                        <li>
                                            <span className="text-jobs-red">Total Value of Jobs</span>
                                            <span className="numbid"><span class="text-danger mr-1"><FontAwesomeIcon icon={faPoundSign} /></span>3,00,000</span>
                                        </li>
                                    </ul>
                                    <Button className="btn-danger" type="submit">Details <img src={ImagePath.loaderred} alt="loader" className="img-fluid ml-3 rotating" /></Button>
                                </CardBody>
                            </Card>
                       </div>
                    </Container>
                </div>
            </section>
        )
  
}

export default RightSidebar;