import React from 'react';

import { Container, Row, Col, Input,Card, FormGroup, CardBody,Label, Button } from 'reactstrap';
import { ImagePath } from '../../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faFolder, faCalendar, faCloudUploadAlt,faPoundSign, faBars, faMapMarker, faLifeRing, faStarOfLife, faChartLine } from '@fortawesome/free-solid-svg-icons';
import './stylebar.css'
import BASE_API_URL from '../../Config/config';
import {getApi, apiCallWithToken, postApiCallWithToken} from '../../Config/api';

import { Multiselect } from "multiselect-react-dropdown";
import moment from 'moment'

class Cform extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            plainArray: ["Option 1", "Option 2", "Option 3", "Option 4", "Option 5"],
            selectedValues: [
                { key: "Option 1" }
            ],
            jobBasicInfo: {
                job_title: '',
                start_date: null,
                end_date: null,
                completion_date: null,
                job_summary: '',
                is_nda: '',
                budget: '',
                preferred_payment_method: '',
            },
            title: '',
            jobType: '',
            capabilityType: '',
            locationType: '',
            location: null,
            postcode: null,
            jobSummary: '',
            selectedIndustry: '',
            Budget: '',
            paymentMethod: '',
            imageName: '',
            propertyImage: '',
            propertyImageFile: '',
            selectedSkillId: '',
            skill_list: [],
            specialisation_list: [],
            designSoftware_list: [],
            inspectionSoftware_list: [],
            programingSoftware_list: [],
            simulationSoftware_list: [],

            hardware_classifications: [],
            machine_capabilities: [],
            materialSuitabilityList: [],

            selectedSpecialism: [],
            selectedDesign: [],
            selectedInspection: [],
            selectedPrograming: [],
            selectedSimulation: [],

            selectedMachineCapabilitiesName: '',
            selectedMachineCapabilities: {
                classification:"",
                material_capability:"",
                x:null,
                y:null,
                z:null,
                gauge:"",
                max_component_diameter:null,
                max_component_length:null,
                max_drill_depth:null,
                quantity:null,
                quality:null,
                stock_material:"",
                additional_process :"",
              },
              selected_hardware: {
                id: 0,
                classification_name: "",
                color: "",
                title: ""
              },

              allIndusExperience: [
                {id: 110, value: 'Aerospace'},
                {id: 111, value: 'Automotive'},
                {id: 112, value: 'Construction'},
                {id: 113, value: 'Consumer Goods'},
                {id: 114, value: 'Defense'},
                {id: 115, value: 'Electric Vehicles'},
                {id: 116, value: 'Marine'},
                {id: 117, value: 'Medical'},
                {id: 118, value: 'Mining'},
                {id: 119, value: 'Motorsport'},
                {id: 120, value: 'Oil and Gas'},
                {id: 121, value: 'Renewable Energy'},
                {id: 122, value: 'Robotics/AI/Automation'},
            ],

              green: {
                position: [
                  { id: 1, title: "x", value: "" },
                  { id: 2, title: "y", value: "" },
                  { id: 3, title: "z", value: "" }
                ],
                inputField: []
              },
              black: {
                position: [
                  { id: 4, title: "x", value: "" },
                  { id: 5, title: "y", value: "" },
                ],
                inputField: [
                  { id: 6, title: "Gauge", value: "" , key: 'gauge'}
                ]
              },
              gray: {
                position: [
                  { id: 7, title: "x", value: "" },
                  { id: 8, title: "y", value: "" },
                  { id: 9, title: "z", value: "" },
                ],
                inputField: [
                  { id: 10, title: "Max Drill Depth", value: "" , key: "max_drill_depth"}
                ]
              },
              yellow: {
                position: [],
                inputField: [
                  { id: 11, title: "Max Component Length", value: "" , key: "max_component_length"},
                  { id: 12, title: "Max Component Diameter", value: "", key: "max_component_diameter"}
                ]
              },
              red: {
                position: [
                  { id: 13, title: "X", value: "" },
                  { id: 14, title: "Y", value: "" },
                  { id: 15, title: "Z", value: "" }
                ],
                inputField: [
                  { id: 16, title: "Max Component Length", value: "" , key: "max_component_length"},
                  { id: 17, title: "Max Component Diameter", value: "" , key: "max_component_diameter"}
                ]
              },
        };
    }
    addItem() {
        this.selectedValues.push({ key: "Option 1" });
    }

    setCommonFieldData = e => {
        let basicInfo = {...this.state.jobBasicInfo}
        basicInfo[e.target.name] = e.target.value;
        this.setState({
            jobBasicInfo : basicInfo
        })
    }

    handleJobTypeSelection = e =>{
        let basicInfo = {...this.state.jobBasicInfo}
        if(e.target.value === 'T') {
            basicInfo.completion_date = null
        }else {
            basicInfo.start_date = null;
            basicInfo.end_date = null;
        }
        this.setState({
            jobType: e.target.value,
            jobBasicInfo: basicInfo
        })
    }
    jobLocationTypeSelection = (e) => {
        this.setState({
          locationType: e.target.value,
        })
        if (e.target.value === 'R') {
            this.setState({
                location: null
            })
        }
      }

      setLocation = (e) => {
        this.setState({
            location: e.target.value,
        })
        if (e.target.value === 'International') {
            this.setState({
                postcode : null
            })
        }
      }
      setPostcode = (e) => {
        this.setState({
            postcode : e.target.value
        })
      }

    handleCapabilitySelection = e =>{
        this.clearSelectedMachineCapabilities();
        this.clearSelectedSoftwareAndSpecialismData();
        //this.clearAllDynamicField();
        console.log("Capability selection: ", e.target.value);
        
        this.setState({
            capabilityType: e.target.value
        })

        if (e.target.value === 'People') {
            let basicInfo = {...this.state.jobBasicInfo}
            basicInfo.quality = "";
            basicInfo.quantity = "";
            basicInfo.stock_material = "";
            basicInfo.additional_process = "";
            this.setState({
                jobBasicInfo : basicInfo
            })
            this.get_skillLists('people');
        }else {
            this.get_skillLists('machine');
        }
    }
    getSelectedSkillItem = (e) => {
        // let selectedSkill = this.state.selectedPeopleCapabilities;
        // selectedSkill.skill = e.target.value
        // this.setState({
        //   addMorePeople: false,
        //   selectedSkillId: e.target.id,
        //   selectedPeopleCapabilities:selectedSkill
        // })
        console.log("selected value : ", e.target.value);
        this.setState({
            selectedSkillId: e.target.id
        })

        this.get_specilizationAndSoftwareList(e.target.id);
        
      }

      clearSoftwareData() {
        this.setState({
            specialisation_list: [],
          designSoftware_list: [],
          simulationSoftware_list: [],
          programingSoftware_list: [],
          inspectionSoftware_list: []
        })
      }
      clearSelectedSoftwareAndSpecialismData() {
          this.setState({
            selectedSpecialism: [],
            selectedDesign: [],
            selectedInspection: [],
            selectedPrograming: [],
            selectedSimulation: [],
          })
      }

      clearSelectedMachineCapabilities() {
          let machineDetails = {...this.state.selectedMachineCapabilities}
          machineDetails.classification = "";
          machineDetails.machining_envelopes = "";
        this.setState({
            selectedMachineCapabilities : machineDetails
        })
    }

      handleImageChange = e => {
        const files = Array.from(e.target.files);
          // data.property_image = files;
          console.log("cheking file type:  ", e.target.file);
        //   setImageName(e.target.files[0].name)
          this.setState({
              imageName: e.target.files[0].name
          })
          Promise.all(
            files.map(file => {
                this.setState({
                    propertyImageFile: file
                })
            //   updatePropertyImageFile(file);
              return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.addEventListener('load', ev => {
                  resolve(ev.target.result);
                });
                reader.addEventListener('error', reject);
                reader.readAsDataURL(file);
              });
            }),
          ).then(
            images => {
                this.setState({
                    propertyImage: images
                })
                // updatePropertyImage(images);
            },
            error => {
              console.error(error);
            },
          );
      };

      clearHardwareClassificationData() {
        let hardware = this.state.selected_hardware;
        hardware.id = '';
        hardware.color = '';
        hardware.classification_name = "";
        hardware.title = "";
        this.setState({
          selected_hardware: hardware,
    
        })
      }

      clearAllMachineData() {
        let machineCapabilities = {...this.state.selectedMachineCapabilities};
        machineCapabilities.classification = '';
        machineCapabilities.material_capability = '';
        machineCapabilities.x = null;
        machineCapabilities.y = null;
        machineCapabilities.z = null;
        machineCapabilities.gauge = null;
        machineCapabilities.max_component_diameter = null;
        machineCapabilities.max_component_length = null;
        machineCapabilities.max_drill_depth = null;
        machineCapabilities.quantity = null;
        machineCapabilities.quality = null;
        machineCapabilities.stock_material = '';
        machineCapabilities.additional_process = '';
        this.setState({
            selectedMachineCapabilities: machineCapabilities
        })
      }

      handleExpSelection = e => {
        this.setState({
            selectedIndustry: e.target.value
        })
    };

      handleClassification = event => {
        let selectedMachineCapabilities = this.state.selectedMachineCapabilities;
        let index = event.target.value;
        let details = this.state.hardware_classifications[index];
        selectedMachineCapabilities.classification = details.id;
        this.setState({
          selected_hardware:this.state.hardware_classifications[index],
          selectedMachineCapabilities: selectedMachineCapabilities
        });
      }

      // For Specialism

      onSelectSpecialism = (selectedList, selectedItem) => {
        let selectedSpecialism = this.state.selectedSpecialism;
        selectedSpecialism.push(selectedItem.id)
        this.setState({
          selectedSpecialism:selectedSpecialism
        })
    }
      onRemoveSpecialism = (selectedList, removedItem) => {
        let selectedSpecialism = this.state.selectedSpecialism;
        for(let i = 0 ; i < selectedSpecialism.length; i++){
          if(selectedSpecialism[i] === removedItem.id){
            selectedSpecialism.splice(i, 1);
          }
        }
        this.setState({
          selectedSpecialism:selectedSpecialism
        })
      }

      // For Design Software

      onSelectDesign = (selectedList, selectedItem) => {
          
          let itemDetails = {
            id: selectedItem.id,
            name: selectedItem.software_name,
            is_preferred: "False",
            is_required: "True",
          };
        let selectedDesign = this.state.selectedDesign;
        selectedDesign.push(itemDetails)
        console.log('selectedDesign:  ', selectedDesign);
        this.setState({
          selectedDesign:selectedDesign
        })
    }
    onRemoveDesign = (selectedList, removedItem) => {
      let selectedDesign = this.state.selectedDesign;
      for(let i = 0 ; i < selectedDesign.length; i++){
        if(selectedDesign[i].id === removedItem.id){
          selectedDesign.splice(i, 1);
        }
      }
      this.setState({
        selectedDesign:selectedDesign
      })
    }

    // For Inspection Software

    onSelectInspection = (selectedList, selectedItem) => {
        let inspectionItemDetails = {
            id: selectedItem.id,
            name: selectedItem.software_name,
            is_preferred: "False",
            is_required: "True",
          };
        let selectedInspection = this.state.selectedInspection;
        selectedInspection.push(inspectionItemDetails)
        this.setState({
          selectedInspection:selectedInspection
        })
      }
      onRemoveInspection = (selectedList, removedItem) => {
        let selectedInspection = this.state.selectedInspection;
        for(let i = 0 ; i < selectedInspection.length; i++){
          if(selectedInspection[i].id === removedItem.id){
            selectedInspection.splice(i, 1);
          }
        }
        this.setState({
          selectedInspection:selectedInspection
        })
      }

      // For Programming Software
      onSelectPrograming = (selectedList, selectedItem) => {
        let itemDetails = {
            id: selectedItem.id,
            name: selectedItem.software_name,
            is_preferred: "False",
            is_required: "True",
          };
        let selectedPrograming = this.state.selectedPrograming;
        selectedPrograming.push(itemDetails)
        this.setState({
          selectedPrograming:selectedPrograming
        })
      }
      onRemovePrograming = (selectedList, removedItem) => {
        let selectedPrograming = this.state.selectedPrograming;
        for(let i = 0 ; i < selectedPrograming.length; i++){
          if(selectedPrograming[i].id === removedItem.id){
            selectedPrograming.splice(i, 1);
          }
        }
        this.setState({
          selectedPrograming:selectedPrograming
        })
      }

      // For Simulation Software
      onSelectSimulation = (selectedList, selectedItem) => {
        let simulationItemDetails = {
            id: selectedItem.id,
            name: selectedItem.software_name,
            is_preferred: "False",
            is_required: "True",
          };
        let selectedSimulation = this.state.selectedSimulation;
        selectedSimulation.push(simulationItemDetails)
        this.setState({
          selectedSimulation:selectedSimulation
        })
      }
      onRemoveSimulation = (selectedList, removedItem) => {
        let selectedSimulation = this.state.selectedSimulation;
        for(let i = 0 ; i < selectedSimulation.length; i++){
          if(selectedSimulation[i].id === removedItem.id){
            selectedSimulation.splice(i, 1);
          }
        }
        this.setState({
          selectedSimulation:selectedSimulation
        })
      }
      setXYZvalue = e => {
        let tempCapabilities = {...this.state.selectedMachineCapabilities};
        tempCapabilities[e.target.name] = e.target.value;
        this.setState({
          selectedMachineCapabilities : tempCapabilities
        })
      }
      setGuageValue = e => {
        let gaugeDetails = this.state.selectedMachineCapabilities;
        gaugeDetails.gauge = e.target.value
          this.setState({
            selectedMachineCapabilities:gaugeDetails
          })
      }
      setDrillDepth = e => {
        let drillDepth = this.state.selectedMachineCapabilities;
        drillDepth.drillDepth = e.target.value
          this.setState({
            selectedMachineCapabilities:drillDepth
          })
      }
      setMaxComponentValue = e => {
        let maxComponent = {...this.state.selectedMachineCapabilities};
        maxComponent[e.target.name] = e.target.value;
        this.setState({
          selectedMachineCapabilities : maxComponent
        })
      }

      formatInput = (e) => {
        // Prevent characters that are not numbers ("e", ".", "+" & "-") ✨
        let checkIfNum;
        if (e.key !== undefined) {
          // Check if it's a "e", ".", "+" or "-"
          checkIfNum = e.key === "e" || e.key === "." || e.key === "+" || e.key === "-" ;
        }
        else if (e.keyCode !== undefined) {
          // Check if it's a "e" (69), "." (190), "+" (187) or "-" (189)
          checkIfNum = e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 187 || e.keyCode === 189;
        }
        return checkIfNum && e.preventDefault();
      }

      // all APIs call here

  get_skillLists(type) {
    apiCallWithToken(BASE_API_URL + 'web/skilllist/', 
    'post',
    { "skill_type":type })
      .then(response => {
          console.log('Response:   ', response);
        if(type === 'people') {
          this.setState({
            skill_list: response.data.allskill
          })
        }else {
          this.setState({
            machine_capabilities: response.data.allskill
          })
        }
        
      })
      .catch(err => {
        console.log(err);
      });
  }

  get_specilizationAndSoftwareList(parent_id) {
    // if(parent_id === '7') {

    // }else {
    //   this.clearSoftwareData();
    // }
    this.clearSoftwareData();
    this.clearSelectedSoftwareAndSpecialismData();
    apiCallWithToken(BASE_API_URL + 'web/peoplefetch/', 
    'post',
    { "parent_id":parent_id })
      .then(response => {
        var allData = response.data.softwares;
        if(allData.length > 0) {
          for (var i = 0; i < allData.length; i++) {
            let details = allData[i];
            if (details.software_type === 'Design Software') {
              this.setState({designSoftware_list: [...this.state.designSoftware_list, details]})
            }else if (details.software_type === 'Simulation Software') {
              this.setState({simulationSoftware_list: [...this.state.simulationSoftware_list, details]})
            }else if (details.software_type === '"Programming Software') {
              this.setState({programingSoftware_list: [...this.state.programingSoftware_list, details]})
            }else {
              this.setState({inspectionSoftware_list: [...this.state.inspectionSoftware_list, details]})
            }
          }
        }
        this.setState({
            specialisation_list: response.data.specialism
          })
        // if(parent_id === '7') {
        //   this.setState({
        //     machine_specialism: response.data.specialism
        //   })
        // }else {
        //   this.setState({
        //     specialisation_list: response.data.specialism
        //   })
        // }
      })
      .catch(err => {
        console.log(err);
      });
  }

  get_materialSuitability() {
    getApi(BASE_API_URL + 'web/materiallist/')
      .then(response => {
        this.setState({
          materialSuitabilityList: response.data
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  getSelectedMachineCapabilities = (e) => {
    this.clearAllMachineData();
    this.setState({
      selectedMachineCapabilitiesName: e.target.value,
      selectedSkillId: e.target.id
    })
    
    if(e.target.id === '7'){
      this.get_specilizationAndSoftwareList(e.target.id);
    }else {
      this.get_hardwareClassification(e.target.id);
    }
    if (e.target.value === 'Subcontract CNC Machining') {
      this.get_materialSuitability();
    }
    if (e.target.value === 'Subcontract Inspection') {
          let basicInfo = {...this.state.jobBasicInfo}
          basicInfo.stock_material = "";
          basicInfo.additional_process = "";
        this.setState({
            jobBasicInfo : basicInfo
        })
      }

    if (e.target.value === 'Subcontract Inspection' || e.target.value === 'Other Machine Capabilities') {
      this.clearHardwareClassificationData();
      //this.clearAllDynamicField();
    }
  }

  clearAllDynamicField() {
    if ('material_capability' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['material_capability'];
      }
      if ('gauge' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['gauge'];
      }
      if ('max_drill_depth' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['max_drill_depth'];
      }
      if ('max_component_diameter' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['max_component_diameter'];
      }
      if ('max_component_length' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['max_component_length'];
      }
      if ('x' in this.state.selectedMachineCapabilities || 'y' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['x'];
        delete this.state.selectedMachineCapabilities['y'];
      }
      if ('z' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['z'];
      }
  }

  get_hardwareClassification(id) {
    //this.clear_selectedHardwareSpecifications();
    console.log('hardware_classifications ID:  ', id);
    apiCallWithToken(BASE_API_URL + 'web/classificationlist/', 
    'post',
    { "parent_id":id })
      .then(response => {
        console.log('hardware_classifications:  ', response.data.classifications.length);
        this.setState({
          hardware_classifications: response.data.classifications
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  submitPressed = e => {
      if(this.state.jobBasicInfo.job_title === '') {
          alert('Please enter job title.')
      }else if (this.state.jobType === '') {
        alert('Please choose job type.')
      }else if (this.state.jobType === 'T' && this.state.jobBasicInfo.start_date === '') {
        alert('Please choose start date.')
      }else if (this.state.jobType === 'T' && this.state.jobBasicInfo.end_date === '') {
        alert('Please choose end date.')
      }if (this.state.jobBasicInfo.start_date > this.state.jobBasicInfo.end_date) {
        alert('End date should be greater than start date')
    }else if (this.state.jobType === 'P' && this.state.jobBasicInfo.completion_date === '') {
        alert('Please select completion date.')
      }else if (this.state.capabilityType === '') {
          alert('Please choose capability type')
      }else if (this.state.jobBasicInfo.job_summary === '') {
          alert('Please enter job summary')
      }else if (this.state.selectedIndustry === '') {
          alert('Please select industry type')
      }else if (this.state.jobBasicInfo.budget === '') {
        alert('Please enter budget')
      }else if (this.state.jobBasicInfo.preferred_payment_method === '') {
          alert('Please choose preferred payment method')
      }
      else {

        let allSoftware = this.state.selectedDesign.concat(this.state.selectedInspection, this.state.selectedPrograming, this.state.selectedSimulation)
        
        let dicSoftware = {};
        for (let i = 0; i < allSoftware.length; i++) {
            dicSoftware[i] = allSoftware[i];
        }
        let jobDetails = {}
        if (this.state.capabilityType === 'People') {
            jobDetails['job_title'] = this.state.jobBasicInfo.job_title;
            jobDetails['job_type'] = this.state.jobType;
            jobDetails['start_date'] = this.state.jobBasicInfo.start_date;
            jobDetails['end_date']=this.state.jobBasicInfo.end_date;
            jobDetails['completion_date']=this.state.jobBasicInfo.completion_date;
            jobDetails['capabilities_type']=this.state.capabilityType;
            jobDetails['skill']=this.state.selectedSkillId;
            jobDetails['is_flexible']=this.state.locationType;
            jobDetails['location']=this.state.location;
            jobDetails['postcode']=this.state.postcode;
            jobDetails['job_summary']=this.state.jobBasicInfo.job_summary;
            jobDetails['industry']=this.state.selectedIndustry;
            jobDetails['budget']=this.state.jobBasicInfo.budget;
            jobDetails['preferred_payment_method']=this.state.jobBasicInfo.preferred_payment_method;
            jobDetails['job_status']="P";
            jobDetails['specialism_skill']=this.state.selectedSpecialism.join(',');
            jobDetails['softwares']=dicSoftware;
        }else {
            jobDetails = this.state.selectedMachineCapabilities;
            jobDetails['job_title']=this.state.jobBasicInfo.job_title;
            jobDetails['job_type']=this.state.jobType;
            jobDetails['start_date']=this.state.jobBasicInfo.start_date;
            jobDetails['end_date']=this.state.jobBasicInfo.end_date;
            jobDetails['completion_date']=this.state.jobBasicInfo.completion_date;
            jobDetails['capabilities_type']=this.state.capabilityType;
            jobDetails['skill']=this.state.selectedSkillId;
            jobDetails['job_summary']=this.state.jobBasicInfo.job_summary;
            jobDetails['industry']=this.state.selectedIndustry;
            jobDetails['budget']=this.state.jobBasicInfo.budget;
            jobDetails['preferred_payment_method']=this.state.jobBasicInfo.preferred_payment_method;
            jobDetails['job_status']="P";
            console.log('jobDetails: ', jobDetails);
            // jobDetails = {
                // "job_title":this.state.jobBasicInfo.job_title,
                // "job_type":this.state.jobType,
                // "start_date":this.state.jobBasicInfo.start_date,
                // "end_date":this.state.jobBasicInfo.end_date,
                // "completion_date":this.state.jobBasicInfo.completion_date,
                // "capabilities_type":this.state.capabilityType,
                // "skill":this.state.selectedSkillId,
                // "job_summary":this.state.jobBasicInfo.job_summary,
                // "industry":this.state.selectedIndustry,
                // "budget":this.state.jobBasicInfo.budget,
                // "preferred_payment_method":this.state.jobBasicInfo.preferred_payment_method,
                // "job_status":"P",

            //     "classification":this.state.selectedMachineCapabilities.classification,
            //     "material_capability":this.state.selectedMachineCapabilities.material_capability,
            //     "x":this.state.selectedMachineCapabilities.x,
            //     "y":this.state.selectedMachineCapabilities.y,
            //     "z":this.state.selectedMachineCapabilities.z,
            //     "gauge":this.state.selectedMachineCapabilities.gauge,
            //     "max_component_diameter":this.state.selectedMachineCapabilities.max_component_diameter,
            //     "max_component_length":this.state.selectedMachineCapabilities.max_component_length,
            //     "max_drill_depth":this.state.selectedMachineCapabilities.max_drill_depth,
            //     "quantity":this.state.jobBasicInfo.quantity,
            //     "quality":this.state.jobBasicInfo.quality,
            //     "stock_material":this.state.jobBasicInfo.stock_material,
            //     "additional_process" :this.state.jobBasicInfo.additional_process
            //   }
        }
        
          if (this.state.jobBasicInfo.is_nda === true) {
              jobDetails['is_nda'] = 'True'
          }else {
            jobDetails['is_nda'] = 'False'
          }
          let userid = localStorage.getItem('userId');
          //http://111.93.169.90:2030/user/[user_id]/job/post/
          postApiCallWithToken(BASE_API_URL + 'user/' + userid + '/job/post/', 
          'post',
          {
            'full_data' : jobDetails
          })
          .then(response => {
            console.log(response);
            alert("Job posted successfully.");
          })
          .catch(err => {
          console.log(err);
          })
      }
      
      
  }

    render() {
        const { plainArray, selectedValues } = this.state;
        return (
            <Container fluid className="p-0">
                <h6 className="gray-title">CREATE NEW JOB</h6>
                <Card>
                    <CardBody className="redform">
                        <FormGroup>
                            <Label className="form-label">Job <span className="font-weight-exlight">Title</span></Label>
                            <div className="form-input">
                                <Input type="text" placeholder="Enter job Name." maxLength={50} name='job_title' onChange={this.setCommonFieldData}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faFolder} /></span>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label className="form-label">Job <span className="font-weight-exlight">Type</span></Label>
                            <div className="mb-3 d-flex align-items-center">
                                <Input type="radio" id="Skill1" name="Skill" className="custom-control-input" onChange={this.handleJobTypeSelection} value='T'/>
                                <Label className="btn-radio" for="Skill1">Time</Label>
                                <span className="error-msg">* I need resource for a set period </span>
                            </div>
                            <div className="mb-3 d-flex align-items-center">
                                <Input type="radio" id="Skill2" name="Skill" className="custom-control-input" onChange={this.handleJobTypeSelection} value='P'/>
                                <Label className="btn-radio" for="Skill2">Project</Label>
                                <span className="error-msg">* I need resource to complete a specific project</span>
                            </div>
                        </FormGroup>
                        {this.state.jobType === "T" ?
                        <FormGroup className="d-md-flex">
                            <div className="w-75 d-md-flex justify-content-between">
                            <div className="form-input bdr-gray flex-fill mb-2 mb-lg-0 mr-md-1">
                                <Input type="date" className="py-0 h-auto" placeholder="Start Date" name='start_date' min={moment().format("YYYY-MM-DD")} onChange={this.setCommonFieldData}/>
                                <span className="icon-wr sm"><FontAwesomeIcon icon={faCalendar} /></span>
                            </div>
                            <div className="form-input bdr-gray flex-fill ml-md-1">
                                <Input type="date" className="py-0 h-auto" placeholder="End Date" name='end_date' min={moment().format("YYYY-MM-DD")} onChange={this.setCommonFieldData}/>
                                <span className="icon-wr sm"><FontAwesomeIcon icon={faCalendar} /></span>
                            </div>
                            </div>
                            <div className="round-check ml-2 w-25">
                                <Input type="checkbox" id="ill1" className="custom-control-input" />
                                <Label className="btn-radio w-100 " for="ill1">Dates Flexible
                                <span className="icon-wr sm d-flex align-items-center justify-content-center"><img src={ImagePath.Check} className="img-fluid " alt="check" /></span>
                                </Label>
                            </div>                            
                        </FormGroup>
                         : this.state.jobType === "P" ?
                        <FormGroup className="d-md-flex">
                            <div className="form-input bdr-gray w-75">
                                <Input type="Date" className="py-0 h-auto" placeholder="Completion Date" name='completion_date' min={moment().format("YYYY-MM-DD")} onChange={this.setCommonFieldData}/>
                                <span className="icon-wr sm"><FontAwesomeIcon icon={faCalendar} /></span>
                            </div>
                            <div className="round-check ml-2 w-25">
                                <Input type="checkbox" id="ill1" className="custom-control-input" />
                                <Label className="btn-radio w-100 " for="ill1">Dates Flexible
                                <span className="icon-wr sm d-flex align-items-center justify-content-center"><img src={ImagePath.Check} className="img-fluid " alt="check" /></span>
                                </Label>
                            </div>
                        </FormGroup> : null
                        }
                        
                        
                        <FormGroup className="d-md-flex pt-3">
                            <Label className="form-label w-25 text-nowrap">Capability <span className="font-weight-exlight">Type</span></Label>
                            <Row className="w-75 pl-md-4">
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cap1" name="capability" className="custom-control-input" onChange={this.handleCapabilitySelection} value='People' />
                                        <Label className="btn-radio w-100" for="cap1">People</Label>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cap2" name="capability" className="custom-control-input" onChange={this.handleCapabilitySelection} value='Machine' />
                                        <Label className="btn-radio w-100" for="cap2">Machine</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        {this.state.capabilityType === "People" ? 
                        <FormGroup >
                            <Label className="form-label"> Location <span className="font-weight-exlight">Type</span></Label>
                            <Row >
                                <Col sm={6} md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="location1" name="location" className="custom-control-input" onChange={(e) => this.jobLocationTypeSelection(e)} value='R'/>
                                        <Label className="btn-radio w-100" for="location1">Remote</Label>
                                    </div>
                                </Col>
                                <Col sm={6} md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="location2" name="location" className="custom-control-input" onChange={(e) => this.jobLocationTypeSelection(e)} value='O' />
                                        <Label className="btn-radio w-100" for="location2">On Site</Label>
                                    </div>
                                </Col>
                                <Col sm={6} md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="locationp2" name="location" className="custom-control-input" onChange={(e) => this.jobLocationTypeSelection(e)} value='F' />
                                        <Label className="btn-radio w-100" for="locationp2">Flexible</Label>
                                    </div>
                                </Col>
                            </Row>
                            {this.state.locationType === 'O' || this.state.locationType === 'F' ?
                                <FormGroup>
                                    <Label className="form-label">Location</Label>
                                    <Row>
                                        <Col md={5}>
                                            <div className="d-flex">
                                                <div className="custom-control custom-radio pr-3">
                                                    <Input type="radio" className="custom-control-input" id="r12" name="place" onChange={(e) => this.setLocation(e)} value='UK'/>
                                                    <Label className="custom-control-label" for="r12">UK</Label>
                                                </div>
                                                <div className="custom-control custom-radio">
                                                    <Input type="radio" className="custom-control-input" id="r21" name="place" onChange={(e) => this.setLocation(e)} value='International'/>
                                                    <Label className="custom-control-label" for="r21">International</Label>
                                                </div>
                                            </div>
                                        </Col>
                                        {this.state.location === 'UK' ?
                                            <Col md={7}>
                                            <div className="form-input bdr-gray">
                                                <Input type="text" className="py-0 h-auto" placeholder="Enter Postcode" name='Postcode' onChange={this.setPostcode}/>
                                                <span className="icon-wr sm"><FontAwesomeIcon icon={faMapMarker} /></span>
                                            </div>
                                            </Col> : null
                                        }
                                        
                                    </Row>
                                </FormGroup> : null
                            }
                        </FormGroup> : null
                        }
                    </CardBody>
                </Card>
                <Card>
                    <CardBody className="redform">
                        <FormGroup>
                            <Label className="form-label pl-0">Job <span className="font-weight-exlight">Summary</span></Label>
                            <Input type="textarea" rows="4" placeholder="Example: 5-axis machining of a batch of 10-off Inconel components 
                                                        for deep-sea application" maxLength={1000} name='job_summary' onChange={this.setCommonFieldData}/>
                            <span className="form-text">* This is an overview of what is required on the job, not used for any matching algorithm</span>
                        </FormGroup>
                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label className="form-label">Industry </Label>
                                    <div className="form-input w-100">
                                        <select
                                        className="form-control"
                                        name="industry"
                                        onChange={this.handleExpSelection}>
                                        {this.state.allIndusExperience.map((industry, index) => (
                                        <>
                                        <option value="none" selected disabled hidden>
                                        Select
                                        </option>
                                        <option key={index} value={industry.value} >{industry.value} </option>
                                        </>
                                        ))}
                                    </select>
                                    </div>
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label className="form-label">NDAs <span className="font-weight-exlight">Required?</span></Label>
                                    <div className="d-flex flex-smwrap">
                                        <div className="pr-2 my-1">
                                            <Input type="radio" id="yes" name="is_nda" className="custom-control-input" onChange={this.setCommonFieldData} value='True'/>
                                            <Label className="btn-radio rounded" for="yes">Yes</Label>
                                        </div>
                                        <div className="pr-2 my-1">
                                            <Input type="radio" id="no" name="is_nda" className="custom-control-input" onChange={this.setCommonFieldData} value='False'/>
                                            <Label className="btn-radio rounded" for="no">No</Label>
                                        </div>
                                        <div className="pr-2 my-1">
                                            <Input type="file" id="up" className="custom-control-input" />
                                            <Label className="btn-radio rounded bg-redfade text-white" for="up"><FontAwesomeIcon icon={faCloudUploadAlt} /></Label>
                                        </div>
                                    </div>
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label className="form-label">Company Logo / Profile Photo</Label>
                            <div class="form-input">
                                <label for="uploadpic" class="btn btn-upload">Upload Image</label>
                                <Input type="file" id="uploadpic" hidden onChange={e => this.handleImageChange(e)}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faCloudUploadAlt} /></span>
                            </div>
                            <div className="row">
                      
                        <div className="col-md-4">
                            <Label style={{fontSize: 12}} >
                            {this.state.imageName}
                            </Label>
                                    
                                
                            </div>
                      
                      </div>
                        </FormGroup>
                        <FormGroup>
                            <Label className="pl-0 form-label">Certification  </Label>
                            <Multiselect options={plainArray} isObject={false} selectedValues={selectedValues} />
                        </FormGroup>
                        <FormGroup className="d-md-flex pt-3">
                            <Label className="form-label w-25 text-nowrap text-muted">Certification 1</Label>
                            <Row className="w-75 pl-sm-4">
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer1" name="Certification" className="custom-control-input" />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer1">Required</Label>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer2" name="Certification" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer2">Preferred</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup className="d-md-flex pt-3">
                            <Label className="form-label w-25 text-nowrap text-muted">Certification 2</Label>
                            <Row className="w-75 pl-sm-4">                               
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer22" name="Certification2" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer22">Required </Label>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer21" name="Certification2" className="custom-control-input" />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer21">Preferred</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup>
                            <Label className="form-label">Budget</Label>
                            <div className="form-input">
                                <Input type="number" className="form-control " placeholder="Enter Your Budget" name='budget' onChange={this.setCommonFieldData} onKeyDown={this.formatInput}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faPoundSign} /></span>
                            </div>
                            <span className="form-text">* Budget information is not shared with potential providers, this is purely recorded to analyse your target versus actual project costs.</span>
                        </FormGroup>
                        <FormGroup>
                            <Label className="form-label">Preferred <span className="font-weight-exlight"> payment method</span> 
                            </Label>

                            <div className="px-3 border-red rounded py-2">
                                <div className="radio-text pb-3">
                                    <div className="custom-control custom-radio">
                                        <Input type="radio" className="custom-control-input" id="r1" name="preferred_payment_method" value='I' onChange={this.setCommonFieldData}/>
                                        <Label className="custom-control-label" for="r1">INSTANT CNCT</Label>
                                    </div>
                                    <p>INSTANT CNCT is our immediate payment method. Electronic payment is taken from 
                                        the buyer at the point that the project is agreed. Once the project is 
                                        completed, both parties confirm that it has been completed correctly and 
                                        payment is automatically sent to the seller's account 
                                        (minus the transaction fee). Buyer's receive a sales invoice from
                                         CNCT and seller's receive a purchase invoice from CNCT</p>
                                </div>
                                <div className="radio-text pb-3">
                                    <div className="custom-control custom-radio">
                                        <Input type="radio" className="custom-control-input" id="r2" name="preferred_payment_method" value='S' onChange={this.setCommonFieldData}/>
                                        <Label className="custom-control-label" for="r2">SPECIAL TERMS</Label>
                                    </div>
                                    <p>Sometimes, on larger projects, we appreciate that extended payment 
                                        terms are needed. Special Terms allows the buyers and seller to agree their
                                         own payment terms and even upload terms and conditions of business. When the 
                                         project is agreed, we take a payment equal to the seller's transaction fee from 
                                         the buyer (akin to a deposit). When the project is completed, we send a completion 
                                         note to both the buyer and seller and the seller invoices the buyer independently 
                                         and directly for the balance (agreed project fee, mins transaction fee).</p>
                                </div>
                                <div className="mb-3">
                                    <Input type="file" id="up" className="custom-control-input" />
                                    <Label className="btn-radio bg-red text-white text-capitalize" for="up">Upload Terms &nbsp; &nbsp; <FontAwesomeIcon icon={faCloudUploadAlt} /></Label>
                                </div>
                            </div>
                        </FormGroup>                       
                    </CardBody>
                </Card>
                <div className="mb-3">
                    <h6 className="gray-title">CAPABILITIES</h6>
                </div>
                <Card className="redform">
                    <CardBody>
                    <FormGroup>
                            <Label className="form-label">Skill</Label>
                            
                        </FormGroup>
                        {this.state.capabilityType === 'People'?
                        <FormGroup>
                            <FormGroup>
                            <Row className="pt-2 pr-md-5">
                                {this.state.skill_list.map((skills, index) => (
                                    <Col sm={6} md={6}>
                                        <div className="clearfix my-2">
                                            <Input type="radio" id={skills.id} className="custom-control-input" name="skills" onChange={(e) => this.getSelectedSkillItem(e)} value={skills.skill_name}/>
                                            <Label className="btn-radio w-100 text-capitalize" for={skills.id}>{skills.skill_name} </Label>
                                        </div>
                                    </Col>
                                ))}                             
                            </Row>
                            </FormGroup>
                            {this.state.specialisation_list.length > 0 ?
                            <FormGroup>
                                <Label className="pl-0 form-label">Specialism </Label>
                                <Multiselect options={this.state.specialisation_list} displayValue='specialism_name' key="Specialism" onSelect={this.onSelectSpecialism} onRemove={this.onRemoveSpecialism}/>
                            </FormGroup>
                            : null
                            }
                            {this.state.designSoftware_list.length > 0 ?
                            <div>
                                <FormGroup>
                                    <Label className="pl-0 form-label">Design Software </Label>
                                    <Multiselect options={this.state.designSoftware_list} displayValue='software_name' onSelect={this.onSelectDesign} onRemove={this.onRemoveDesign}/>
                                </FormGroup>
                                {this.state.selectedDesign.map((designs, index) => (
                                    <FormGroup className="pt-3 addrow">                       
                                    <Row >
                                        <Col md={4}>
                                            <Label className="form-label text-nowrap text-muted">{designs.name}</Label>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id="autocad1" name={designs.name} className="custom-control-input" defaultChecked={designs.is_required} />
                                                <Label className="btn-radio w-100 text-capitalize" for="autocad1">Required </Label>
                                            </div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id="autocad2" name={designs.name} className="custom-control-input" defaultChecked={designs.is_preferred} />
                                                <Label className="btn-radio w-100 text-capitalize" for="autocad2">Preferred</Label>
                                            </div>
                                        </Col>
                                    </Row>
                                    </FormGroup>
                                ))}
                                
                            </div>
                            
                            : null
                            }
                            {this.state.programingSoftware_list.length > 0 ?
                            <div>
                                <FormGroup>
                                    <Label className="pl-0 form-label">Programming Software </Label>
                                    <Multiselect options={this.state.programingSoftware_list} displayValue='software_name' onSelect={this.onSelectPrograming} onRemove={this.onRemovePrograming}/>
                                </FormGroup>
                                {this.state.selectedPrograming.map((programing, index) => (
                                    <FormGroup className="pt-3 addrow">                       
                                    <Row >
                                        <Col md={4}>
                                            <Label className="form-label text-nowrap text-muted">{programing.name}</Label>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id="autocad1" name={programing.name} className="custom-control-input" defaultChecked={programing.is_required} />
                                                <Label className="btn-radio w-100 text-capitalize" for="autocad1">Required </Label>
                                            </div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id="autocad2" name={programing.name} className="custom-control-input" defaultChecked={programing.is_preferred} />
                                                <Label className="btn-radio w-100 text-capitalize" for="autocad2">Preferred</Label>
                                            </div>
                                        </Col>
                                    </Row>
                                    </FormGroup>
                                ))}
                            </div>
                            
                            : null
                            }
                            {this.state.simulationSoftware_list.length > 0 ?
                            <div>
                                <FormGroup>
                                <Label className="pl-0 form-label">Simulation Software </Label>
                                <Multiselect options={this.state.simulationSoftware_list} displayValue='software_name' onSelect={this.onSelectSimulation} onRemove={this.onRemoveSimulation}/>
                            </FormGroup>
                            {this.state.selectedSimulation.map((simulation, index) => (
                                    <FormGroup className="pt-3 addrow">                       
                                    <Row >
                                        <Col md={4}>
                                            <Label className="form-label text-nowrap text-muted">{simulation.name}</Label>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id='Simulation' name={simulation.name} className="custom-control-input"  />
                                                <Label className="btn-radio w-100 text-capitalize" for="Simulation">Required </Label>
                                            </div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id='Simulation1' name={simulation.name} className="custom-control-input"  />
                                                <Label className="btn-radio w-100 text-capitalize" for="Simulation1">Preferred</Label>
                                            </div>
                                        </Col>
                                    </Row>
                                    </FormGroup>
                                ))}
                            </div>
                            
                            : null
                            }
                            {this.state.inspectionSoftware_list.length > 0 ?
                            <div>
                                <FormGroup>
                                    <Label className="pl-0 form-label">Inspection Software </Label>
                                    <Multiselect options={this.state.inspectionSoftware_list} displayValue='software_name' onSelect={this.onSelectInspection} onRemove={this.onRemoveInspection}/>
                                </FormGroup>
                                {this.state.selectedInspection.map((inspection, index) => (
                                    <FormGroup className="pt-3 addrow">                       
                                    <Row >
                                        <Col md={4}>
                                            <Label className="form-label text-nowrap text-muted">{inspection.name}</Label>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id='Inspection' name={inspection.name} className="custom-control-input" />
                                                <Label className="btn-radio w-100 text-capitalize" for="Inspection">Required </Label>
                                            </div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="clearfix mb-2">
                                                <Input type="radio" id='Inspection1' name={inspection.name} className="custom-control-input" />
                                                <Label className="btn-radio w-100 text-capitalize" for="Inspection1">Preferred</Label>
                                            </div>
                                        </Col>
                                    </Row>
                                    </FormGroup>
                                ))}
                            </div>
                            
                            : null
                            }
                        </FormGroup>: this.state.capabilityType === 'Machine'?
                        <FormGroup>
                            <FormGroup>
                            <Row className="pt-2 pr-md-5">
                                {this.state.machine_capabilities.map((machine, index) => (
                                    <Col sm={6} md={6}>
                                        <div className="clearfix my-2">
                                            <Input type="radio" id={machine.id} className="custom-control-input" name="machine" onChange={(e) => this.getSelectedMachineCapabilities(e)} value={machine.skill_name}/>
                                            <Label className="btn-radio w-100 text-capitalize" for={machine.id}>{machine.skill_name} </Label>
                                        </div>
                                    </Col>
                                ))}                             
                            </Row>
                            </FormGroup>
                            {this.state.selectedMachineCapabilitiesName === 'Subcontract Inspection' || this.state.selectedMachineCapabilitiesName === 'Subcontract CNC Machining' ?
                            <FormGroup>
                                <Label className="form-label">Hardware <span className="font-weight-exlight">Classification</span> </Label>
                                {/* <div className="form-input bdr-gray w-100 px-3">
                                    <Input type="select" name="select" id="leSelect">
                                        <option>Select</option>
                                        <option>2-Axis Lathe</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                </div> */}
                                <div className="form-input bdr-gray w-100 px-3">
                                    <select
                                        className="form-control"
                                        name="hardware"
                                        onChange={this.handleClassification}>
                                        {this.state.hardware_classifications.map((hardwares, index) => (
                                        <>
                                        <option value="none" selected disabled hidden>
                                        Select
                                        </option>
                                        <option key={index} value={index} >{hardwares.classification_name} </option>
                                        </>
                                        ))}
                                    </select>
                                </div>
                                {this.state.selected_hardware.color === "green" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Milling <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.green.position.map((allPositions, index) => (
                                            <Col md={4}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "golden" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Max Component <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.green.position.map((allPositions, index) => (
                                            <Col md={4}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "purple" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Max Printing <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.green.position.map((allPositions, index) => (
                                            <Col md={4}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "black" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Dimensional <span className="font-weight-exlight"> Capabilities</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.black.position.map((allPositions, index) => (
                                            <Col md={4}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "black" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Gauge </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.black.inputField.map((myInput, index) => (
                                            <Col md={6}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={myInput.key} className="form-control" placeholder={myInput.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "gray" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Component <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.gray.position.map((allPositions, index) => (
                                            <Col md={4}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "gray" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Drill Depth </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.gray.inputField.map((myInput, index) => (
                                            <Col md={6}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={myInput.key} className="form-control" placeholder={myInput.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "yellow" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Turning <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.yellow.inputField.map((myInput, index) => (
                                            <Col md={6}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={myInput.key} className="form-control" placeholder={myInput.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "red" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Milling <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.red.position.map((allPositions, index) => (
                                            <Col md={4}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "red" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Turning <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.red.inputField.map((myInput, index) => (
                                            <Col md={6}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={myInput.key} className="form-control" placeholder={myInput.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                                {this.state.selected_hardware.color === "cyan" ?
                                    <div>
                                    <FormGroup>
                                        <Label className="form-label">Milling <span className="font-weight-exlight"> Envelope</span> </Label>
                                    </FormGroup>
                                    <Row>
                                        {this.state.green.position.map((allPositions, index) => (
                                            <Col md={4}>
                                                <FormGroup>
                                                <div className="form-input p-0">
                                                    <Input type="text" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue}/>
                                                    <span className="mm">mm</span>
                                                </div>
                                                </FormGroup>
                                            </Col>
                                        ))}
                                    </Row>
                                    </div>
                                    : null  
                                }
                            </FormGroup>:null
                            }
                          {this.state.selectedMachineCapabilitiesName === 'Subcontract CNC Machining'?
                          <FormGroup>
                            <Label className="form-label">Material  <span className="font-weight-exlight">Classification</span> </Label>
                                <div className="form-input bdr-gray w-100 px-3">
                                    {/* <Input type="select" name="select" id="leSelect">
                                        <option>Medium (Aluminium, Mild Steel)</option>
                                        <option>2-Axis Lathe</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input> */}

                                    <select className="form-control" name="Material" name="material_capability" onChange={this.setXYZvalue}>
                                    {this.state.materialSuitabilityList.map((Material, index) => (
                                        <>
                                        <option value="none" selected disabled hidden>
                                        Select
                                        </option>
                                        <option key={index} value={Material.id}>{Material.material_capability_name}</option>
                                        </>
                                        ))}                               
                                    </select>

                                </div>
                            </FormGroup> : null
                          }
                        </FormGroup>
                        :null
                        }
                        
                        {this.state.capabilityType === 'Machine' ? 
                            this.state.selectedMachineCapabilitiesName === 'Subcontract CNC Machining' ?
                            <FormGroup>
                                <Row>
                                    <Col xs={12} sm={12} md={6} lg={6}>
                                        <FormGroup>
                                        <Label className="form-label">Quantity</Label>
                                        <div className="form-input bdr-gray ">
                                            <Input type="number" className="py-0 h-auto" placeholder="Enter Number" name='quantity' onChange={this.setXYZvalue} onKeyDown={this.formatInput}/>
                                            <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faBars} /></span>
                                        </div>
                                        </FormGroup>
                                    </Col>
                                    <Col xs={12} sm={12} md={6} lg={6}>
                                        <FormGroup>
                                        <Label className="form-label">Quality</Label>
                                        <div className="form-input bdr-gray ">
                                            <Input type="number" className="py-0 h-auto" placeholder="Quality" name='quality' onChange={this.setXYZvalue} onKeyDown={this.formatInput}/>
                                            <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faLifeRing} /></span>
                                        </div>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={6} lg={6}>
                                        <FormGroup>
                                        <Label className="form-label">Stock material</Label>
                                        <div className="form-input bdr-gray ">
                                            <Input type="text" className="py-0 h-auto" placeholder="Enter Stock material" name='stock_material' onChange={this.setXYZvalue}/>
                                            <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faChartLine} /></span>
                                        </div>
                                        </FormGroup>
                                    </Col>
                                    <Col xs={12} sm={12} md={6} lg={6}>
                                        <FormGroup>
                                        <Label className="form-label">Additional processes needed</Label>
                                        <div className="form-input bdr-gray ">
                                            <Input type="text" className="py-0 h-auto" placeholder="Enter Additional processes needed" name='additional_process' onChange={this.setXYZvalue}/>
                                            <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faStarOfLife} /></span>
                                        </div>
                                        </FormGroup>
                                    </Col>
                                </Row>

                            </FormGroup> : 
                                this.state.selectedMachineCapabilitiesName === 'Subcontract Inspection' ?
                                <FormGroup>
                                    <Row>
                                    <Col xs={12} sm={12} md={6} lg={6}>
                                        <FormGroup>
                                        <Label className="form-label">Quantity</Label>
                                        <div className="form-input bdr-gray ">
                                            <Input type="number" className="py-0 h-auto" placeholder="Enter Number" name='quantity' onChange={this.setXYZvalue} onKeyDown={this.formatInput}/>
                                            <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faBars} /></span>
                                        </div>
                                        </FormGroup>
                                    </Col>
                                    <Col xs={12} sm={12} md={6} lg={6}>
                                        <FormGroup>
                                        <Label className="form-label">Quality</Label>
                                        <div className="form-input bdr-gray ">
                                            <Input type="number" className="py-0 h-auto" placeholder="Quality" name='quality' onChange={this.setXYZvalue} onKeyDown={this.formatInput}/>
                                            <span className="icon-wr sm mr-1"><FontAwesomeIcon icon={faLifeRing} /></span>
                                        </div>
                                        </FormGroup>
                                    </Col>
                                </Row>

                                </FormGroup> : null
                                : null
                        }
                                
                    </CardBody>
                </Card>
                <FormGroup>
                <Button className="btn-danger bg-red py-2" type="submit" onClick={this.submitPressed}>submit <img src={ImagePath.Redcircle} alt="loader" className="img-fluid ml-4 w30loader  rotating" /></Button>
                </FormGroup>
                
            </Container>
        )
    }
}
export default Cform;