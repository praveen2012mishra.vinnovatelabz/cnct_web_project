import React from 'react';
import { Container, Row, Col, Input, Card, FormGroup, CardBody, Label, Button } from 'reactstrap';
import { ImagePath } from '../../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolder, faCalendar, faCloudUploadAlt, faPoundSign, faMapMarker, faPlusCircle,faCheck } from '@fortawesome/free-solid-svg-icons';
import './stylebar.css'
import DatePicker from 'react-date-picker';
import { Multiselect } from "multiselect-react-dropdown";

class Cform extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            plainArray: ["Option 1", "Option 2", "Option 3", "Option 4", "Option 5"],
            selectedValues: [
                { key: "Option 1" }
            ]
        };
    }
    addItem() {
        this.selectedValues.push({ key: "Option 1" });
    }
    state = {
        date: new Date(),
    }

    onChange = date => this.setState({ date })

    render() {
        const { plainArray, selectedValues } = this.state;
        return (
            <Container fluid className="p-0">
                <h6 className="gray-title">CREATE NEW JOB</h6>
                <Card>
                    <CardBody className="redform">
                        <FormGroup>
                            <Label className="form-label">Job <span className="font-weight-exlight">Title</span></Label>
                            <div className="form-input">
                                <Input type="text" placeholder="Enter job Name." />
                                <span className="icon-wr"><FontAwesomeIcon icon={faFolder} /></span>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label className="form-label">Job <span className="font-weight-exlight">Type</span></Label>
                            <div className="mb-3 d-flex align-items-center">
                                <Input type="radio" id="Skill1" name="Skill" className="custom-control-input" />
                                <Label className="btn-radio" for="Skill1">Time</Label>
                                <span className="error-msg">* I need resource for a set period </span>
                            </div>
                            <div className="mb-3 d-flex align-items-center">
                                <Input type="radio" id="Skill2" name="Skill" className="custom-control-input" checked />
                                <Label className="btn-radio" for="Skill2">Project</Label>
                                <span className="error-msg">* I need resource to complete a specific project</span>
                            </div>
                        </FormGroup>
                        <Row>
                            <Col md={4}>
                                <FormGroup>
                                    <div className="form-input bdr-gray ">
                                        <DatePicker onChange={this.onChange} value={this.state.date} calendarIcon={null} clearIcon={null}
                                        />
                                        <span className="icon-wr sm"><FontAwesomeIcon icon={faCalendar} /></span>
                                    </div>
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup>
                                    <div className="form-input bdr-gray ">
                                        <DatePicker onChange={this.onChange} value={this.state.date} calendarIcon={null} clearIcon={null} />
                                        <span className="icon-wr sm"><FontAwesomeIcon icon={faCalendar} /></span>
                                    </div>
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup>
                                    <div className="round-check">
                                        <Input type="checkbox" id="ill1" className="custom-control-input" />
                                        <Label className="btn-radio w-100 " for="ill1">Dates Flexible
                                            <span className="icon-wr sm d-flex align-items-center justify-content-center"><img src={ImagePath.Check} className="img-fluid " alt="check" /></span>
                                        </Label>
                                    </div>
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup className="d-md-flex pt-3">
                            <Label className="form-label w-25 text-nowrap">Capability <span className="font-weight-exlight">Type</span></Label>
                            <Row className="w-75 pl-md-4">
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cap1" name="capability" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100" for="cap1">People</Label>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cap2" name="capability" className="custom-control-input"  />
                                        <Label className="btn-radio w-100" for="cap2">Machine</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup >
                            <Label className="form-label"> Location <span className="font-weight-exlight">Type</span></Label>
                            <Row >
                                <Col sm={6} md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="location1" name="location" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100" for="location1">Remote</Label>
                                    </div>
                                </Col>
                                <Col sm={6} md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="location2" name="location" className="custom-control-input"  />
                                        <Label className="btn-radio w-100" for="location2">On Site</Label>
                                    </div>
                                </Col>
                                <Col sm={6} md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="locationp2" name="location" className="custom-control-input"  />
                                        <Label className="btn-radio w-100" for="locationp2">Flexible</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup>
                            <Label className="form-label">Location</Label>
                            <Row>
                                <Col md={5}>
                                    <div className="d-flex">
                                        <div className="custom-control custom-radio pr-3">
                                            <Input type="radio" className="custom-control-input" id="r12" name="place" checked />
                                            <Label className="custom-control-label" for="r12">UK</Label>
                                        </div>
                                        <div className="custom-control custom-radio">
                                            <Input type="radio" className="custom-control-input" id="r21" name="place" />
                                            <Label className="custom-control-label" for="r21">International</Label>
                                        </div>
                                    </div>
                                </Col>
                                <Col md={7}>
                                <div className="form-input bdr-gray">
                                    <Input type="text" className="py-0 h-auto" placeholder="Enter Postcode" />
                                    <span className="icon-wr sm"><FontAwesomeIcon icon={faMapMarker} /></span>
                                </div>
                                </Col>
                            </Row>
                        </FormGroup>
                    </CardBody>
                </Card>
                <Card>
                    <CardBody className="redform">
                        <FormGroup>
                            <Label className="form-label pl-0">Job <span className="font-weight-exlight">Summary</span></Label>
                            <Input type="textarea" rows="4" placeholder="" defaultValue="Example: 5-axis machining of a batch of 10-off Inconel components 
                                                        for deep-sea application" />
                            <span className="form-text">* This is an overview of what is required on the job, not used for any matching algorithm</span>
                        </FormGroup>
                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label className="form-label">Industry </Label>
                                    <div className="form-input w-100">
                                        <Input type="select" name="select" id="leSelect">
                                            <option>Select industry</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Input>
                                    </div>
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label className="form-label">NDAs <span className="font-weight-exlight">Required?</span></Label>
                                    <div className="d-flex flex-smwrap">
                                        <div className="pr-2 my-1">
                                            <Input type="radio" id="yes" name="option" className="custom-control-input" checked />
                                            <Label className="btn-radio rounded" for="yes">Yes</Label>
                                        </div>
                                        <div className="pr-2 my-1">
                                            <Input type="radio" id="no" name="option" className="custom-control-input" />
                                            <Label className="btn-radio rounded" for="no">No</Label>
                                        </div>
                                        <div className="pr-2 my-1">
                                            <Input type="file" id="up" className="custom-control-input" />
                                            <Label className="btn-radio rounded bg-redfade text-white" for="up"><FontAwesomeIcon icon={faCloudUploadAlt} /></Label>
                                        </div>
                                    </div>
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label className="form-label">Company Logo / Profile Photo</Label>
                            <div class="form-input">
                                <label for="uploadpic" class="btn btn-upload">Upload Image</label>
                                <Input type="file" id="uploadpic" hidden />
                                <span className="icon-wr"><FontAwesomeIcon icon={faCloudUploadAlt} /></span>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label className="pl-0 form-label">Certification  </Label>
                            <Multiselect options={plainArray} isObject={false} selectedValues={selectedValues} />
                        </FormGroup>
                        <FormGroup className="d-md-flex pt-3">
                            <Label className="form-label w-25 text-nowrap text-muted">Certification 1</Label>
                            <Row className="w-75 pl-sm-4">
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer1" name="Certification" className="custom-control-input" />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer1">Required</Label>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer2" name="Certification" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer2">Preferred</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup className="d-md-flex pt-3">
                            <Label className="form-label w-25 text-nowrap text-muted">Certification 2</Label>
                            <Row className="w-75 pl-sm-4">
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer22" name="Certification2" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer22">Required </Label>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="cer21" name="Certification2" className="custom-control-input" />
                                        <Label className="btn-radio w-100 text-capitalize" for="cer21">Preferred</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup>
                            <Label className="form-label">Budget</Label>
                            <div className="form-input">
                                <Input type="text" className="form-control " placeholder="Enter Your Budget" />
                                <span className="icon-wr"><FontAwesomeIcon icon={faPoundSign} /></span>
                            </div>
                            <span className="form-text">* Budget information is not shared with potential providers, this is purely recorded to analyse your target versus actual project costs.</span>
                        </FormGroup>
                        <FormGroup>
                            <Label className="form-label">Preferred <span className="font-weight-exlight"> payment method</span>
                            </Label>

                            <div className="px-3 border-red rounded py-2">
                                <div className="radio-text pb-3">
                                    <div className="custom-control custom-radio">
                                        <Input type="radio" className="custom-control-input" id="r1" name="stacked" />
                                        <Label className="custom-control-label" for="r1">INSTANT CNCT</Label>
                                    </div>
                                    <p>INSTANT CNCT is our immediate payment method. Electronic payment is taken from
                                    the buyer at the point that the project is agreed. Once the project is
                                    completed, both parties confirm that it has been completed correctly and
                                    payment is automatically sent to the seller's account
                                    (minus the transaction fee). Buyer's receive a sales invoice from
                                         CNCT and seller's receive a purchase invoice from CNCT</p>
                                </div>
                                <div className="radio-text pb-3">
                                    <div className="custom-control custom-radio">
                                        <Input type="radio" className="custom-control-input" id="r2" name="stacked" checked />
                                        <Label className="custom-control-label" for="r2">SPECIAL TERMS</Label>
                                    </div>
                                    <p>Sometimes, on larger projects, we appreciate that extended payment
                                    terms are needed. Special Terms allows the buyers and seller to agree their
                                    own payment terms and even upload terms and conditions of business. When the
                                    project is agreed, we take a payment equal to the seller's transaction fee from
                                    the buyer (akin to a deposit). When the project is completed, we send a completion
                                    note to both the buyer and seller and the seller invoices the buyer independently
                                         and directly for the balance (agreed project fee, mins transaction fee).</p>
                                </div>
                                <div className="mb-3">
                                    <Input type="file" id="up" className="custom-control-input" />
                                    <Label className="btn-radio bg-red text-white text-capitalize" for="up">Upload Terms &nbsp; &nbsp; <FontAwesomeIcon icon={faCloudUploadAlt} /></Label>
                                </div>
                            </div>
                        </FormGroup>
                    </CardBody>
                </Card>
                <div className="mb-3">
                    <h6 className="gray-title">CAPABILITIES</h6>
                </div>
                <Card className="redform">
                    <CardBody>
                        <FormGroup>
                            <Label className="form-label">Skill</Label>
                            <div className="pt-2 d-flex flex-wrap">                                
                                    <div className="clearfix my-2 mx-2">
                                        <Input type="radio" id="pc1" name="Cer1" className="custom-control-input" checked />
                                        <Label className="btn-radio text-capitalize" for="pc1">Design </Label>
                                    </div>                             
                              
                                    <div className="clearfix my-2 mx-2">
                                        <Input type="radio" id="pc2" name="Cer1" className="custom-control-input"  />
                                        <Label className="btn-radio  text-capitalize" for="pc2">Offline CNC Programming</Label>
                                    </div>
                             
                                    <div className="clearfix my-2 mx-2">
                                        <Input type="radio" id="pc3" name="Cer1" className="custom-control-input" />
                                        <Label className="btn-radio  text-capitalize" for="pc3">Offline CMM Programming</Label>
                                    </div>
                                    <div className="clearfix my-2 mx-2">
                                        <Input type="radio" id="pc4" name="Cer1" className="custom-control-input" />
                                        <Label className="btn-radio  text-capitalize" for="pc4">Other People Capabilities</Label>
                                    </div>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label className="pl-0 form-label">Specialism  </Label>
                            <Multiselect options={plainArray} isObject={false} selectedValues={selectedValues} />
                        </FormGroup>
                        <FormGroup>
                            <Label className="pl-0 form-label">Design Software  </Label>
                            <Multiselect options={plainArray} isObject={false} selectedValues={selectedValues} />
                        </FormGroup>
                        <FormGroup>
                            <Row>
                                <Col sm={5}>
                                    <div className="form-input bdr-gray overflow-hidden">
                                        <button type="button" className="btn text-muted d-flex justify-content-between w-100"><span>Other</span> <FontAwesomeIcon icon={faPlusCircle} className="mt-1" /></button>
                                    </div>
                                </Col>
                                <Col sm={5}>
                                    <div className="form-input bdr-gray">
                                        <Input type="text" placeholder="Other Software" />
                                    </div>
                                </Col>
                                <Col sm={2} className="pl-0 pr-md-3">
                                    <button className="btn " type="button"> <span className="icon-wr ml-0 bg-redfade text-white"><FontAwesomeIcon icon={faCheck} /></span></button>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup className="pt-3 addrow">   
                            <Button className="btn bg-transparent border-0 p-0" type="reset">
                                 <img src={ImagePath.Closered} alt="close" className="img-fluid" />
                            </Button>                       
                            <Row >
                                <Col md={4}>
                                    <Label className="form-label text-nowrap text-muted">Autodesk autocad</Label>
                                </Col>
                                <Col md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="autocad1" name="autocad" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100 text-capitalize" for="autocad1">Required </Label>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="autocad2" name="autocad" className="custom-control-input" />
                                        <Label className="btn-radio w-100 text-capitalize" for="autocad2">Preferred</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup className=" pt-3 addrow">       
                            <Button className="btn bg-transparent border-0 p-0" type="reset">
                                 <img src={ImagePath.Closered} alt="close" className="img-fluid" />
                            </Button>                        
                            <Row >
                                <Col md={4}>
                                    <Label className="form-label text-nowrap text-muted">Autodesk Fusion 360 </Label>
                                </Col>
                                <Col md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="Fusion1" name="Fusion" className="custom-control-input"  />
                                        <Label className="btn-radio w-100 text-capitalize" for="Fusion1">Required </Label>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="Fusion2" name="Fusion" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100 text-capitalize" for="Fusion2">Preferred</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup className="addrow pt-3">            
                             <Button className="btn bg-transparent border-0 p-0" type="reset">
                                 <img src={ImagePath.Closered} alt="close" className="img-fluid" />
                            </Button>                   
                            <Row >
                                <Col md={4}>
                                    <Label className="form-label text-nowrap text-muted">Other Software</Label>
                                </Col>
                                <Col md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="Other1" name="Other" className="custom-control-input" checked />
                                        <Label className="btn-radio w-100 text-capitalize" for="Other1">Required </Label>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="clearfix mb-2">
                                        <Input type="radio" id="Other2" name="Other" className="custom-control-input" />
                                        <Label className="btn-radio w-100 text-capitalize" for="Other2">Preferred</Label>
                                    </div>
                                </Col>
                            </Row>
                        </FormGroup>
                    </CardBody>
                </Card>
                <FormGroup>
                    <Button className="btn-danger bg-red py-2" type="submit">submit <img src={ImagePath.Redcircle} alt="loader" className="img-fluid ml-4 w30loader  rotating" /></Button>
                </FormGroup>

            </Container>
        )
    }
}
export default Cform;