import React from 'react';
import { Container, Card, CardBody, Button, } from 'reactstrap';
import { ImagePath } from '../../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilter, faBriefcase, faUser, faBookOpen, faCalendar, faClock, faLongArrowAltLeft, faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import './stylebar.css';
import BASE_API_URL from '../../Config/config';
import { getApiWithToken } from '../../Config/api';
import moment from 'moment';
import SubcontractingApplyJob from '../../Dashboard/SubconductingApplyJob';
import { withRouter } from "react-router-dom";

class greenform extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentIndex: 0,
            highestCount: 10,
            arrayCount: 2,
            totalArrayElement: [],
            matchedOppertunitiesList: [],
            totalJobOppertunities: ''
        }
    }

    componentDidMount() {
        this.get_ButtonCount();
        this.get_matchedOppertunitiesList()
    }
    get_ButtonCount() {
        this.state.totalArrayElement = [];
        console.log("this.state.arrayCount:  ", this.state.arrayCount);
        let arrayElement = this.state.totalArrayElement;
        for (let index = this.state.currentIndex + 1; index <= this.state.arrayCount; index++) {
            arrayElement.push(index);
        }
        this.setState({
            totalArrayElement: arrayElement
        })
    }

    nextClick = () => {
        if (this.state.arrayCount < this.state.highestCount) {
            this.setState({
                currentIndex: this.state.arrayCount,
                arrayCount: this.state.arrayCount + 2,
            }, () => {
                this.get_ButtonCount()
            })
        }

    }
    previousClick = () => {
        if (this.state.arrayCount > 2) {
            this.setState({
                arrayCount: this.state.arrayCount - 2,
                currentIndex: this.state.arrayCount - 4
            }, () => {
                this.get_ButtonCount()
            })
        }
    }
    dataLoad = (getIndex) => {
        console.log("tag:  ", getIndex.target.value);
    }
    applyButtonClick = (e) => {
        console.log("Job Id:  ", e.target.value);
        this.props.history.push(`/SubconductingApplyJob/${e.target.value}`);
        // this.props.history.push({
        //     pathname: '/SubconductingApplyJob',
        //     customNameData: e.target.value
        //   })
    }
    get_matchedOppertunitiesList() {
        let userid = localStorage.getItem('userId');
        //http://111.93.169.90:2030/user/[user_id]/job/
        getApiWithToken(BASE_API_URL + 'user/' + userid + '/job/listget/')
            .then(response => {
                console.log('job response: ', response.data.results);
                this.setState({
                    matchedOppertunitiesList: response.data.results,
                    totalJobOppertunities: response.data.count
                })
            })
            .catch(err => {
                console.log(err);
            });
    }

    render() {
        return (
            <Container fluid>
                <div className="filter-green">
                    <h6 className="text-green mb-0">New Matched Opportunities</h6>
                    <Button color="green w-auto rounded"><FontAwesomeIcon icon={faFilter} /></Button>
                </div>
                <div className="subcon">
                    <div className="d-flex align-items-center py-3 px-2 title border-bottom mb-4">
                        <span className="text-green"><FontAwesomeIcon icon={faBriefcase} /></span>
                        {this.state.totalJobOppertunities > 0 ?
                            this.state.totalJobOppertunities === 1 ?
                            <span className="d-block ml-3">{this.state.totalJobOppertunities} Matched Job Found</span> :
                            <span className="d-block ml-3">{this.state.totalJobOppertunities} Matched Jobs Found</span> :
                            <span className="d-block ml-3">{this.state.totalJobOppertunities} No Matched Job Found</span>
                        }
                        
                    </div>
                    {this.state.matchedOppertunitiesList.map((jobDetails, index) => (
                        <Card>
                            <CardBody className="greenformpart">
                                <div className="d-sm-flex justify-content-between mb-4">
                                    <Button color="light">{jobDetails.job_title}</Button>
                                    <div className="d-flex align-items-center rating">
                                        <span>Supplier Rating</span>
                                        <span className="greenrate">7.5</span>
                                    </div>
                                </div>
                                <div className="name-comp d-flex mb-3">
                                    <div className="d-flex mr-3">
                                        <span className="text-green mr-2"><FontAwesomeIcon icon={faUser} /></span>
                                        <span>Toolmaker Ltd</span>
                                    </div>
                                    <div className="d-flex mr-3">
                                        <span className="text-green mr-2"><FontAwesomeIcon icon={faBookOpen} /></span>
                                        {jobDetails.job_type === 'T' ?
                                            <span>Time</span> :
                                            <span>Project</span>
                                        }
                                    </div>
                                </div>
                                <div className="dates d-md-flex mb-3">
                                    <div className="d-flex mr-3 mb-3 mb-md-0">
                                        <span className="text-green mr-2"><FontAwesomeIcon icon={faCalendar} /></span>
                                        <span>Delivery Date : <span className="text-black">{moment(jobDetails.end_date).format("DD/MM/yyyy")}</span></span>
                                    </div>
                                    <div className="d-flex">
                                        <span className="text-green mr-2"><FontAwesomeIcon icon={faClock} /></span>
                                        <span>Posted : <span className="text-black">{moment(jobDetails.created_on).format("DD/MM/yyyy, h:mm a")}</span></span>
                                    </div>
                                </div>
                                <div className="d-sm-flex mb-3">
                                    <div className="btn-tags mr-3">
                                        <span className="tags green">Capability</span>
                                        <span className="tags ">Design</span>
                                    </div>
                                    <div className="btn-tags mr-3">
                                        <span className="tags green">Specialism</span>
                                        <span className="tags ">Mould Tool Design</span>
                                    </div>
                                </div>
                                <div className="d-sm-flex mb-3">
                                    <div className="btn-tags mr-3">
                                        <span className="tags green">NDA Required</span>
                                        {jobDetails.is_nda ?
                                            <span className="tags ">Yes</span> :
                                            <span className="tags ">No</span>
                                        }

                                    </div>
                                </div>
                                <div className="d-flex pt-3 justify-content-between align-items-center bttn-wrap">
                                    <Button color="success rounded" value={jobDetails.id} onClick={this.applyButtonClick}>Apply <img src={ImagePath.Smloader} alt="loader" className="img-fluid ml-2 rotating" /></Button>
                                    {jobDetails.is_flexible === 'R' ?
                                        <h5 className="mb-0 text-green font-weight-bold">Remote</h5> :
                                        jobDetails.is_flexible === 'O' ?
                                            <h5 className="mb-0 text-green font-weight-bold">On-Site</h5> :
                                            <h5 className="mb-0 text-green font-weight-bold">Flexible</h5>
                                    }

                                </div>

                            </CardBody>
                        </Card>
                    ))}
                    <div className="pager d-flex justify-content-center py-5">
                        <ul className="list-inline pagination">
                            <li><Button className="nextpage" onClick={this.previousClick}><FontAwesomeIcon icon={faLongArrowAltLeft} /></Button></li>
                            {this.state.totalArrayElement.map((count, index) => (
                                <li><Button onClick={this.dataLoad} value={count}>{count}</Button></li>
                            ))}
                            <li><Button className="prevpage" onClick={this.nextClick}><FontAwesomeIcon icon={faLongArrowAltRight} /></Button></li>
                        </ul>
                    </div>
                </div>
            </Container>
        )
    }
}
export default withRouter(greenform)  ;