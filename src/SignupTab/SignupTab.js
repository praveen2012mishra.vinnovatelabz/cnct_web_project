import React, { useState} from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink} from 'reactstrap';
import TabContentOne from './TabContentOne';
import TabContentTwo from './TabContentTwo';
import TabContentThree from './TabContentThree';
import TabContentFour from './TabContentFour';
import Succesful from './Succesful';
import classnames from 'classnames';
import './style.css';
import Dashboard from '../Dashboard/Dashboard';

const SignupTab = (props) => {
    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
        console.log("getting called");
        if (activeTab !== tab) setActiveTab(tab);
        if(tab === '3') {
            localStorage.setItem('pageType', tab);
        }if(tab === '4') {
            localStorage.setItem('payment', tab);
        }
        
        // {localStorage.hasOwnProperty("tempUserId")}
    }

    return (
        <div>
            <section className="br-nav">
                <div className="container">
                    <div className="nav-wrap">
                        <Nav tabs>
                            <NavItem>
                                <NavLink className={classnames({ active: activeTab === '1' })} onClick={() => { toggle('1'); }}>
                                    <span class="number">01</span> Basic Info
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className={classnames({ active: activeTab === '2' })} onClick={() => { toggle('2'); }}>
                                    <span class="number">02</span> Profile Type
                                </NavLink>
                            </NavItem>
                            {localStorage.getItem('pageType') === '3'?
                            <NavItem>
                                <NavLink className={classnames({ active: activeTab === '3' })} onClick={() => { toggle('3'); }}>
                                    <span class="number">03</span> Payment
                                </NavLink>
                            </NavItem>: null
                            }
                            {localStorage.getItem('payment') === '4'?
                            <NavItem>
                                <NavLink className={classnames({ active: activeTab === '4' })} onClick={() => { toggle('4'); }} >
                                    <span class="number">04</span> Capabilities
                                </NavLink>
                            </NavItem>:null
                            }
                        </Nav>
                    </div>
                </div>
            </section>
            

            <section className="tab-content myTabContent">
                <TabContent activeTab={activeTab}>
                    <TabPane tabId="1" 
                     
                    >
                        <TabContentOne  activeTab={activeTab} showModal={activeTab} setShowModal={setActiveTab} setToggle={toggle}/>
                    </TabPane>
                   
                    <TabPane tabId="2">
                        <TabContentThree setToggle={toggle} />
                    </TabPane>

                    <TabPane tabId="3">
                        <TabContentFour setToggle={toggle} />
                    </TabPane>
                    <TabPane tabId="4">
                        <TabContentTwo  setToggle={toggle}/>
                        {/* <Dashboard /> */}
                    </TabPane>
                                   
                </TabContent>
            </section>
     </div>
    );
}


export default SignupTab