import React from 'react';
import { Container, Row, Col, Input, FormGroup, Label } from 'reactstrap';
import { ImagePath } from './../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faCheck, faSortNumericDownAlt, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { Button } from 'react-bootstrap';
import BASE_API_URL from '../Config/config';
import {getApi, postApi, apiCallWithToken} from '../Config/api';

import { Multiselect } from "multiselect-react-dropdown";

class PeopleActivities extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      howInterested: '',
      specialisation_list: [],
      designSoftware_list: [],
      inspectionSoftware_list: [],
      programingSoftware_list: [],
      simulationSoftware_list: [],
      selectedSkillId: '',
      selectedMachineCapabilities: 'Subcontract Inspection',
      selectedValues: [
        { key: "Option 1" }
      ]
    };
  }

  componentDidMount() {

    // this.get_skillListItems("people");
    // this.get_skillListItems("machine");
    console.log('input arr', this.state.plainArray);
  }

  static getDerivedStateFromProps(props, state) {
    // Any time the current user changes,
    // Reset any parts of state that are tied to that user.
    // In this simple example, that's just the email.
    return {
        skill_list: props.value,
        plainArray: props.otherArray
      };
  }

  addItem() {
    this.selectedValues.push({ key: "Option 1" });
  }

  handleEntitySelection = (e, index) => {
      console.log("get index: ", index);
      let tempArr = [...this.state.plainArray];

      const isOpen = !tempArr[index].isOpen;
      tempArr = tempArr.map(elem => {
        elem.isOpen = false;
        return elem
     });
      tempArr[index].isOpen = isOpen;

    this.setState({
      howInterested: e.target.value,
      plainArray: tempArr
    });
  }
  handleClassification = event => {
    console.log("total event:  ",event.target.value);
    let index = event.target.value;
    this.setState({
      selected_hardware:this.state.hardware_classifications[index]
    });
  }

  getSelectedSkillItem = (e) => {
    this.setState({
      selectedSkillId: e.target.id
    })
    this.get_specilizationAndSoftwareList(e.target.id);
    
  }
  selectedOption = (e) => {
      console.log("dfgdgdgdffdfffffffffff:   ", e.target.id);
  }
  clearSoftwareData() {
    this.setState({
      designSoftware_list: [],
      simulationSoftware_list: [],
      programingSoftware_list: [],
      inspectionSoftware_list: []
    })
  }

  clear_selectedHardwareSpecifications() {
    this.setState({
      hardware_classifications: []
    })
  }

  // all APIs call here

  get_skillListItems(type) {
    apiCallWithToken(BASE_API_URL + 'web/skilllist/', 
    'post',
    { "skill_type":type })
      .then(response => {
        if(type === 'people') {
        //   this.setState({
        //     skill_list: response.data.allskill
        //   })
        }else {
          this.setState({
            machine_capabilities: response.data.allskill
          })
        }
        
      })
      .catch(err => {
        console.log(err);
      });
  }

  get_specilizationAndSoftwareList(parent_id) {
    if(parent_id === '7') {

    }else {
      this.clearSoftwareData();
    }
    
    apiCallWithToken(BASE_API_URL + 'web/peoplefetch/', 
    'post',
    { "parent_id":parent_id })
      .then(response => {
        var allData = response.data.softwares;
        if(allData.length > 0) {
          for (var i = 0; i < allData.length; i++) {
            let details = allData[i];
            if (details.software_type === 'Design Software') {
              this.setState({designSoftware_list: [...this.state.designSoftware_list, details]})
            }else if (details.software_type === 'Simulation Software') {
              this.setState({simulationSoftware_list: [...this.state.simulationSoftware_list, details]})
            }else if (details.software_type === '"Programming Software') {
              this.setState({programingSoftware_list: [...this.state.programingSoftware_list, details]})
            }else {
              this.setState({inspectionSoftware_list: [...this.state.inspectionSoftware_list, details]})
            }
          }
        }
        if(parent_id === '7') {
          this.setState({
            machine_specialism: response.data.specialism
          })
        }else {
          this.setState({
            specialisation_list: response.data.specialism
          })
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  get_hardwareClassification(id) {
    this.clear_selectedHardwareSpecifications();
    console.log('hardware_classifications ID:  ', id);
    apiCallWithToken(BASE_API_URL + 'web/classificationlist/', 
    'post',
    { "parent_id":id })
      .then(response => {
        console.log('hardware_classifications:  ', response.data.classifications.length);
        this.setState({
          hardware_classifications: response.data.classifications
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { plainArray, selectedValues } = this.state;
    console.log(plainArray);
    return (
      <React.Fragment>
        <div className="capability-tab common-padd bgright">
          <Container>
          {plainArray && plainArray.map((myArray, i) => (
            <Row key={i}>
                <Col md="6">
                    <FormGroup>
                    <Label className="pl-0">How are you interested to work?</Label>
                    <div className="btn-flex d-flex">
                        <div className="pr-2">
                        <Input type="radio" id={'cr1-'+i} name="rem" className="custom-control-input" onChange={(e) => this.handleEntitySelection(e, i)} value='Remote' />
                        <Label className="btn-radio" for={'cr1-'+i}>Remote </Label>
                        </div>
                        <div className="pr-2">
                        <Input type="radio" id={'cr2-'+i} name="rem" className="custom-control-input" onChange={(e) => this.handleEntitySelection(e, i)} value='On-Site' />
                        <Label className="btn-radio" for={'cr2-'+i}>On-Site</Label>
                        </div>
                        <div className="pr-2">
                        <Input type="radio" id={'cr3-'+i} name="rem" className="custom-control-input" onChange={(e) => this.handleEntitySelection(e, i)} value='Flexible' />
                        <Label className="btn-radio" for={'cr3-'+i}>Flexible</Label>
                        </div>
                    </div>
                    </FormGroup>
                </Col>
                
                {(this.state.howInterested === "On-Site" || this.state.howInterested === "Flexible" && myArray.isOpen)?
                    <Col md="6">
                    <FormGroup>
                        <Label>Willing-to-Travel Radius</Label>
                        <div className="form-input" style={{ width: "200px" }}>
                        <Input type="text" className="form-control" placeholder="Enter Name" />
                        <span className="mm">mm</span>
                        </div>
                    </FormGroup>
                    </Col> : null
                }
                <Col md="12">
                    <FormGroup>
                    <label className="pl-0">Skill</label>
                    <div className="btn-flex d-flex">
                    {this.state.skill_list.map((skills, childIndex) => (
                            <div className="pr-2" key={childIndex}>
                            <Input type="radio" id={skills.id} className="custom-control-input" name="skills" onChange={this.simpleLog} value={skills.skill_name}/>
                            <Label className="btn-radio" for={skills.id}>{skills.skill_name} </Label>
                        </div>
                        ))}
                    </div>
                    </FormGroup>
                </Col>
                {this.state.specialisation_list.length > 0 ?
                <Col md="12">
                    <FormGroup>
                    <Label className="pl-0">Specialism </Label>
                    <Multiselect options={this.state.specialisation_list} displayValue='specialism_name' />
                    
                    </FormGroup>
                </Col>
                : null
                }
                {this.state.designSoftware_list.length > 0 ?
                    <Col md="12">
                    <FormGroup>
                        <Label className="pl-0">Design Software </Label>
                        <Multiselect options={this.state.designSoftware_list} displayValue='software_name' />
                    </FormGroup>
                    </Col>: null
                }
                {this.state.programingSoftware_list.length > 0 ?
                    <Col md="12">
                    <FormGroup>
                        <Label className="pl-0">Programming Software </Label>
                        <Multiselect options={this.state.programingSoftware_list} displayValue='software_name' />
                    </FormGroup>
                    </Col>: null
                }
                {this.state.simulationSoftware_list.length > 0 ?
                    <Col md="12">
                    <FormGroup>
                        <Label className="pl-0">Simulation Software </Label>
                        <Multiselect options={this.state.simulationSoftware_list} displayValue='software_name' />
                    </FormGroup>
                    </Col>: null
                }
                {this.state.inspectionSoftware_list.length > 0 ?
                    <Col md="12">
                    <FormGroup>
                        <Label className="pl-0">Inspection Software </Label>
                        <Multiselect options={this.state.inspectionSoftware_list} displayValue='software_name' />
                    </FormGroup>
                    </Col>: null
                } 
            </Row>
            ))}
          </Container>
        </div>
      </React.Fragment>
    );
  }
}
export default PeopleActivities;