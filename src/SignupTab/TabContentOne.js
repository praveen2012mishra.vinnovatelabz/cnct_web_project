import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Input, Form, FormGroup, Label, Button } from 'reactstrap';
import {ImagePath} from './../ImagePath';
import {apiCallWithToken} from '../Config/api';
import BASE_API_URL from '../Config/config';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBuilding, faUser, faEnvelope, faAddressBook, faLock, faImage, faBars } from '@fortawesome/free-solid-svg-icons';
// import Select from 'react-select'
import countryList from 'react-select-country-list'
import validator from 'validator';

const TabContentOne = (props) =>{

const [userDetails, setUserDetails] = useState({ 
    companyCountry: '',
    companyName: '', 
    name: '',
    email: '',
    postCode: '',
    address: '',
    linkedin_link: '',
    password: '',
    confirmPassword: '',
    ukRegistrationNumber: '',
    companyBio: '',
    customerExp: '',
    otherRegistrationNumber: '',
    vatNo: '',
    agreeTermsAndCondition: ''
});

const [checkRegistrationNo, ukRegistrationNoChecking] = useState(false);
const [checkVatRegister, vatRegisterChecking] = useState(false);
const [hearFromOther, hearFromOtherChecking] = useState(false);
const [checktermsCondition, termsConditionChecking] = useState(false);
const [entitySize, setEntitySize] = useState('');
const [industries, setIndustries] = useState([]);
const [whyJoining, setWhyJoining] = useState('');
const [whereHear, setWhereHear] = useState('');
const [allCountryList, setCountryList] = useState([]);
const [propertyImage, updatePropertyImage] = useState(null);
const [propertyImageFile, updatePropertyImageFile] = useState(null);
const fileData = null;
const [imageName, setImageName] = useState(null);

const allEntitySize = [
    {id: 101, value: 'Individual', shortName: 'IN'},
    {id: 102, value: 'Micro', shortName: 'MI'},
    {id: 103, value: 'Small', shortName: 'SM'},
    {id: 104, value: 'Medium', shortName: 'ME'},
    {id: 105, value: 'Large', shortName: 'LA'},
    {id: 106, value: 'Enterprise', shortName: 'EN'},
]
const allIndusExperience = [
    {id: 110, value: 'Aerospace'},
    {id: 111, value: 'Automotive'},
    {id: 112, value: 'Construction'},
    {id: 113, value: 'Consumer Goods'},
    {id: 114, value: 'Defense'},
    {id: 115, value: 'Electric Vehicles'},
    {id: 116, value: 'Marine'},
    {id: 117, value: 'Medical'},
    {id: 118, value: 'Mining'},
    {id: 119, value: 'Motorsport'},
    {id: 120, value: 'Oil and Gas'},
    {id: 121, value: 'Renewable Energy'},
    {id: 122, value: 'Robotics/AI/Automation'},
]
const allWhereHear = [
    {id: 11, value: 'email'},
    {id: 12, value: 'Twitter'},
    {id: 13, value: 'LinkedIn'},
    {id: 14, value: 'userReferral'},
    {id: 15, value: 'Other Industry Referral'},
]
useEffect(() => {
    setCountryList(countryList().getData())
}, [])

  const handleInputValue = e => {
    setUserDetails({
      ...userDetails,
      [e.target.name]: e.target.value
    });
  };

const handleExpSelection = e => {
    let isChecked = e.target.value;
    console.log('selection check', isChecked);
    // do whatever you want with isChecked value
    if (industries.length > 0){
        if (industries.splice(e.target.value));
    }
};

const handleEntitySelection = e =>{
    setEntitySize(e.target.value);
}
const handleWhyJoining = e =>{
    setWhyJoining(e.target.value);
}
const handleWhereHear = e =>{
    setWhereHear(e.target.value);
}

  const saveUserBasicInfo = () => {
    // props.setToggle('2');
    // window.scrollTo(0, 0)

      let allDetails = {
        'company_name' : userDetails.companyName,
        'name' : userDetails.name,
        'email' : userDetails.email,
        'password' : userDetails.password,
        'company_address' : userDetails.address,
        'entity_size' : entitySize,
        'company_bio' : userDetails.companyBio,
        'customer_experience' : userDetails.customerExp,
        'company_postcode' : userDetails.postCode,
        'company_country' : userDetails.companyCountry,
        'avatar' : '',
        'why_us' : whyJoining,
        'hear_about_us' : whereHear,
        "linkedin_link": "https://www.google.com",
        'industry_type': "1,2,3"
      };
      
      if (!checkRegistrationNo){
        allDetails['company_registration_number'] = userDetails.ukRegistrationNumber;
      }
      if (!checkVatRegister){
        allDetails['vat_number'] = userDetails.ukRegistrationNumber;
      }

      if (userDetails.companyName === ''){
        alert('Please enter company name');
      }else if (userDetails.name === ''){
          alert('Please enter your name');
      }
      else if (userDetails.companyCountry === ''){
          alert('Please select country');
      }
      else if (userDetails.email === ''){
          alert('Please enter email id');
      }else if (!validator.isEmail(userDetails.email)){
        alert('Please enter valid email id');
    }else if (userDetails.postCode === ''){
          alert('Please enter postcode');
      }else if (userDetails.password === ''){
          alert('Please enter password');
      }else if (userDetails.password !== userDetails.confirmPassword){
          alert('Password mismatch. Please re-enter your password');
      }else if (userDetails.entitySize === ''){
        alert('Please choose entity size');
      }else if (!checktermsCondition){
          alert('Please accept Terms & Conditions');
      }else{
          //http://111.93.169.90:2030/web/registerbasicinfo/
        apiCallWithToken(BASE_API_URL + 'web/registerbasicinfo/', 
        'post',
        allDetails)
            .then(response => {
                alert('Details saved successfully. Please choose Profile Type to complete the signup process.');
                props.setToggle('2');
                window.scrollTo(0, 0);
                localStorage.setItem('tempUserId', response.data.user['id']);
            })
            .catch(err => {
                alert("Some error occured.")
            })
        }
  };

  const handleHearFromOtherChange = async() => {
    await hearFromOtherChecking(!hearFromOther)
        
  }
  const handleUkRegistrationChecking = async() => { 
    await ukRegistrationNoChecking(!checkRegistrationNo)
  }
  const handleVatChecking = async() =>{
    await vatRegisterChecking(!checkVatRegister);
 }
 const handleTermsAndConditionChecking = async() =>{
    await termsConditionChecking(!checktermsCondition);
 }
  const handleFileChange = e => {
    const files = Array.from(e.target.files);
      // data.property_image = files;
      console.log("cheking file type:  ", e.target.file);
      setImageName(e.target.files[0].name)
      Promise.all(
        files.map(file => {
          updatePropertyImageFile(file);
          return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.addEventListener('load', ev => {
              resolve(ev.target.result);
            });
            reader.addEventListener('error', reject);
            reader.readAsDataURL(file);
          });
        }),
      ).then(
        images => {
            updatePropertyImage(images);
        },
        error => {
          console.error(error);
        },
      );
  };

  const validation = () => {
    let value = {};
    const regEm = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/gim;

    if (userDetails.email) {
      if (regEm.test(userDetails.email)) {
        value = { valid: true };
      } else {
        value = { invalid: true };
      }
    }

    return {
      emailValue: value,
    };
  };

  const value = validation();

  return(
    <React.Fragment>
      <div className="basic-tab common-padd bgright">
        <Container>
          <div className="in-basic">
            <Form>
              <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                <h2 className=" text-green">Headlines </h2>
                <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader"></img></span>
              </div>
              <Row>
              <Col sm="6">
                  <FormGroup>
                      <Label>Company Name </Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Name Here" name='companyName' onChange={handleInputValue} value={userDetails.companyName}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faBuilding} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Name</Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Enter Name" name="name" onChange={handleInputValue} value={userDetails.name}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                    <FormGroup>
                        <Label>Company Country </Label>
                        <div className="form-input">
                            <select className="form-control" name="companyCountry" onChange={handleInputValue}>
                            {allCountryList.map((country, index) => (
                                <>
                                <option value="none" selected disabled hidden>
                                Select
                              </option>
                              <option key={index} value={country.label}>{country.label}</option>
                                </>
                            ))}                               
                            </select>
                        </div>
                    </FormGroup>
                </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Email </Label>
                      <div className="form-input">
                           {/* {...value.emailValue} */}
                          <Input type="email" className="form-control" name="email" placeholder="Enter Email Id" onChange={handleInputValue} value={userDetails.email}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faEnvelope} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Postcode Lookup</Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Enter Postcode" name="postCode" onChange={handleInputValue} value={userDetails.postCode}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faAddressBook} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Enter Manually</Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="ex: enter address" name="address" onChange={handleInputValue} value={userDetails.address}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Company Logo / Profile Photo</Label>
                      <div className="form-input">
                          <Label for="uploadpic" className="btn btn-upload">Upload Image</Label>
                          <Input type="file" id="uploadpic" hidden onChange={e => handleFileChange(e)}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faImage} /></span>
                      </div>
                      
                      <div className="row">
                      
                        <div className="col-md-4">
                            <Label style={{fontSize: 12}} >
                            {imageName}
                            </Label>
                                    {/* <Input
                                    value={imageName}
                                    readOnly
                                    className="form-group"
                                    /> */}
                                    {/* <FontAwesomeIcon
                                    style={{ position: 'absolute', top: '-7' }}
                                    // icon={faTimesCircle}
                                    /> */}
                                
                            </div>
                      
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>LinkedIn </Label>
                      <div className="form-input">
                          <Input type="text" className="form-control" placeholder="http:// www.linkedin.com" name="linkedin_link" onChange={handleInputValue} value={userDetails.linkedin_link}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Password </Label>
                      <div className="form-input">
                          <Input type="password" className="form-control" placeholder="*******" name="password" onChange={handleInputValue} value={userDetails.password}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faLock} /></span>
                      </div>
                  </FormGroup>
              </Col>
              <Col sm="6">
                  <FormGroup>
                      <Label>Re-Enter Password</Label>
                      <div className="form-input">
                          <Input type="password" className="form-control" placeholder="*******" name="confirmPassword" onChange={handleInputValue} value={userDetails.confirmPassword}/>
                          <span className="icon-wr"><FontAwesomeIcon icon={faLock} /></span>
                      </div>
                  </FormGroup>
              </Col>
            </Row>
            <div className="d-flex justify-content-between align-items-center border-bottom pb-3 my-4">
                <h2 className=" text-green">Legal <span class="font-weight-exlight">Entity </span> </h2>
                <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader"></img></span>
            </div>
            
            <Row>
                <Col md="6">
                    <FormGroup>
                        <Label>UK Company Registration Number</Label>
                        <div className="my-4 ml-3">
                            <div className="custom-control custom-checkbox ">
                                <Input type="checkbox" className="custom-control-input" id="ck1" name="vatRegisterChecking" value={checkRegistrationNo} onChange={handleUkRegistrationChecking} defaultChecked={checkRegistrationNo}/>
                                <Label className="custom-control-label" for="ck1">I am not a UK-based limited company, registered with companies house</Label>
                            </div>
                        </div>
                        
                        {checkRegistrationNo===true?
                            <div className="form-input bg-light">
                                <Input type="text" disabled={true} className="form-control bg-transparent" placeholder="Enter reg no." name="ukRegistrationNumber" />
                                <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                            </div> : 
                            <div className="form-input">
                                <Input type="text" disabled={false} className="form-control " placeholder="Enter reg no." name="ukRegistrationNumber" onChange={handleInputValue} value={userDetails.ukRegistrationNumber}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                            </div>
                        }
                        
                    </FormGroup>
                </Col>
                <Col md="6">
                    <FormGroup>
                        <Label>VAT Registration Number</Label>
                        <div className="my-4 ml-3">
                            <div className="custom-control custom-checkbox">
                                <Input type="checkbox" className="custom-control-input" id="ck2" name="vatRegisterChecking" value={checkVatRegister} onChange={handleVatChecking} defaultChecked={checkVatRegister}/>
                                <Label className="custom-control-label" for="ck2">I am not VAT-registered <br /> &nbsp;</Label>
                            </div>
                        </div>
                        {checkVatRegister === true?
                            <div className="form-input bg-light">
                                <Input type="text" disabled={true} className="form-control bg-transparent" placeholder="Enter reg no." name="vatNo" />
                                <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                            </div> : 
                            <div className="form-input">
                                <Input type="text" disabled={false} className="form-control " placeholder="Enter reg no." name="vatNo" onChange={handleInputValue} value={userDetails.vatNo}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                            </div>
                        }
                        
                    </FormGroup>
                </Col>
            </Row>
            <div className="my-4 ml-3">
                <div className="custom-control custom-checkbox">
                    <Input type="checkbox" className="custom-control-input" id="ck3" name="checktermsCondition" onChange={handleTermsAndConditionChecking} value={checktermsCondition}/>
                    <Label className="custom-control-label" for="ck3">I agree to the terms and conditions of use of the CNCT platform. I will take full responsibility for the appropriate tax declarations relevant to my territory. (These words will be amended to proper legal wording with a pop up to view full Ts and Cs) </Label>
                </div>
            </div>
          </Form>
          </div>
        </Container>


        <section className="common-padd bgleft">
                <Container>
                    <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                        <h2 className=" text-green">Details </h2>
                        <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
                    </div>
                    <Row>
                        <Col md="6">
                            <FormGroup>
                                <Label className="pl-0 mb-3">Entity Size </Label>
                                <div>
                                {allEntitySize.map((entity, index) => (
                                    <div className="custom-control custom-radio">
                                    <Input type="radio" className="custom-control-input" id={entity.id} name="entitySize" onChange={handleEntitySelection} value={entity.shortName}/>
                                    <Label className="custom-control-label" for={entity.id}>{entity.value}</Label>
                                  </div>
                                ))}
                                </div>
                                  <span className="d-block text-green">*Company size has no bearing on price </span>
                            </FormGroup>
                        </Col>
                        <Col md="6">
                        <FormGroup>
                            <Label className="pl-0 mb-3">Key Industry Experience </Label>
                            <div className="d-flex flex-wrap">
                            {allIndusExperience.map((experience, index) => (
                                <div className="custom-control custom-checkbox mb-3 col-xs-12 col-md-6">
                                <Input type="checkbox" className="custom-control-input" id={experience.id} onChange={handleExpSelection} value={experience.value}/>
                                <Label className="custom-control-label" for={experience.id}>{experience.value}  </Label>
                            </div>
                            ))}
                            </div>
                        </FormGroup>
                        </Col>
                        
                        
                    </Row>
                    <div className="clearfix">
                        <FormGroup>
                            <Label>Company Bio</Label>
                            <div className="form-input radius70 pl-4">
                                <Input type="textarea" className="form-control" rows="3" placeholder="Write Message Here" name='companyBio' onChange={handleInputValue} value={userDetails.companyBio} />
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label>Blue-Chip Customer Experience</Label>
                            <div className="form-input radius70 pl-4">
                                <Input type="textarea" className="form-control" rows="3" placeholder="Write Message Here" name='customerExp' onChange={handleInputValue} value={userDetails.customerExp}/>
                            </div>
                            <span className="text-green d-block pl-3 pt-1">*Please be aware your connections via CNCT may wish for evidence of this work </span>
                        </FormGroup>
                       
                    </div>
                </Container>
            </section>
            <section className="common-padd bgright">
                <Container>
                    <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                        <h2 className=" text-green">Purpose</h2>
                        <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
                    </div>
                    <Row>
                        <Col md="6">
                            <FormGroup>
                                <label className="mb-2 pl-0"> Why are you joining CNCT?</label>
                                <label className="mb-3 pl-0">Are you: </label>
                                <div className="btn-flex d-flex">
                                    <div className="pr-2">
                                        <Input type="radio" id="btr1" name="joining" className="custom-control-input" onChange={handleWhyJoining} value='Looking to take on subcontract work'/>
                                        <Label className="btn-radio" for="btr1">Looking to take on subcontract work </Label>
                                    </div>  
                                    <div className="pr-2">
                                        <Input type="radio" id="btr2" name="joining" className="custom-control-input" onChange={handleWhyJoining} value='Looking to outsource work'/>
                                        <Label className="btn-radio" for="btr2" >Looking to outsource work</Label>
                                    </div>
                                    <div className="pr-2">
                                        <Input type="radio" id="btr3" name="joining" className="custom-control-input" onChange={handleWhyJoining} value='Both'/>
                                        <Label className="btn-radio" for="btr3" >Both </Label>
                                    </div>
                                </div>
                            </FormGroup>
                        </Col>
                        <Col md="6">
                            <FormGroup className="pr-md-4">
                                <Label className="mb-3 pl-0"> Where did you hear about us? </Label>

                                <div className="btn-flex d-flex"> 
                                    {allWhereHear.map((hear, index) => (
                                        <div className="pr-2">
                                            {hearFromOther?
                                            <Input type="radio" id={hear.id} checked={false} disabled={true} name="Referral" className="custom-control-input" value={hear.value}/>
                                            :
                                            <Input type="radio" id={hear.id} disabled={false} name="Referral" className="custom-control-input" onChange={handleWhereHear} value={hear.value}/>
                                            }
                                        
                                        <Label className="btn-radio" for={hear.id} >{hear.value}</Label>
                                        </div>
                                    ))}
                                    </div>

                                {/* <div className="btn-flex d-flex">
                                    <div className="pr-2">
                                        <Input type="radio" id="btr4" name="Referral" className="custom-control-input" />
                                        <Label className="btn-radio" for="btr4" >email</Label>
                                    </div>
                                    <div className="pr-2">
                                        <Input type="radio" id="btr5" name="Referral" className="custom-control-input" />
                                        <Label className="btn-radio" for="btr5" >Twitter</Label>
                                    </div>
                                    <div className="pr-2">
                                        <Input type="radio" id="btr6" name="Referral" className="custom-control-input" />
                                        <Label className="btn-radio" for="btr6" >LinkedIn</Label>
                                    </div>
                                    <div className="pr-2">
                                        <Input type="radio" id="btr7" name="Referral" className="custom-control-input" />
                                        <Label className="btn-radio" for="btr7" > User Referral</Label>
                                    </div>
                                    <div className="pr-2">
                                        <Input type="radio" id="btr8" name="Referral" className="custom-control-input" />
                                        <Label className="btn-radio" for="btr8" > Other Industry Referral</Label>
                                    </div>
                                </div> */}
                                <FormGroup className="pr-md-5">                                   
                                    <div className="my-4">
                                        <div className="custom-control custom-checkbox">
                                            <Input type="checkbox" className="custom-control-input" id="ck112" name="hearFromOther" value={hearFromOther} onChange={handleHearFromOtherChange} defaultChecked={hearFromOther}/>
                                            <Label className="custom-control-label" for="ck112">Other</Label>
                                          </div>
                                    </div>
                                    {hearFromOther === true?
                                    <div className="form-input">
                                    <Input type="text" className="form-control " name="otherRegistrationNumber" placeholder="Other" onChange={handleInputValue} value={userDetails.otherRegistrationNumber} />
                                    <span className="icon-wr"><FontAwesomeIcon icon={faBars} /></span>
                                </div> : null
                                    }
                                    
                                </FormGroup>
                            </FormGroup>
                        </Col>
                    </Row>
                    <div className="py-4 container d-flex justify-content-end">
                        <Button className="btn-dark" type="submit" onClick={saveUserBasicInfo}>Continue <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating"/></Button>
                    </div>
                </Container>
            </section>
            
      </div>
    </React.Fragment>
  );
}

export default TabContentOne;