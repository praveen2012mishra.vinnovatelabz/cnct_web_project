import React, { useState } from 'react';
import { Container, Row, Col, Input, FormGroup, Label, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faLock, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import {ImagePath} from './../ImagePath';
import Dashboard from '../Dashboard/Dashboard';
import { NavLink } from 'react-router-dom';

const Succesful = (props) =>{
  const [isButtonTapped, setButtonState] = useState(false);

const getStartedPressed = (e) => {
  window.location.assign('/');
  
}

  return(
    <React.Fragment>
      <section className="common-padd contact-us position-relative">
        <Container>
          <div className="text-center">
            <img src={ImagePath.bigLoader} alt="" />
            <h2 className="my-3">Welcome to CNCT</h2>
            <p className="text">Thanks for signing up, it's great to have you on board!<br /> for sign-up confirmation, check your email</p>
          </div>
          <div className="py-4 d-flex justify-content-center">
            {/* <NavLink to="/Dashboard">Get Started</NavLink> */}
            <Button className="btn-dark" type="submit" onClick={() => props.callTab() }>Get Started <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating"/></Button>
          </div>
        </Container>
      </section>

      

    </React.Fragment>
  )
}

export default Succesful;