import React from 'react';
import { Container, Row, Col, Input, FormGroup, Label, NavLink } from 'reactstrap';
import { ImagePath } from './../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faCheck, faSortNumericDownAlt, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { Button } from 'react-bootstrap';
import BASE_API_URL from '../Config/config';
import {getApi, apiCallWithToken, postApiCallWithToken} from '../Config/api';
import axios from 'axios';
import PeopleCapabilities from './PeopleActivities'
import SignUpTab from './SignupTab';
import { Multiselect } from "multiselect-react-dropdown";
import { TagInput } from "reactjs-tag-input";

class TabContentTwo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      OtherItems: [],
      singleItems: [],
      activeTab: '2',
      howInterested: '',
      radious: '',
      skill_list: [],
      specialisation_list: [],
      designSoftware_list: [],
      inspectionSoftware_list: [],
      programingSoftware_list: [],
      simulationSoftware_list: [],
      selectedSkillId: '',
      selectedMachineCapabilitiesName: 'Subcontract Inspection',

      addMorePeople: true,
      selectedSpecialism: [],
      selectedDesign: [],
      selectedInspection: [],
      selectedPrograming: [],
      selectedSimulation: [],

      selectedPeopleCapabilities: {
        capability_type:"People",
        skill:"",
        specialism_skill:"",
        relocation:"",
        radius:"",
        software:""
      },
      selectedMachineCapabilities: {
        capability_type:"Machine",
        machine_capability_type:"",
        classification:"",
        machining_envelopes:"",
        gauge:"",
        max_drill_depth:""
      },

      machine_capabilities: [],
      hardware_classifications: [],
      machine_specialism: [],
      selected_hardware: {
        id: 0,
        classification_name: "",
        color: "",
        title: ""
      },

      manufacturelist: [],
      controllerList: [],
      materialSuitabilityList: [],

      green: {
        position: [
          { id: 1, title: "x", value: "" },
          { id: 2, title: "y", value: "" },
          { id: 3, title: "z", value: "" }
        ],
        inputField: []
      },
      black: {
        position: [
          { id: 4, title: "x", value: "" },
          { id: 5, title: "y", value: "" },
        ],
        inputField: [
          { id: 6, title: "Gauge", value: "" }
        ]
      },
      gray: {
        position: [
          { id: 7, title: "x", value: "" },
          { id: 8, title: "y", value: "" },
          { id: 9, title: "z", value: "" },
        ],
        inputField: [
          { id: 10, title: "Max Drill Depth", value: "" , key: "max_component_length"}
        ]
      },
      yellow: {
        position: [],
        inputField: [
          { id: 11, title: "Max Component Length", value: "" , key: "max_component_length"},
          { id: 12, title: "Max Component Diameter", value: "", key: "max_component_diameter" }
        ]
      },
      red: {
        position: [
          { id: 13, title: "X", value: "" },
          { id: 14, title: "Y", value: "" },
          { id: 15, title: "Z", value: "" }
        ],
        inputField: [
          { id: 16, title: "Max Component Length", value: "" },
          { id: 17, title: "Max Component Diameter", value: "" }
        ]
      },
      selectedValues: [
        { key: "Option 1" }
      ]
    };
  }

  // static getDerivedStateFromProps(props, state) {
  //   // Any time the current user changes,
  //   // Reset any parts of state that are tied to that user.
  //   // In this simple example, that's just the email.
  //   return {
  //       skill_list: props.value,
  //       plainArray: props.otherArray
  //     };
  // }

  onTagsChanged = (tags) => {};

  onSingleTagsChanged = (tags) => {
    let element = window.document.querySelectorAll(
      ".other-container-manufacture .cNrjqJ >span"
    );
    element.forEach((item, i) => {
      if (element.length - 1 === i) {
        //console.log(item,i);
      } else {
        item.style.display = "none";
      }
    });
    //console.log(tags[tags.length-1], element,"------------------>",tags);
    // if(tags[tags.length-1]){
    //   this.setState({singleItems:Object.assign([],tags[tags.length-1])})
    //   return Object.assign([],tags[tags.length-1]);
    // }
  };

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab : tab,
      })
      console.log('sdasadsdsd:   ', this.props);
    }
    // <NavLink className={classnames({ active: this.state.activeTab === tab })}>
        
    // </NavLink>
}

  componentDidMount() {
    this.get_skillListItems("people");
    this.get_skillListItems("machine");
  }
  addItem() {
    this.selectedValues.push({ key: "Option 1" });
  }

  formatInput = (e) => {
    // Prevent characters that are not numbers ("e", ".", "+" & "-") ✨
    let checkIfNum;
    if (e.key !== undefined) {
      // Check if it's a "e", ".", "+" or "-"
      checkIfNum = e.key === "e" || e.key === "." || e.key === "+" || e.key === "-" ;
    }
    else if (e.keyCode !== undefined) {
      // Check if it's a "e" (69), "." (190), "+" (187) or "-" (189)
      checkIfNum = e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 187 || e.keyCode === 189;
    }
    return checkIfNum && e.preventDefault();
  }

  handleHowInterestedSelection = (e) => {
    let selectinterest = this.state.selectedPeopleCapabilities;
    selectinterest.relocation = e.target.value
    this.setState({
      addMorePeople: false,
      howInterested: e.target.value,
      selectedPeopleCapabilities:selectinterest
    })
  }
  getRadious = e => {
    this.setState({
      radious: e.target.value,
    })
  }
  handleClassification = event => {
    console.log("total event:  ",event.target.value);
    let index = event.target.value;
    this.setState({
      selected_hardware:this.state.hardware_classifications[index]
    });
  }

  getSelectedSkillItem = (e) => {
    this.clearSelectedSofrwareAndSpecialism();
    let selectedSkill = this.state.selectedPeopleCapabilities;
    selectedSkill.skill = e.target.value
    this.setState({
      addMorePeople: false,
      selectedSkillId: e.target.id,
      selectedPeopleCapabilities:selectedSkill
    })
    this.get_specilizationAndSoftwareList(e.target.id);
    
  }
  clearSoftwareData() {
    this.setState({
      designSoftware_list: [],
      simulationSoftware_list: [],
      programingSoftware_list: [],
      inspectionSoftware_list: []
    })
  }
  clearSelectedSofrwareAndSpecialism() {
    this.setState({
      selectedSpecialism: [],
      selectedDesign: [],
      selectedInspection: [],
      selectedPrograming: [],
      selectedSimulation: [],
    })
  }

  clearHardwareClassificationData() {
    let hardware = this.state.selected_hardware;
    hardware.id = '';
    hardware.color = '';
    hardware.classification_name = "";
    hardware.title = "";
    this.setState({
      selected_hardware: hardware,

    })
  }

  clear_selectedHardwareSpecifications() {
    this.setState({
      hardware_classifications: []
    })
  }

  onSelectSpecialism = (selectedList, selectedItem) => {
    let selectedSpecialism = this.state.selectedSpecialism;
    selectedSpecialism.push(selectedItem.id)
    this.setState({
      selectedSpecialism:selectedSpecialism
    })
}
  onRemoveSpecialism = (selectedList, removedItem) => {
    let selectedSpecialism = this.state.selectedSpecialism;
    for(let i = 0 ; i < selectedSpecialism.length; i++){
      if(selectedSpecialism[i] == removedItem.id){
        selectedSpecialism.splice(i, 1);
      }
    }
    this.setState({
      selectedSpecialism:selectedSpecialism
    })
  }
  
  onSelectDesign = (selectedList, selectedItem) => {
    let selectedDesign = this.state.selectedDesign;
    selectedDesign.push(selectedItem.id)
    this.setState({
      selectedDesign:selectedDesign
    })
}
onRemoveDesign = (selectedList, removedItem) => {
  let selectedDesign = this.state.selectedDesign;
  for(let i = 0 ; i < selectedDesign.length; i++){
    if(selectedDesign[i] == removedItem.id){
      selectedDesign.splice(i, 1);
    }
  }
  this.setState({
    selectedDesign:selectedDesign
  })
}
onSelectInspection = (selectedList, selectedItem) => {
  let selectedInspection = this.state.selectedInspection;
  selectedInspection.push(selectedItem.id)
  this.setState({
    selectedInspection:selectedInspection
  })
}
onRemoveInspection = (selectedList, removedItem) => {
  let selectedInspection = this.state.selectedInspection;
  for(let i = 0 ; i < selectedInspection.length; i++){
    if(selectedInspection[i] == removedItem.id){
      selectedInspection.splice(i, 1);
    }
  }
  this.setState({
    selectedInspection:selectedInspection
  })
}
onSelectPrograming = (selectedList, selectedItem) => {
  let selectedPrograming = this.state.selectedPrograming;
  selectedPrograming.push(selectedItem.id)
  this.setState({
    selectedPrograming:selectedPrograming
  })
}
onRemovePrograming = (selectedList, removedItem) => {
  let selectedPrograming = this.state.selectedPrograming;
  for(let i = 0 ; i < selectedPrograming.length; i++){
    if(selectedPrograming[i] == removedItem.id){
      selectedPrograming.splice(i, 1);
    }
  }
  this.setState({
    selectedPrograming:selectedPrograming
  })
}
onSelectSimulation = (selectedList, selectedItem) => {
  let selectedSimulation = this.state.selectedSimulation;
  selectedSimulation.push(selectedItem.id)
  this.setState({
    selectedSimulation:selectedSimulation
  })
}
onRemoveSimulation = (selectedList, removedItem) => {
  let selectedSimulation = this.state.selectedSimulation;
  for(let i = 0 ; i < selectedSimulation.length; i++){
    if(selectedSimulation[i] == removedItem.id){
      selectedSimulation.splice(i, 1);
    }
  }
  this.setState({
    selectedSimulation:selectedSimulation
  })
}

setXYZvalue = e => {
  let tempCapabilities = {...this.state.selectedMachineCapabilities};
  tempCapabilities[e.target.name] = e.target.value;
  this.setState({
    selectedMachineCapabilities : tempCapabilities
  })
}
setGuageValue = e => {
  let gaugeDetails = this.state.selectedMachineCapabilities;
  gaugeDetails.gauge = e.target.value
    this.setState({
      selectedMachineCapabilities:gaugeDetails
    })
}
setDrillDepth = e => {
  let drillDepth = this.state.selectedMachineCapabilities;
  drillDepth.drillDepth = e.target.value
    this.setState({
      selectedMachineCapabilities:drillDepth
    })
}
setMaxComponentValue = e => {
  let maxComponent = {...this.state.selectedMachineCapabilities};
  maxComponent[e.target.name] = e.target.value;
  this.setState({
    selectedMachineCapabilities : maxComponent
  })
}

  getSelectedMachineCapabilities = (e) => {
    let selectedMachineCapabilities = this.state.selectedMachineCapabilities;
    selectedMachineCapabilities.machine_capability_type = e.target.value
    this.setState({
      selectedMachineCapabilitiesName: e.target.value,
      selectedMachineCapabilities:selectedMachineCapabilities
    })
    
    this.get_manufacturelist(e.target.id);
    if(e.target.id === '7'){
      this.get_specilizationAndSoftwareList(e.target.id);
    }else {
      this.get_hardwareClassification(e.target.id);
    }
    if (e.target.value === 'Subcontract CNC Machining') {
      this.get_controllerList();
      this.get_materialSuitability();
    }

    if (e.target.value === 'Subcontract Inspection' || e.target.value === 'Other Machine Capabilities') {
      this.clearHardwareClassificationData();
      if ('controller_list' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['controller_list'];
      }
      if ('material_capability' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['material_capability'];
      }
      if ('gauge' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['gauge'];
      }
      if ('max_drill_depth' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['max_drill_depth'];
      }
      if ('max_component_diameter' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['max_component_diameter'];
      }
      if ('max_component_length' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['max_component_length'];
      }
      if ('x' in this.state.selectedMachineCapabilities || 'y' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['x'];
        delete this.state.selectedMachineCapabilities['y'];
      }
      if ('z' in this.state.selectedMachineCapabilities) {
        delete this.state.selectedMachineCapabilities['z'];
      }
    }
  }

  setButtonInteraction = (status) => {
    console.log("status: ", status.target.state)
  }

  // all APIs call here

  get_skillListItems(type) {
    apiCallWithToken(BASE_API_URL + 'web/skilllist/', 
    'post',
    { "skill_type":type })
      .then(response => {
        if(type === 'people') {
          this.setState({
            skill_list: response.data.allskill
          })
        }else {
          this.setState({
            machine_capabilities: response.data.allskill
          })
        }
        
      })
      .catch(err => {
        console.log(err);
      });
  }
  get_manufacturelist(id) {
    apiCallWithToken(BASE_API_URL + 'web/manufacturelist/', 
    'post',
    { "parent_id": id})
      .then(response => {
        this.setState({
          manufacturelist: response.data.manufacturers
        })
      })
      .catch(err => {
        console.log(err);
      });
  }
  get_controllerList() {
    getApi(BASE_API_URL + 'web/controllerlist/')
      .then(response => {
        this.setState({
          controllerList: response.data
        })
      })
      .catch(err => {
        console.log(err);
      });
  }
  get_materialSuitability() {
    getApi(BASE_API_URL + 'web/materiallist/')
      .then(response => {
        this.setState({
          materialSuitabilityList: response.data
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  get_specilizationAndSoftwareList(parent_id) {
    if(parent_id === '7') {

    }else {
      this.clearSoftwareData();
    }
    
    apiCallWithToken(BASE_API_URL + 'web/peoplefetch/', 
    'post',
    { "parent_id":parent_id })
      .then(response => {
        var allData = response.data.softwares;
        if(allData.length > 0) {
          for (var i = 0; i < allData.length; i++) {
            let details = allData[i];
            if (details.software_type === 'Design Software') {
              this.setState({designSoftware_list: [...this.state.designSoftware_list, details]})
            }else if (details.software_type === 'Simulation Software') {
              this.setState({simulationSoftware_list: [...this.state.simulationSoftware_list, details]})
            }else if (details.software_type === '"Programming Software') {
              this.setState({programingSoftware_list: [...this.state.programingSoftware_list, details]})
            }else {
              this.setState({inspectionSoftware_list: [...this.state.inspectionSoftware_list, details]})
            }
          }
        }
        if(parent_id === '7') {
          this.setState({
            machine_specialism: response.data.specialism
          })
        }else {
          this.setState({
            specialisation_list: response.data.specialism
          })
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  get_hardwareClassification(id) {
    this.clear_selectedHardwareSpecifications();
    console.log('hardware_classifications ID:  ', id);
    apiCallWithToken(BASE_API_URL + 'web/classificationlist/', 
    'post',
    { "parent_id":id })
      .then(response => {
        console.log('hardware_classifications:  ', response.data.classifications.length);
        this.setState({
          hardware_classifications: response.data.classifications
        })
      })
      .catch(err => {
        console.log(err);
      });
  }
  clearAllPeopleCapabilities = () => {
    console.log("button status");
    this.clearSelectedSofrwareAndSpecialism();
    this.clearSoftwareData();
    this.setState({
      howInterested: '',
      radious: '',
      selectedSkillId: '',
      specialisation_list: [],
    })
  }
  savePeopleCapabilities = () => {    
    if (localStorage.hasOwnProperty("userId")) {
      if (this.state.howInterested === '') {
        alert("Please select your interest of work");
      }else if(this.state.selectedSkillId === '') {
        alert("Please select your skills");
      }else {
          let allPeopleCapabilities = this.state.selectedPeopleCapabilities;
          let softwares = [...this.state.selectedDesign, ...this.state.selectedInspection, ...this.state.selectedPrograming, ...this.state.selectedSimulation];
          let allSoftware = softwares.join(',');//JSON.stringify(softwares);
          let specialism = this.state.selectedSpecialism.join(',');//JSON.stringify(this.state.selectedSpecialism);
          allPeopleCapabilities.specialism_skill = specialism;
          allPeopleCapabilities.software = allSoftware;
          if (this.state.howInterested === "On-Site" || this.state.howInterested === "Flexible") {
            
            allPeopleCapabilities.radius = this.state.radious;
          }else {
            allPeopleCapabilities.radius = '';
          }
          this.setState({
            selectedPeopleCapabilities:allPeopleCapabilities,
          })
          let machineDetails = this.state.selectedMachineCapabilities;
          machineDetails.classification = this.state.selected_hardware.id
        this.setState({
  
          selectedMachineCapabilities:machineDetails
        })
          let details = {
            '0': this.state.selectedPeopleCapabilities,
            '1': this.state.selectedMachineCapabilities
          };
          console.log("details: ",details);
          //http://111.93.169.90:2030/web/registercapabilities/
          postApiCallWithToken(BASE_API_URL + 'web/registercapabilities/', 
          'post',
          {'user_id': localStorage.getItem('userId'),
          'full_data' : details
          })
          .then(response => {
            console.log(response);
            alert("User capabilities are successfully stored.");
            window.location.assign('/');
          })
          .catch(err => {
          console.log(err);
          })
        }
    }else{
      alert("Please save basic info to continue.");
    }
    
    
  }

  saveData = () => {
    localStorage.setItem('pageType', '');
    localStorage.setItem('payment', '');
    this.props.setToggle('1');
    
  }

  render() {
    return (
      <React.Fragment>
        <div className="capability-tab common-padd bgright">
          <Container>
            <div className="in-basic">
              <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                <h2 className=" text-green">People <span class="font-weight-exlight">Capabilities</span>  </h2>
                <span><img src={ImagePath.loaderCircle} class="img-fluid rotating" alt="loader"></img></span>
              </div>
            </div>
            {/* <PeopleCapabilities value={this.state.skill_list} otherArray={this.state.plainArray}/> */}
            {/* {this.state.plainArray.map((peopleCapabilities, index) => <PeopleCapabilities key={index} value={this.state.skill_list} uniqueId={index}/>)} */}
            <Row>
              <Col md="6">
                <FormGroup>
                  <Label className="pl-0">How are you interested to work?</Label>
                  <div className="btn-flex d-flex">
                    <div className="pr-2">
                      <Input type="radio" id="cr1" name="rem" className="custom-control-input" onChange={(e) => this.handleHowInterestedSelection(e)} value='Remote' />
                      <Label className="btn-radio" for="cr1">Remote </Label>
                    </div>
                    <div className="pr-2">
                      <Input type="radio" id="cr2" name="rem" className="custom-control-input" onChange={(e) => this.handleHowInterestedSelection(e)} value='On-Site' />
                      <Label className="btn-radio" for="cr2">On-Site</Label>
                    </div>
                    <div className="pr-2">
                      <Input type="radio" id="cr3" name="rem" className="custom-control-input" onChange={(e) => this.handleHowInterestedSelection(e)} value='Flexible' />
                      <Label className="btn-radio" for="cr3">Flexible</Label>
                    </div>
                  </div>
                </FormGroup>
              </Col>
              {this.state.howInterested === "On-Site" || this.state.howInterested === "Flexible" ?
                <Col md="6">
                  <FormGroup>
                    <Label>Willing-to-Travel Radius</Label>
                    <div className="form-input" style={{ width: "200px" }}>
                      <Input type="number" className="form-control" placeholder="Radius" onChange={(e) => this.getRadious(e)} onKeyDown={this.formatInput}/>
                      <span className="mm">mm</span>
                    </div>
                  </FormGroup>
                </Col> : null
              }
              <Col md="12">
                <FormGroup>
                  <label className="pl-0">Skill</label>
                  <div className="btn-flex d-flex">
                  {this.state.skill_list.map((skills, index) => (
                        <div className="pr-2">
                        <Input type="radio" id={skills.id} className="custom-control-input" name="skills" onChange={(e) => this.getSelectedSkillItem(e)} value={skills.skill_name}/>
                        <Label className="btn-radio" for={skills.id}>{skills.skill_name} </Label>
                      </div>
                      ))}

                  </div>
                </FormGroup>
              </Col>
              {this.state.specialisation_list.length > 0 ?
              <Col md="12">
                <FormGroup>
                  <Label className="pl-0">Specialism </Label>
                  <Multiselect options={this.state.specialisation_list} displayValue='specialism_name' key="Specialism" onSelect={this.onSelectSpecialism} onRemove={this.onRemoveSpecialism}/>

                </FormGroup>
              </Col>
              : null
              }
              {this.state.designSoftware_list.length > 0 ?
                <Col md="12">
                  <FormGroup>
                    <Label className="pl-0">Design Software </Label>
                    <Multiselect options={this.state.designSoftware_list} displayValue='software_name' onSelect={this.onSelectDesign} onRemove={this.onRemoveDesign}/>
                  </FormGroup>
                  <div className="other-container">
                                  <Label className="pl-0">Other </Label>
                                  <TagInput
                                    tags={this.state.OtherItems}
                                    onTagsChanged={this.onTagsChanged}
                                    addTagOnEnterKeyPress={false}
                                    placeholder="Add new item"
                                    tagStyle={`background: #fbfbfb;`}
                                    hideInputPlaceholderTextIfTagsPresent={
                                      false
                                    }
                                  />
                                </div>
                </Col>: null
              }
              {this.state.programingSoftware_list.length > 0 ?
                <Col md="12">
                  <FormGroup>
                    <Label className="pl-0">Programming Software </Label>
                    <Multiselect options={this.state.programingSoftware_list} displayValue='software_name' onSelect={this.onSelectPrograming} onRemove={this.onRemovePrograming}/>
                  </FormGroup>
                </Col>: null
              }
              {this.state.simulationSoftware_list.length > 0 ?
                <Col md="12">
                  <FormGroup>
                    <Label className="pl-0">Simulation Software </Label>
                    <Multiselect options={this.state.simulationSoftware_list} displayValue='software_name' onSelect={this.onSelectSimulation} onRemove={this.onRemoveSimulation}/>
                  </FormGroup>
                </Col>: null
              }
              {this.state.inspectionSoftware_list.length > 0 ?
                <Col md="12">
                  <FormGroup>
                    <Label className="pl-0">Inspection Software </Label>
                    <Multiselect options={this.state.inspectionSoftware_list} displayValue='software_name' onSelect={this.onSelectInspection} onRemove={this.onRemoveInspection}/>
                  </FormGroup>
                </Col>: null
              }
            </Row>
          </Container>


        </div>

        {/* <div className="bg-white d-flex justify-content-center py-4">
          <Container>
            <div className="d-md-flex justify-content-center align-items-center">
              <Label className="h6 mb-0">Add more people capability requirements?</Label>
              <div className="d-flex ml-3">
                <Button variant="btn btn-green mr-3">Yes</Button>
                <Button variant="btn btn-black">No</Button>
              </div>
            </div>
          </Container>
        </div> */}
        <div className="capability-tab common-padd bgleft">
          <Container>
            <div className="in-basic">
              <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                <h2 className=" text-green">Machine  <span className="font-weight-exlight">Capabilities</span>  </h2>
                <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
              </div>
              
              <Row>
                <Col md="12">
                  <FormGroup>
                    <label className="pl-0">Select</label>
                    <div className="btn-flex d-flex">
                      {this.state.machine_capabilities.map((machine, index) => (
                        <div className="pr-2">
                          <input type="radio" id={machine.id} name="machine" className="custom-control-input" onChange={(e) => this.getSelectedMachineCapabilities(e)} value={machine.skill_name}/>
                          <label className="btn-radio" for={machine.id}>{machine.skill_name} </label>
                        </div>
                        ))}
                    </div>
                  </FormGroup>
                </Col>
                {this.state.selectedMachineCapabilitiesName === 'Subcontract Inspection' || this.state.selectedMachineCapabilitiesName === 'Subcontract CNC Machining' ?
                  <Col md="12">
                    <FormGroup>
                      <Label>Hardware ClassNameification</Label>
                      <div className="form-input">
                      <select
                        className="form-control"
                        name="hardware"
                        onChange={this.handleClassification}>
                          {this.state.hardware_classifications.map((hardwares, index) => (
                          <>
                           <option value="none" selected disabled hidden>
                           Select
                         </option>
                          <option key={index} value={index} >{hardwares.classification_name} </option>
                          </>
                        ))}
                      </select>
                      </div>
                    </FormGroup>
                  </Col> : 
                  <Col md="12">
                    <FormGroup>
                      <Label className="pl-0">Specialism </Label>
                      <Multiselect options={this.state.machine_specialism} displayValue='specialism_name' />
                    </FormGroup>
                  </Col>
                }
                {/* {this.state.selectedColorContent === "blackContent" ?
                (this.state.blackContent.position.length > 0?
                    <Col md="12">
                      <FormGroup>
                        <Label>Milling Envelope</Label>
                      </FormGroup>
                      <Row>
                      {this.state.blackContent.position.map((green, index) => (
                        <Col lg="2" className="col-auto">
                        <FormGroup>
                          <div className="form-input">
                            <Input type="text" className="form-control" placeholder={green.title} />
                            <span className="mm">mm</span>
                          </div>
                        </FormGroup>
                      </Col>
                      ))}
                      </Row>
                    </Col>
                    
                    : null)
                    (this.state.blackContent.inputField.length > 0?
                    <Col xs="12" sm="12" md="12" lg="12">
                    <FormGroup>
                      <Label>Turning Envelope </Label>
                    </FormGroup>
                    <Row>
                        {this.state.blackContent.inputField.map((myInput, index) => (
                          <Col lg="2" className="col-auto">
                          <FormGroup>
                          <Label>{myInput.title} </Label>
                          <div className="form-input">
                              <Input type="text" className="form-control" placeholder={myInput.title} />
                              <span className="mm">mm</span>
                            </div>
                          
                        </FormGroup>
                        </Col>
                        ))}
                        </Row>
                    </Col>
                    : null)
                    : null
                } */}
                
                {this.state.selected_hardware.color === "green" ?
                    <Col md="12">
                      <FormGroup>
                        <Label>Milling Envelope</Label>
                      </FormGroup>
                      <Row>
                      {this.state.green.position.map((allPositions, index) => (
                        <Col lg="2" className="col-auto">
                        <FormGroup>
                          <div className="form-input">
                            <Input type="number" name={allPositions.title} className="form-control" placeholder={allPositions.title} onChange={this.setXYZvalue} onKeyDown={this.formatInput}/>
                            <span className="mm">mm</span>
                          </div>
                        </FormGroup>
                      </Col>
                      ))}
                      </Row>
                    </Col>
                    : null  
                }
                {this.state.selected_hardware.color === "golden" ?
                    <Col md="12">
                      <FormGroup>
                        <Label>Max Component Envelope</Label>
                      </FormGroup>
                      <Row>
                      {this.state.green.position.map((allPositions, index) => (
                        <Col lg="2" className="col-auto">
                        <FormGroup>
                          <div className="form-input">
                            <Input type="number" className="form-control" name={allPositions.title} placeholder={allPositions.title} onChange={(e) => this.setXYZvalue(e)} onKeyDown={this.formatInput}/>
                            <span className="mm">mm</span>
                          </div>
                        </FormGroup>
                      </Col>
                      ))}
                      </Row>
                    </Col>
                    : null  
                }
                {this.state.selected_hardware.color === "purple" ?
                    <Col md="12">
                      <FormGroup>
                        <Label>Max Printing Envelope</Label>
                      </FormGroup>
                      <Row>
                      {this.state.green.position.map((allPositions, index) => (
                        <Col lg="2" className="col-auto">
                        <FormGroup>
                          <div className="form-input">
                            <Input type="number" className="form-control" onKeyDown={this.formatInput} name={allPositions.title} placeholder={allPositions.title} onChange={(e) => this.setXYZvalue(e)}/>
                            <span className="mm">mm</span>
                          </div>
                        </FormGroup>
                      </Col>
                      ))}
                      </Row>
                    </Col>
                    : null  
                }
                {this.state.selected_hardware.color === "black" ?
                  <Col md="12">
                    <FormGroup>
                      <Label>Dimensional Capabilities</Label>
                    </FormGroup>
                    <Row>
                    {this.state.black.position.map((allPositions, index) => (
                      <Col lg="2" className="col-auto">
                      <FormGroup>
                        <div className="form-input">
                          <Input type="number" className="form-control" onKeyDown={this.formatInput} name={allPositions.title} placeholder={allPositions.title} onChange={(e) => this.setXYZvalue(e)}/>
                          <span className="mm">mm</span>
                        </div>
                      </FormGroup>
                    </Col>
                    ))}
                    </Row>
                  </Col>
                  : null  
                }
                {this.state.selected_hardware.color === "black" ?
                  <Col xs="12" sm="12" md="12" lg="12">
                  <FormGroup>
                    <Label>Gauge </Label>
                  </FormGroup>
                  <Row>
                      {this.state.black.inputField.map((myInput, index) => (
                        <Col md="6">
                        <FormGroup>
                        <div className="form-input">
                            <Input type="number" className="form-control" onKeyDown={this.formatInput} placeholder={myInput.title} name="manufacturer" onChange={this.setGuageValue}/>
                            <span className="mm">mm</span>
                          </div>
                        
                      </FormGroup>
                      </Col>
                      ))}
                      </Row>
                  </Col>
                  : null
                }

                {this.state.selected_hardware.color === "gray" ?
                  <Col md="12">
                    <FormGroup>
                      <Label>Component Envelope</Label>
                    </FormGroup>
                    <Row>
                    {this.state.gray.position.map((allPositions, index) => (
                      <Col lg="2" className="col-auto">
                      <FormGroup>
                        <div className="form-input">
                          <Input type="number" className="form-control" onKeyDown={this.formatInput} name={allPositions.title} placeholder={allPositions.title} onChange={(e) => this.setXYZvalue(e)}/>
                          <span className="mm">mm</span>
                        </div>
                      </FormGroup>
                    </Col>
                    ))}
                    </Row>
                  </Col>
                  : null  
                }
                {this.state.selected_hardware.color === "gray" ?
                  <Col xs="12" sm="12" md="12" lg="12">
                  <FormGroup>
                    <Label>Drill Depth </Label>
                  </FormGroup>
                  <Row>
                      {this.state.gray.inputField.map((myInput, index) => (
                        <Col md="6">
                        <FormGroup>
                        
                        <div className="form-input">
                            <Input type="number" className="form-control" onKeyDown={this.formatInput} placeholder={myInput.title} onChange={this.setDrillDepth}/>
                            <span className="mm">mm</span>
                          </div>
                        
                      </FormGroup>
                      </Col>
                      ))}
                      </Row>
                  </Col>
                  : null
                }
                {this.state.selected_hardware.color === "yellow" ?
                  <Col xs="12" sm="12" md="12" lg="12">
                  <FormGroup>
                    <Label>Turning Envelope </Label>
                  </FormGroup>
                  <Row>
                      {this.state.yellow.inputField.map((myInput, index) => (
                        <Col md="6">
                        <FormGroup>
                        <div className="form-input">
                            <Input type="number" className="form-control" onKeyDown={this.formatInput} placeholder={myInput.title} name={myInput.key} onChange={this.setMaxComponentValue}/>
                            <span className="mm">mm</span>
                          </div>
                        
                      </FormGroup>
                      </Col>
                      ))}
                      </Row>
                  </Col>
                  : null
                }

                {this.state.selected_hardware.color === "red" ?
                  <Col md="12">
                    <FormGroup>
                      <Label>Milling Envelope</Label>
                    </FormGroup>
                    <Row>
                    {this.state.red.position.map((allPositions, index) => (
                      <Col lg="2" className="col-auto">
                      <FormGroup>
                        <div className="form-input">
                          <Input type="number" className="form-control" onKeyDown={this.formatInput} name={allPositions.title} placeholder={allPositions.title} onChange={(e) => this.setXYZvalue(e)}/>
                          <span className="mm">mm</span>
                        </div>
                      </FormGroup>
                    </Col>
                    ))}
                    </Row>
                  </Col>
                  : null  
                }
                {this.state.selected_hardware.color === "red" ?
                  <Col xs="12" sm="12" md="12" lg="12">
                  <FormGroup>
                    <Label>Turning Envelope </Label>
                  </FormGroup>
                  <Row>
                      {this.state.red.inputField.map((myInput, index) => (
                        <Col md="6">
                        <FormGroup>
                        
                        <div className="form-input">
                            <Input type="number" className="form-control" onKeyDown={this.formatInput} placeholder={myInput.title} name={myInput.key} onChange={this.setMaxComponentValue}/>
                            <span className="mm">mm</span>
                          </div>
                        
                      </FormGroup>
                      </Col>
                      ))}
                      </Row>
                  </Col>
                  : null
                }
                {this.state.selected_hardware.color === "cyan" ?
                    <Col md="12">
                      <FormGroup>
                        <Label>Milling Envelope</Label>
                      </FormGroup>
                      <Row>
                      {this.state.green.position.map((allPositions, index) => (
                        <Col lg="2" className="col-auto">
                        <FormGroup>
                          <div className="form-input">
                            <Input type="number" className="form-control" onKeyDown={this.formatInput} name={allPositions.title} placeholder={allPositions.title} onChange={(e) => this.setXYZvalue(e)}/>
                            <span className="mm">mm</span>
                          </div>
                        </FormGroup>
                      </Col>
                      ))}
                      </Row>
                    </Col>
                    : null  
                }
                
                {/* <Col md="12">
                                    <FormGroup>
                                        <Label>Milling Envelope</Label>
                                    </FormGroup>
                                    <Row>
                                        <Col lg="2" className="col-auto">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="text" className="form-control" placeholder="X" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col lg="2" className="col-auto">
                                            <FormGroup>
                                                <div className="form-input" >
                                                    <Input type="text" className="form-control" placeholder="Y" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col lg="2" className="col-auto">
                                            <FormGroup>
                                                <div className="form-input">
                                                    <Input type="text" className="form-control" placeholder="Z" />
                                                    <span className="mm">mm</span>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col> */}
                {/* <Col xs="12" sm="12" md="12" lg="12">
                  <FormGroup>
                    <Label>Turning Envelope </Label>
                  </FormGroup>
                  <Row>
                    <Col md="6">
                      <FormGroup>
                        <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Max Component Length" />
                          <span className="mm">mm</span>
                        </div>
                      </FormGroup>
                    </Col>

                    <Col md="6">
                      <FormGroup>
                        <div className="form-input">
                          <Input type="text" className="form-control" placeholder="Max Component Diameter" />
                          <span className="mm">mm</span>
                        </div>
                      </FormGroup>
                    </Col>
                  </Row>
                </Col> */}
                {this.state.selectedMachineCapabilitiesName === 'Subcontract Inspection'?
                  <Col xs="12" sm="12" md="12" lg="12">
                    <FormGroup>
                      <Label>Manufacturer</Label>
                    </FormGroup>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <div className="form-input">
                            <select className="form-control" name="manufacturer" onChange={this.setXYZvalue}>
                              {this.state.manufacturelist.map((manufacture, index) => (
                                <>
                                <option value="none" selected disabled hidden>
                                Select
                              </option>
                              <option key={index} value={manufacture.id}>{manufacture.manufacturer_name}</option>
                                </>
                              ))}                                               
                            </select>
                          </div>
                        </FormGroup>
                        <div className="manufacture-box">
                                          <Label className="pl-0">Other </Label>
                                          <div className="manufacture-input-box">
                                            <input
                                              type="text"
                                              className="input-box"
                                              placeholder="Other"
                                              name='manufacture-cnc'
                                            />
                                          </div>
                                        </div> 
                      </Col>
                      {/* <Col md="8" lg="6">
                        <FormGroup>
                          <Row>
                            <Col sm="5">
                              <div className="form-input p-0">
                                <button type="button" className="btn btn-block btn-green-roound">Other <FontAwesomeIcon icon={faPlusCircle} className="mt-1" /></button>
                              </div>
                            </Col>
                            <Col sm="5">
                              <div className="form-input">
                                <Input type="text" placeholder="enter here.." />
                              </div>
                            </Col>
                            <Col sm="2" className="pl-0 pr-md-3">
                              <button className="btn btn-link" type="button"> <span className="icon-wr ml-0 bg-light"><FontAwesomeIcon icon={faCheck} /></span></button>
                            </Col>
                          </Row>
                        </FormGroup>
                      </Col> */}
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <Label>Model</Label>
                          <div className="form-input">
                            <Input type="text" className="form-control " placeholder="Modal" name="model_description" onChange={this.setXYZvalue}/>
                            <span className="icon-wr"><FontAwesomeIcon icon={faSortNumericDownAlt} /></span>
                          </div>
                        </FormGroup>
                      </Col>
                    </Row>
                  

                  </Col>: 
                  this.state.selectedMachineCapabilitiesName === 'Subcontract CNC Machining'?
                  <Col xs="12" sm="12" md="12" lg="12">
                    
                    <FormGroup>
                      <Label>Manufacturer</Label>
                    </FormGroup>
                    <Row>
                      <Col md="6">
                      <FormGroup>
                          <div className="form-input">
                            <select className="form-control" name="manufacturer" onChange={this.setXYZvalue}>
                              {this.state.manufacturelist.map((manufacture, index) => (
                                  <>
                                  <option value="none" selected disabled hidden>
                                  Select
                                </option>
                                <option key={index} value={manufacture.id}>{manufacture.manufacturer_name}</option>
                                  </>
                                ))}                               
                            </select>
                          </div>
                        </FormGroup>
                        <div className="manufacture-box">
                                          <Label className="pl-0">Other </Label>
                                          {/* <TagInput
                                            tags={this.state.singleItems}
                                            onTagsChanged={this.onSingleTagsChanged}
                                            addTagOnEnterKeyPress={false}
                                            placeholder="Add new item"
                                            tagStyle={`background: #fbfbfb;`}
                                            hideInputPlaceholderTextIfTagsPresent={
                                              false
                                            }
                                          /> */}
                                          <div className="manufacture-input-box">
                                            <input
                                              type="text"
                                              className="input-box"
                                              placeholder="Other"
                                              name='manufacture-inspection'
                                            />
                                          </div>
                                        </div>
                      </Col>
                      {/* <Col md="8" lg="6">
                        <FormGroup>
                          <Row>
                            <Col sm="5">
                              <div className="form-input p-0">
                                <button type="button" className="btn btn-block btn-green-roound">Other <FontAwesomeIcon icon={faPlusCircle} className="mt-1" /></button>
                              </div>
                            </Col>
                            <Col sm="5">
                              <div className="form-input">
                                <Input type="text" placeholder="enter here.." />
                              </div>
                            </Col>
                            <Col sm="2" className="pl-0 pr-md-3">
                              <button className="btn btn-link" type="button"> <span className="icon-wr ml-0 bg-light"><FontAwesomeIcon icon={faCheck} /></span></button>
                            </Col>
                          </Row>
                        </FormGroup>
                      </Col> */}
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <Label>Model</Label>
                          <div className="form-input">
                            <Input type="text" className="form-control " placeholder="Modal" name="model_description" onChange={this.setXYZvalue}/>
                            <span className="icon-wr"><FontAwesomeIcon icon={faSortNumericDownAlt} /></span>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col md="6">
                        <FormGroup>
                          <label>Material Suitability</label>
                          <div className="form-input">
                            <select className="form-control" name="Material" name="material_capability" onChange={this.setXYZvalue}>
                              {this.state.materialSuitabilityList.map((Material, index) => (
                                  <>
                                  <option value="none" selected disabled hidden>
                                  Select
                                </option>
                                <option key={index} value={Material.id}>{Material.material_capability_name}</option>
                                  </>
                                ))}                               
                            </select>
                          </div>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="12" md="12" lg="12">
                        <FormGroup>
                          <Label>Controller</Label>
                        </FormGroup>
                      </Col>
                      <Col md="6">
                        <FormGroup>
                          <div className="form-input">
                            <select className="form-control" name="controller_list" onChange={this.setXYZvalue}>
                            {this.state.controllerList.map((Controller, index) => (
                                <>
                                <option value="none" selected disabled hidden>
                                Select
                              </option>
                              <option key={index} value={Controller.id}>{Controller.controller_name}</option>
                                </>
                              ))}                               
                            </select>
                          </div>
                        </FormGroup>
                        <div className="manufacture-box">
                                          <Label className="pl-0">Other </Label>
                                          <div className="manufacture-input-box">
                                            <input
                                            name='controller-cnc'
                                              type="text"
                                              className="input-box"
                                              placeholder="Other"
                                            />
                                          </div>
                                        </div>
                      </Col>
                      {/* <Col md="6">
                        <FormGroup>
                          <div className="d-flex align-items-center">
                            <div className="form-input mr-1">
                              <Input type="text" className="form-control " placeholder="" />
                            </div>
                            <button className="btn btn-link" type="button"> <span className="icon-wr"><FontAwesomeIcon icon={faTimesCircle} className="text-black fa-2x" /></span></button>
                          </div>
                        </FormGroup>
                      </Col> */}
                    </Row>
                  </Col>: null
                }
              </Row>
            </div>
          </Container>
        </div>
        <div className="bg-white d-flex justify-content-center py-4">
          <Container>
            {/* <div className="d-md-flex justify-content-center align-items-center">
              <label className="h6 mb-0">Add more people capability requirements?</label>
              <div className="d-flex ml-3">
                <Button variant="btn btn-green mr-3">Yes</Button>
                <Button variant="btn btn-black">No</Button>
              </div>
            </div> */}
            <div className="py-4 d-flex justify-content-end">
              <Button variant="btn btn-dark" type="submit" onClick={this.savePeopleCapabilities}>Continue <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating" /></Button>
            </div>
          </Container>
        </div>







      </React.Fragment>
    );
  }
}
export default TabContentTwo;