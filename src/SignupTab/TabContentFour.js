import React, {useState, useEffect } from 'react';
import { Container, Row, Col, FormGroup, Input, Label } from 'reactstrap';
import {ImagePath} from './../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faCalendar, faCreditCard, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import DatePicker from "react-datepicker";
import {apiCallWithToken} from '../Config/api';
import BASE_API_URL from '../Config/config';
import Succesful from './Succesful';

const TabContentFour = (props) =>{

    //  const [creditCardExpireDate, setCreditCardExpireDate] = useState('');
     const [agreeStatus, checkAgree] = useState(false);
     const [isSubmit, setSubmitStatus] = useState(false);
     const [cardDetails, setCardDetails] = useState({
        user_id:"",
        cardholder_name:"",
        card_number:"",
        expiry_month:"",
        expiry_year:"",
        CVV:""
    });

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    const formatInput = (e) => {
        // Prevent characters that are not numbers ("e", ".", "+" & "-") ✨
        let checkIfNum;
        if (e.key !== undefined) {
          // Check if it's a "e", ".", "+" or "-"
          checkIfNum = e.key === "e" || e.key === "." || e.key === "+" || e.key === "-" ;
        }
        else if (e.keyCode !== undefined) {
          // Check if it's a "e" (69), "." (190), "+" (187) or "-" (189)
          checkIfNum = e.keyCode === 69 || e.keyCode === 190 || e.keyCode === 187 || e.keyCode === 189;
        }
        return checkIfNum && e.preventDefault();
      }

const setAllValue = e => {
    setCardDetails({
      ...cardDetails,
      [e.target.name]: e.target.value
    });
  };
  const handleAgreeChecking = async() =>{
    await checkAgree(!agreeStatus);
 }

 const submitPaymentDetails = () => {
    // setSubmitStatus(!isSubmit)
    // localStorage.setItem('payment', '4');
    // window.scrollBy(0, -5000);

    if (localStorage.hasOwnProperty("tempUserId")) {
        let currentYear = new Date().getFullYear();
    let inputYear = cardDetails.expiry_year;
    

     if (cardDetails.cardholder_name === '') {
         alert('Please enter card holder name');
     }else if (cardDetails.card_number === '') {
         alert('Please enter card number');
     }else if (cardDetails.expiry_month === '') {
        alert('Please enter expire month');
     }else if (cardDetails.expiry_month < 1 || cardDetails.expiry_month > 12) {
        alert('Expire month is not valid');
    }else if (cardDetails.expiry_year === '') {
        alert('Please enter expire year');
     }else if (cardDetails.expiry_year < currentYear) {
        alert('Please check expire year');
    }else if(cardDetails.CVV === '') {
         alert('Please enter CVV Number');
     }else if(!agreeStatus) {
         alert('Please check the agreement');
     }else{
        cardDetails.user_id = localStorage.getItem('tempUserId');
        console.log('UsserDetails: ', cardDetails);
        //http://111.93.169.90:2030/transaction/payment/
        apiCallWithToken(BASE_API_URL + 'transaction/payment/', 
        'post',
        cardDetails)
            .then(response => {
            console.log("Response: ", response.data.token);
            // alert('You have logged in successfully');
            //props.setToggle('4');
            localStorage.setItem('payment', '4');
            window.scrollTo(0, 0);
            localStorage.setItem('userToken', response.data.token);
            localStorage.setItem('userId', response.data.user['id']);
            localStorage.setItem('tempUserId', '');
            setSubmitStatus(!isSubmit)
            })
            .catch(err => {
                alert('Card number is invalid');
            console.log("Error: ",err);
            })
     }
    }else {
        alert("Please fill up basic info to continue");
      }
    
 };

    const checkYear = () => {
        
    }

    const handlingCardExpiry = (text) => {
        let sdsd = text.index["."]
        if (text.indexOf(".") >= 0 || text.length > 7) {
          return;
        }
    
        // if (text.length === 2 && creditCardExpireDate.length === 1) {
        //   text += "/";
        // }
        // setCreditCardExpireDate({
        //     creditCardExpireDate: text
        //   });
        
      }

      const formatCreditCard = () => {
        var x = document.getElementById("credit-card");
        var index = x.value.lastIndexOf('-');
        var test = x.value.substr(index + 1);

        if (test.length === 4)
             x.value = x.value + ' ';
    }

    const maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
         object.target.value = object.target.value.slice(0, object.target.maxLength)
          }
        }

    const datePicker = () => {
        let d = document.getElementById("myDate").value;
        alert(d);
        // document.getElementById("demo").innerHTML = d;
    }

  return(
    isSubmit ?
    <Succesful callTab = {() => props.setToggle('4')}/> :
    <React.Fragment>
      <div className="payment-tab common-padd bgright">
        <Container>
            <div className="in-basic">
              <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                <h2 className=" text-green font-weight-normal"> Set up your credit or debit card </h2>
                <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
              </div>
              <div className="d-flex justify-content-center align-items-center py-4">
                  <img src={ImagePath.pay1} alt="payment" className="img-fluid pay-img" />
                  <img src={ImagePath.pay2} alt="payment" className="img-fluid pay-img" />
                  <img src={ImagePath.pay3} alt="payment" className="img-fluid pay-img" />
                  <img src={ImagePath.pay4} alt="payment" className="img-fluid pay-img" />
              </div>                    
              <Row className="contact-wrap">
                <Col md="6">
                    <FormGroup>
                        <Label>Cardholder's Name</Label>
                        <div className="form-input">
                            <Input type="text" className="form-control" name='cardholder_name' placeholder="Enter Name" onChange={setAllValue}/>
                            <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                        </div>
                    </FormGroup>
                </Col>
                <Col md="6">
                    <FormGroup>
                        <Label>Expiry Date</Label>
                        <div className="d-md-flex d-lg-flex justify-content-between">
                            <div className="form-input mb-2 mb-md-0 mb-lg-0">
                            {/* onKeyDown={ e => ( e.keyCode === 69 || e.keyCode === 190 ) && e.preventDefault() } */}
                                <Input type="number" id="myDate" name='expiry_month' className="form-control" placeholder="MM"  maxLength={2} onInput={maxLengthCheck} onChange={setAllValue} onKeyDown={formatInput}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faCalendar} /></span>
                            </div>
                            <div className="form-input">
                                <Input type="number" id="myDate" name='expiry_year' className="form-control" placeholder="YYYY"  maxLength={4} onInput={maxLengthCheck} onChange={setAllValue} onKeyDown={formatInput}/>
                                <span className="icon-wr"><FontAwesomeIcon icon={faCalendar} /></span>
                            </div>
                        </div>
                    </FormGroup>
                </Col>
                <Col md="6">
                    <FormGroup>
                        <Label>Card Number</Label>
                        <div className="form-input">
                            <Input type="number" maxLength={19} id="credit-card" name='card_number' className="form-control" placeholder="Enter Here.." onInput={maxLengthCheck} onChange={setAllValue} onKeyDown={formatInput}/>
                            <span className="icon-wr"><FontAwesomeIcon icon={faCreditCard} /></span>
                        </div>
                    </FormGroup>
                </Col>
                <Col md="6">
                    <FormGroup>
                        <Label>Security Code CVV</Label>
                        <div className="form-input">
                            <Input type="number" maxLength={3} className="form-control" name='CVV' placeholder="CVV" onInput={maxLengthCheck} onChange={setAllValue} onKeyDown={formatInput}/>
                            <span className="icon-wr"><FontAwesomeIcon icon={faQuestionCircle} /></span>
                        </div>
                    </FormGroup>
                </Col>
                <Col md="12">
                    <FormGroup>
                        <label className="pl-0 font-weight-light">By ticking the tickbox below, you agree to our <span className="text-green"> Terms of Use, Privacy Statement.</span></label>
                    </FormGroup>
                </Col>
                <Col md="12">
                    <FormGroup>
                        <div className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" id="c2" value={agreeStatus} onChange={handleAgreeChecking} defaultChecked={agreeStatus}/>
                            <label className="custom-control-label" for="c2">I Agree</label>
                          </div>
                    </FormGroup>
                </Col>
            </Row>  

            <div className="py-4">
              <button className="btn btn-dark" type="submit" onClick={submitPaymentDetails}>Confirm <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating" /></button>
            </div>


            </div>
        </Container>
    </div>
    </React.Fragment>
  );
}
export default TabContentFour;