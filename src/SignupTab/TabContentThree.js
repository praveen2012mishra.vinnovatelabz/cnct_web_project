import React from 'react';
import { Container, Row, Col, FormGroup, Label, Button, CardBody,Input,Alert } from 'reactstrap';
import { Multiselect } from "multiselect-react-dropdown";
import {ImagePath} from './../ImagePath';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGraduationCap,faImage } from '@fortawesome/free-solid-svg-icons';
import BASE_API_URL from '../Config/config';
import {getApi, postApi, apiCallWithToken} from '../Config/api';
import Succesful from './Succesful';
// import $ from "jquery";
import './style.css';
// var input = $("#control");

class TabContentThree extends React.Component{
  fileMap = new Map()
  constructor(props) {
    super(props);
    this.state = {
      plainArray: ["Option 1", "Option 2", "Option 3", "Option 4", "Option 5"],
      selectedValues: [
        
      ],
      isCertificate:false,
      pdf:"",
      isDisabled:false,
      in:true,
      selectionStatus: '',
      isSubmit: false,
      option:"",
      array:[],
      isInputDisabled:true,

    };    
  }
  myMethod = () => {
    this.setState({
      isSubmit: !this.state.isSubmit
    })
  }
 
  onChangeHandler = (event) => {
   
   
    if(this.state.option!==""){
    this.setState({
      pdf: event.target.files[0],
    
     
    }); 
  
  
  }
  };

  handleMain=(e,b)=>{
    console.log(b)
    this.setState({option:b})
     this.state.array.push(b)
     this.setState({pdf:""})
     this.setState({isDisabled:true})
   if(this.state.array.length!==0){
     this.setState({isInputDisabled:false})
   }
    
  }
  handleData=(e)=>{
   
   setTimeout(this.redirect,400)
    this.resetPdf();
    
   
  }
 
  resetPdf=()=>{
    this.setState({pdf:""})
    this.forceUpdate();
   
  }

redirect=()=>{
 
  if(this.state.pdf!=="" && this.state.pdf!=undefined){
  console.log(this.state.pdf.name)
    
  this.fileMap.set(this.state.option,this.state.pdf.name)
  
  this.setState({isDisabled:false})
  this.forceUpdate();
}


}
  handleSubmit=(e)=>{
    // this.setState({
    //   isSubmit: !this.state.isSubmit
    // })
    console.log(this.state.array)
    var b=Array.from(this.fileMap.values());
    if(this.state.array.length!==b.length){
      alert("Please Uplaod one file for each type")
      this.setState({isDisabled:false})
    }
    if (localStorage.hasOwnProperty("tempUserId")) {
      if (this.state.selectionStatus === '') {
        alert("Please choose profile type")
      }else {
        apiCallWithToken(BASE_API_URL+ 'web/registerprofiletype/', 
        'post',
        {"user_id":localStorage.getItem('tempUserId'),
        "profile_level":this.state.selectionStatus
        })
        .then(response => {
          console.log(response);
          window.scrollTo(0, 0);
          if (this.state.selectionStatus === "S") {
            alert("Profile type saved.");
            localStorage.setItem('pageType', '3');
            this.props.setToggle('3')
          }else{
            // alert("Signup Successful ")
            // this.props.setToggle('4')
            localStorage.setItem('userToken', response.data.token);
            localStorage.setItem('userId', response.data.user['id']);
            localStorage.setItem('tempUserId', '');
            this.setState({
              isSubmit: !this.state.isSubmit
            })
          }
        })
        .catch(err => {
          console.log(err);
          alert("Some error occured.")
          
        });
        e.preventDefault();
      }
  }else {
    alert("Please fill up basic info to continue");
  }
    
  }
  addItem() {
    this.selectedValues.push({ key: "Option 1"});
    console.log("hii")
  }
  handleStarter = (e) => {
    this.setState({ 
      selectionStatus: 'F',
      isCertificate: false });
   
  };
  handleStandard = (e) => {
  
    this.setState({ 
      selectionStatus: 'S',
      isCertificate: true });
   
  };

 
  renderFile(){
    
  var lengths= this.fileMap.size
   var b=Array.from(this.fileMap.values())
  // var removeDuplicate= [...new Set(b)]
   console.log(b)
   if(lengths>0){

   
  return b.map((item) => {
    return (
       <div>
       <p style={{marginLeft:"5px"}}>{item}</p>
     </div>
   
     
     );
   });
 


}
  
 


 


  
 
  
   }
  
  
   onRemove=(e,b) =>{

   
     this.fileMap.delete(b)
     var a = this.state.array.indexOf(b);
     this.state.array.splice(a, 1)
     this.setState({option:""})
     this.setState({pdf:""})
     if(this.state.array.length===0){
      this.setState({isInputDisabled:true})
    }
    else if(this.state.array.length>0){
     this.setState({isInputDisabled:false})
    }
     
}
render() {
  


console.log(this.state.pdf)
const { plainArray, selectedValues } = this.state;

  return(
    this.state.isSubmit ?
    <Succesful  callTab = {() => this.props.setToggle('4')}/> :
    <React.Fragment>
      <div className=" profile-tab common-padd">
            <Container>
                <div className="in-basic">
                    <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
                        <h2 className=" text-green">Profile <span className="font-weight-exlight">Level</span> </h2>
                        <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
                    </div>

                    <div className="price-box">
                            <div className="shadow border"></div>
                            <div className="price-box-headers">
                                <div className="pbox1st price-table-header w30">
                                    <div className="clearfix">
                                        <Label className="p-label">Pick the plan that works for you</Label>
                                    </div>                        
                                </div>
                                <div className="pbox2nd price-table-header ">
                                    <div className="clearfix">
                                        <Label className="h-label mb40">Starter</Label>
                                        <Label className="h-label text-uppercase">FREE</Label>
                                        <Button type="button" className="btn btn-round border-dark" onClick={this.handleStarter}>Sign Up</Button>
                                        
                                    </div>
                                </div>
                            
                                <div className="pbox3rd price-table-header ">
                        <div className="clearfix">
                            <label className="h-label">Standard</label>
                           
                            <label className="h-label text-black">Most Popular </label>
                            <label className="h-label ">£4 per month</label>
                            <Button type="button" className="btn btn-round bg-white text-green shadow" onClick={this.handleStandard}>Sign Up</Button>
                        </div>
                    </div>
                                <div className="pbox4th price-table-header ">
                                    <div className="clearfix">
                                        <Label className="h-label">Enterprise</Label>
                                        <img src={ImagePath.chimni} className="img-fluid mx-auto d-block mb-2" alt="chimni" />
                                        <Button type="button" className="btn btn-round text-white">Contact Us</Button>
                                    </div>
                                </div>
                            </div>
                            <div className="price-box-list">
                                <div className="p-box-row">
                                      <div className="p-box-col w30">
                                        <Label for="" className="p-label">Users</Label>
                                      </div>  
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">1</Label>
                                      </div>
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">17</Label>
                                       
                                      </div>
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">Multiple</Label>
                                      </div>
                                </div>
                                <div className="p-box-row gray-col">
                                      <div className="p-box-col w30">
                                        <Label for="" className="p-label">Profile Basics</Label>
                                      </div>  
                                      <div className="p-box-col">                           
                                      </div>
                                      <div className="p-box-col">                            
                                      </div>
                                      <div className="p-box-col">                            
                                      </div>
                                </div>
                                <div className="p-box-row">
                                      <div className="p-box-col w30">
                                            <Label for="" className="p-label">Create Profile</Label>
                                      </div>  
                                      <div className="p-box-col">
                                        <label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </label>
                                      </div>
                                      <div className="p-box-col">
                                        <label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </label>
                                      </div>
                                      <div className="p-box-col">
                                        <label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </label>
                                      </div>
                                </div>
                                <div className="p-box-row">
                                      <div className="p-box-col w30">
                                            <Label for="" className="p-label">Full Transaction History</Label>
                                      </div>  
                                      <div className="p-box-col">
                                      <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>
                                      </div>
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>
                                      </div>
                                      <div className="p-box-col">
                                        <label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </label>
                                      </div>
                                </div>
                                <div className="p-box-row">
                                      <div className="p-box-col w30">
                                            <Label for="" className="p-label">Ability to Outsource (Buy) and Subcontract (Sell)</Label>
                                      </div>  
                                      <div className="p-box-col">
                                      <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>                           
                                      </div>
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>
                                      </div>
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>
                                      </div>
                                </div>
                                <div className="p-box-row">
                                      <div className="p-box-col w30">
                                            <Label for="" className="p-label">Dynamic Rating</Label>
                                            <div className="position-relative ml-4">
                                                <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                                <div className="popout">
                                                    <smal>Upon project completion, both parties rate each other out of 10. The weighted average rating is then displayed on your profile to provide a further indication of your credibility to potential customers/suppliers. Weighted average calculated as: average of last 5 projects, plus average of all previous projects, divided by two.</smal>
                                                </div>
                                                </span>
                                            </div>
                                      </div>  
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>
                                      </div>
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>
                                      </div>
                                      <div className="p-box-col">
                                        <Label for="" className="p-label">
                                            <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                        </Label>
                                      </div>
                                </div>
                                <div className="p-box-row gray-col">
                                    <div className="p-box-col w30">
                                          <Label for="" className="p-label">Outsourcing (Buying)</Label>
                                    </div>  
                                    <div className="p-box-col">                           
                                    </div>
                                    <div className="p-box-col">                            
                                    </div>
                                    <div className="p-box-col">                            
                                    </div>
                              </div>
                              <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <label for="" className="p-label">Post Jobs	</label>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                        <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                    </Label>                   
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Upload Accompanying Data</Label>
                                      <div className="position-relative ml-4">
                                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                        <div className="popout">
                                            <smal>Upload all accompanying data to support specification of the project such as CAD, Post Processors or Tool Databases.</smal>
                                        </div>
                                        </span>
                                    </div>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">See Full Capabilities of Bidding Subcontractors</Label>
                                      <div className="position-relative ml-4">
                                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                        <div className="popout">
                                            <smal>Make educated decisions about which potential vendor is right for your business. When a subcontractor bids on your job, look up their ratings, capabilities and experience to pick the best provider for your project.</smal>
                                        </div>
                                        </span>
                                    </div>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Require Non-Disclosure Agreements</Label>
                                      <div className="position-relative ml-4">
                                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                        <div className="popout">
                                            <smal>Handling sensitive data? Safeguard it with a NDA and it will be hidden from matched subcontractors until they sign your documentation.</smal>
                                        </div>
                                        </span>
                                    </div>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row gray-col">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Subcontracting (Selling)</Label>
                                </div>  
                                <div className="p-box-col">                           
                                </div>
                                <div className="p-box-col">                            
                                </div>
                                <div className="p-box-col">                            
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Match to Relevant Jobs</Label>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Priority Matching</Label>
                                </div>  
                                <div className="p-box-col">

                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Sign Non-Disclosure Agreements</Label>
                                      <div className="position-relative ml-4">
                                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                        <div className="popout">
                                            <smal> It is a long established fact that a reader will be distracted by the readable content</smal>
                                        </div>
                                        </span>
                                    </div>
                                </div>  
                                <div className="p-box-col">                     
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                    <Label for="" className="p-label">List People Capabilities (Skills)</Label>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                        <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                    </Label>                   
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                    <Label for="" className="p-label">List Machine Capabilities</Label>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                        <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                    </Label>                   
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                    <Label for="" className="p-label">Upload Certificates</Label>
                                </div>  
                                <div className="p-box-col">
                                                     
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                        
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                    <Label for="" className="p-label">Upload Photos of Your Machine Assets</Label>
                                </div>  
                                <div className="p-box-col">
                                                     
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          {/* <div className="p-box-row gray-col">
                            <div className="p-box-col w30">
                                  <Label for="" className="p-label">Transaction Fees</Label>
                                  <div className="position-relative ml-4">
                                    <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                    <div className="popout">
                                        <smal> It is a long established fact that a reader will be distracted by the readable content</smal>
                                    </div>
                                    </span>
                                </div>
                            </div>  
                            <div className="p-box-col">                           
                            </div>
                            <div className="p-box-col">                            
                            </div>
                            <div className="p-box-col">                            
                            </div>
                      </div> */}
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Transaction Fees</Label>
                                        <div className="position-relative ml-4">
                                            <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                            <div className="popout">
                                                <smal> Transaction Fees deducted from seller's fee. For example, if a project is worth £500, and transaction fee is 10%, £500 is paid by the buyer and the seller receives £400. Transaction fees are determined by the seller's profile level.</smal>
                                            </div>
                                            </span>
                                        </div>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                    10%
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                    5%
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                    Included
                                  </Label>
                                </div>
                          </div>
                          {/* <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Transaction Fees – 
                                        Machine Projects</Label>
                                        <div className="position-relative ml-4">
                                            <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                            <div className="popout">
                                                <smal> It is a long established fact that a reader will be distracted by the readable content</smal>
                                            </div>
                                            </span>
                                        </div>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                    5%
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <label for="" className="p-label">
                                   2%
                                  </label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                    Included
                                  </Label>
                                </div>
                          </div> */}
                          <div className="p-box-row gray-col">
                            <div className="p-box-col w30">
                                  <label for="" className="p-label">Payment Terms</label>
                            </div>  
                            <div className="p-box-col">                           
                            </div>
                            <div className="p-box-col">                            
                            </div>
                            <div className="p-box-col">                            
                            </div>
                      </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Instant CNCT Payments</Label>
                                      <div className="position-relative ml-4">
                                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                        <div className="popout">
                                            <smal> INSTANT CNCT is our immediate payment method. Electronic payment is taken from the buyer at the point that the project is agreed. Once the project is completed, both parties confirm that it has been completed correctly and payment is automatically sent to the seller's account (minus the transaction fee). Buyer's receive a sales invoice from CNCT and seller's receive a purchase invoice from CNCT</smal>
                                        </div>
                                        </span>
                                    </div>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Special Terms</Label>
                                      <div className="position-relative ml-4">
                                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                        <div className="popout">
                                            <smal> Sometimes, on larger projects, we appreciate that extended payment terms are needed. Special Terms allows the buyers and seller to agree their own payment terms and even upload terms and conditions of business. When the project is agreed, we take a payment equal to the seller's transaction fee from the buyer (akin to a deposit). When the project is completed, we send a completion note to both the buyer and seller and the seller invoices the buyer independently and directly for the balance (agreed project fee, mins transaction fee).</smal>
                                        </div>
                                        </span>
                                    </div>
                                </div>  
                                <div className="p-box-col">                     
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row gray-col">
                            <div className="p-box-col w30">
                                  <label for="" className="p-label">Partnerships</label>
                            </div>  
                            <div className="p-box-col">                           
                            </div>
                            <div className="p-box-col">                            
                            </div>
                            <div className="p-box-col">                            
                            </div>
                      </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <label for="" className="p-label">Payments Powered via</label>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.stripe} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.stripe} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <label for="" className="p-label">Data Hosting and Storage via</label>
                                </div>  
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.aws} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.aws} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row gray-col">
                            <div className="p-box-col w30">
                                  <label for="" className="p-label">Extras</label>
                            </div>  
                            <div className="p-box-col">                           
                            </div>
                            <div className="p-box-col">                            
                            </div>
                            <div className="p-box-col">                            
                            </div>
                      </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <Label for="" className="p-label">Monthly Industry Trend Reports</Label>
                                        <div className="position-relative ml-4">
                                            <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                            <div className="popout">
                                                <smal> Sometimes, on larger projects, we appreciate that extended payment terms are needed. Special Terms allows the buyers and seller to agree their own payment terms and even upload terms and conditions of business. When the project is agreed, we take a payment equal to the seller's transaction fee from the buyer (akin to a deposit). When the project is completed, we send a completion note to both the buyer and seller and the seller invoices the buyer independently and directly for the balance (agreed project fee, mins transaction fee).</smal>
                                            </div>
                                            </span>
                                        </div>
                                </div>  
                                <div className="p-box-col">
                                  
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                          <div className="p-box-row">
                                <div className="p-box-col w30">
                                      <label for="" className="p-label">Post Processor Exchange</label>
                                      <div className="position-relative ml-4">
                                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                        <div className="popout">
                                            <smal> Gain access to a free online database of post processors, digital twins and virtual machines, to better enable remote programming. This is open source and self-serve and we encourage our customers to contribute to our community but sharing any safe post processors that they have available.</smal>
                                        </div>
                                        </span>
                                    </div>
                                </div>  
                                <div className="p-box-col">                      
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                                <div className="p-box-col">
                                  <Label for="" className="p-label">
                                      <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                                  </Label>
                                </div>
                          </div>
                            </div>
                        </div>
                        <div className="mobile-price-slide">
                            <ul className="list-unstyled price-slide">
                                <li>
                                    <Col xs="12" sm="12" md="12" lg="12" className="princing-item red">
                                        <div className="pricing-divider ">
                                            <h5 className="">Starter</h5>
                                          <h4 className="my-0 font-weight-normal mb-3">FREE </h4>
                                           
                                        </div>
                                        <CardBody className="bg-white mt-0 shadow">
                                          <ul className="list-unstyled mb-5 position-relative">
                                            <li><b>10</b> users included</li>
                                            <li><b>2 GB</b> of storage</li>
                                            <li><b>Free </b>Email support</li>
                                            <li><b>Help center access</b></li>
                                          </ul>
                                          <Button type="button" className="btn btn-round border-dark">Sign </Button>
                                        </CardBody>
                                      </Col>
                                </li>
                                <li>
                                    <Col xs="12" sm="12" md="12" lg="12" className="princing-item green">
                                        <div className="pricing-divider ">
                                            <h5 className="text-light">Standard</h5>
                                          <h4 className="my-0 text-light font-weight-normal mb-3"><span className="h3">£</span> 19 <span className="h5">/mo</span></h4>
                                          
                                        </div>
                              
                                        <CardBody className="bg-white mt-0 shadow">
                                          <ul className="list-unstyled mb-5 position-relative">
                                            <li><b>300</b> users included</li>
                                            <li><b>20 GB</b> of storage</li>
                                            <li><b>Free</b> Email support</li>
                                            <li><b>Help center access</b></li>
                                          </ul>
                                          <Button type="button" className="btn btn-round">Sign Up</Button>
                                        </CardBody>
                                      </Col>
                                </li>
                               
                            </ul>
                        </div>
                      {this.state.isCertificate &&   
                        <div className="b-pdf mt-5">
                            <div className="form-group border-bottom pb-1">
                                <Label className="pl-0">Add Certificate </Label>
            
                            </div>
              
                            <FormGroup>
                                <Label className="pl-0">Certificate Type</Label>
                                <Multiselect options={plainArray} isObject={false} selectedValues={selectedValues}  onSelect={this.handleMain} onRemove={this.onRemove} disable={this.state.isDisabled}/>
                                {/* <span className="multi-select-file"></span> */}
                                <div className="row" style={{marginLeft:"10px"}}>
                                         {this.renderFile()}
                                          </div>
                            </FormGroup>
                                 
                         
                     
                            <Row>
                                <Col md="6">
                                    <FormGroup>
                                        <Label>Upload Certificate</Label>
                                        <div className="form-input">
                                {/*        <Label  className="btn btn-upload">Upload Image</Label>
              
                 
                 <input type="file" accept="application/pdf" onChange={this.onChangeHandler} onBlur={this.handleArray}   id="control" name="file"></input>
                                            <span className="icon-wr"><FontAwesomeIcon icon={faGraduationCap}  /></span>
                      */}
                      <label className="fileContainer"  ><span style={{fontWeight:"lighter"}} >
      Upload File</span>
    <input type="file"  accept="application/pdf" onChange={this.onChangeHandler} onInput={this.handleData}  disabled={this.state.isInputDisabled}  />
   
</label>
<span className="icon-wr"><FontAwesomeIcon icon={faGraduationCap}  /></span>
                                        </div>
                                    </FormGroup>
                                </Col>
                            </Row>

                        </div>
}
                        <div className="py-4 d-flex justify-content-end">
                            <Button className="btn btn-dark" type="submit" onClick={this.handleSubmit}>FINISH <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating" /></Button>
                        </div>


                </div>
                      
            </Container>


           



        </div>
    </React.Fragment>
  );
}
}
function Welcome(props) {
  return <h1>{props.name}</h1>;
}
export default TabContentThree;