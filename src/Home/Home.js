import  React, {useEffect} from 'react';
import { Container, Row, Col, Card, CardBody, CardTitle, CardImg, CardText } from 'reactstrap';
import {ImagePath} from './../ImagePath';
import AnchorLink from 'react-anchor-link-smooth-scroll'

const Home = () =>{
  
  return(
    <React.Fragment>
      
    {/* Banner */}
    <section className="banner-section">
      {/* <img src={ImagePath.banner} alt="slider" className="w-100" /> */}
      <img src={ImagePath.bannerIcon} alt="slider" className="sliderImg" />
      <div className="caption">
        <Container className="caption-position">
          <div className="d-flex mb-5">
            <div className="black-skew ">
              <h2>Isn’t it time you</h2>
            </div>
            <div className="green-skew mt-1 ml-1">
              <h2>CNCTED?</h2>
            </div>
          </div>
          <div className="clearfix">
            <p className="bannerText mb-5"><img src={ImagePath.loaderCircle} className="bannerCircle rotating" alt="green loder" /><strong>OUTSOURCING</strong> <br />subcontractors you need to deliver on your critical projects.</p>
            <p className="bannerText"><img src={ImagePath.loaderCircle} className="bannerCircle rotating" alt="green loder" /><strong>SUBCONTRACTING</strong> <br />work relevant to your skills and machine capabilities.</p>
          </div>
          <AnchorLink href='#industry' className="explore-btn">Explore CNCT</AnchorLink>
          </Container>
      </div>
    </section>
    {/* End Banner */}
  

  {/* featured */}
  <section className="featured-compnay common-padd pb-0" id="industry">
    <Container>
      <div className="text-center mb-4 mb-md-5 pb-md-3">
        <h2 className="font-weight-light">Addressing the ultimate</h2>
        <h2 className="font-weight-bold text-uppercase">industry-wide problem</h2>
      </div>
      <Row>
        <Col md="6">
          <div className="supply-chain supply">
            <div className="wrong-part position-relative">
              <h4 className="text-center">Supply </h4>
              <figure>
                <img src={ImagePath.icon1} className="img-fluid" alt="icon" />
              </figure>
              <div className="content">
                <p>Manufacturers unable to outsource projects as they cannot locate the relevant specialist skills or
                  machines required to complete the work</p>
              </div>

            </div>
            <div className="right-part">
              <figure>
                <img src={ImagePath.icon1Right} className="img-fluid" alt="icon" />
              </figure>
              <div className="content">
                <p>Quickly locate certified suppliers with the key relevant capabilities to keep your project on the
                  critical path, whilst ensuring you never pay above true market rates</p>
              </div>
            </div>
          </div>
        </Col>
        <Col md="6">
          <div className="supply-chain demand">
            <div className="wrong-part">
              <h4 className="text-center">Demand </h4>
              <figure>
                <img src={ImagePath.icon2} className="img-fluid" alt="icon" />
              </figure>
              <div className="content">
                <p>Subcontractors with the right skills or machines cannot find the specialist work suited to their
                  capabilities</p>
              </div>

            </div>
            <div className="right-part position-relative">
              <figure>
                <img src={ImagePath.icon2Right} className="img-fluid" alt="icon" />
              </figure>
              <div className="content">
                <p>Pick up new projects suited to your skills and resources and get paid immediately upon completion
                  with INSTANT CNCT</p>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  </section>
  {/* End featured */}


  
  {/* Category */}
  <section class="category overflow-hidden">
      <Container>
        <div class="text-center text-white mb-4 mb-md-5 ">
          <h2 class="font-weight-light">To provide a simple solution,</h2>
          <h2 class="font-weight-bold text-uppercase">we understand the complex</h2>
        </div>
        <div class="row  animatedParent">
            <Col md="4" class="animated bounceInLeft animate-3">
              <Card>
                <CardBody>
                    <div class="d-flex align-items-center">
                      <CardTitle>Human Factors</CardTitle>
                      <CardImg class="img-fluid" src={ImagePath.ficon1} alt="Card image cap" />
                    </div>
                    <CardText>Sometimes you need an ISO:9001 certified programmer for aerospace work, sometimes it’s critical for your press-tool designer to be fluent in VISI CAD. Our algorithms incorporate the skills, specialisms, software, experience and certification to perfectly marry supply and demand.</CardText>
                </CardBody>
              </Card>
            </Col>
            <Col md="4" class="animated bounceInLeft animate-2">
              <Card>
                <CardBody>
                    <div class="d-flex align-items-center">
                        <CardTitle>Machine Factors</CardTitle>
                        <CardImg class="img-fluid" src={ImagePath.ficon2} alt="Caicon" />
                    </div>
                    <CardText>You’d struggle to turn Inconel on a machine optimised for 3-axis milling of clay prototype cars, so our matrix takes into account the nuances of size, material and operations of your projects to match with the machines capable of cutting (or additive machining) a perfect finished component.</CardText>
                </CardBody>
              </Card>
            </Col>
            <Col md="4" class="animated bounceInLeft animate-1">
              <Card>
                <CardBody>
                    <div class="d-flex align-items-center">
                        <CardTitle>Other Project Factors</CardTitle>
                        <CardImg class="img-fluid" src={ImagePath.ficon3} alt="Caicon" />
                    </div>
                    <CardText>Whether you need other specialist services such as injection moulding, or your project hinges on location-sensitive production or an extremely tight deadline, you can build in all of your key requirements. You can even specify whether you need a resource to complete a project or for a certain amount of time.</CardText>
                </CardBody>
              </Card>
            </Col>
        </div>
      </Container>
  </section>
  {/* End Category */}

  
  {/* how-works */}
  <section className="how-works common-padd">
    <Container>
      <div className="text-center mb-md-5 mb-4">
          <h2>How <strong>CNCT</strong> Works</h2>
      </div>
      <ul className="list-unstyled list-tree">
          <li>
            <div className="list-content">
              <div className="d-flex align-items-center mb-4">
                  <img src={ImagePath.work1} className="icon" alt="icon" />
                  <h4>Upload Profile</h4>
              </div>
              <p>Sign up for free, as an independent or a business and list your capabilities in all their nuanced glory. Want to stand out? Include your certificates and key customer experience.</p>
            </div>
          </li>
          <li>
            <div className="list-content">
              <div className="d-flex align-items-center mb-4">
                  <img src={ImagePath.work2} className="icon" alt="icon" />
                  <h4>Post Jobs</h4>
              </div>
              <p>Looking to sub work out? Post your job for free, detailing all of your key requirements. Sensitive info? Attach a Non-Disclosure-Agreement, requiring signature prior to accessing your job data, which of course you can upload too, be it CAD, post processors or any other associated info.</p>
            </div>
          </li>
          <li>
            <div className="list-content">
              <div className="d-flex align-items-center mb-4">
                  <img src={ImagePath.work3} className="icon" alt="icon" />
                  <h4>Intelligent Matching</h4>
              </div>
              <p>Time for us to do some work. This is where our algorithms pinpoint the right profiles capable of completing the job. Once we have them, we instantly push the project to these providers.

              </p>
            </div>
          </li>
          <li>
            <div className="list-content">
              <div className="d-flex align-items-center mb-4">
                  <img src={ImagePath.work4} className="icon" alt="icon" />
                  <h4>Negotiating</h4>
              </div>
              <p>Once matched, providers bid for the project and utilise our secure messaging client to discuss any queries relating to the job. Once a price and terms are agreed, we take payment from the buyer.</p>
            </div>
          </li>
          <li>
            <div className="list-content">
              <div className="d-flex align-items-center mb-4">
                  <img src={ImagePath.work5} className="icon" alt="icon" />
                  <h4>Completion and Payment</h4>
              </div>
              <p>Once the work is completed, the subcontractor confirms completion via the CNCT app and this is then acknowledged by the happy buyer, upon their receipt of the work. At this point, payment is transferred from CNCT to the subcontractor, minus a small transaction fee.
              </p>
            </div>
          </li>
      </ul>
    </Container>
  </section>
  {/* End how-works */}


  


    </React.Fragment>
  )
}

export default Home;