import React, { useState, useEffect } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
} from 'reactstrap';

import { NavLink, Link} from 'react-router-dom';

import {ImagePath} from './../ImagePath';

import AnchorLink from 'react-anchor-link-smooth-scroll'

const Header = (props) =>{
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  // on scroll header fixed
  const [fixedHeader, setFixedHeader] = useState(false);
  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
  });
  const getStatus =(status)=>{
    localStorage.setItem('pageType', '');
    localStorage.setItem('payment', '');
    // props.setToggle('1');
  }
  const handleScroll = () => {
      if (window.scrollY > 100) {
        setFixedHeader(true)
      } else if (window.scrollY < 100) {
        setFixedHeader(false)
      }
  }

  const btnLogout = () => {
    localStorage.removeItem('userId');
    window.location.href='/Home';
}
  return(
    <React.Fragment>
      {localStorage.getItem('userId') ? 
      (<header className={"header"}>
      <Navbar light expand="md" className="dashboard-nav-top">
          <Container>
              {/* <NavLink to="/"><img src={ImagePath.logo} className="img-fluid" alt="logo" /></NavLink> */}
              <NavbarToggler onClick={toggle} />
              <Collapse isOpen={isOpen} navbar id="navbarResponsive">
                  <Nav className="ml-auto" navbar>
                      <UncontrolledDropdown nav inNavbar>
                          <DropdownToggle nav caret>
                              Solutions
                          </DropdownToggle>
                          <DropdownMenu right>
                              <DropdownItem tag={Link} to="/Solution">
                                  Freelancing
                              </DropdownItem>
                              <DropdownItem tag={Link} to="/Solution">
                                  Project Management
                              </DropdownItem>
                              <DropdownItem tag={Link} to="/Solution">
                                  Sales
                              </DropdownItem>
                              <DropdownItem tag={Link} to="/Solution">
                                  Procurement
                              </DropdownItem>
                          </DropdownMenu>
                      </UncontrolledDropdown>
                      <NavItem>
                          <NavLink to="/Pricing">Pricing</NavLink>
                      </NavItem>
                      <NavItem>
                          <NavLink to="/Contact">Contact us</NavLink>
                      </NavItem>
                      <NavItem>
                          <NavLink to="/Signup" className="nav-btn black-btn mr-3"> Upgrade</NavLink>
                      </NavItem>
                      <NavItem>
                          <NavLink to="/" className="nav-btn outline" onClick={btnLogout}>Log Out</NavLink>
                      </NavItem>
                  </Nav>
              </Collapse>
          </Container>
      </Navbar>
  </header>
) : (<header className={fixedHeader? "header sticky" : "header"}>
<Navbar light expand="lg" >
<Container>
    <NavLink to="/"><img src={ImagePath.logo} className="logo" alt="logo" /></NavLink>
    <NavbarToggler onClick={toggle} />
    <Collapse isOpen={isOpen} navbar id="navbarResponsive">
      <Nav className="ml-auto" navbar>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Solutions
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem href="/Solution#freelancing">
              Freelancing
            </DropdownItem>
            <DropdownItem href="/Solution#project_management">
              Project Management
            </DropdownItem>
            <DropdownItem href="/Solution#sales">
              Sales
            </DropdownItem>
            <DropdownItem href="/Solution#procurement">
              Procurement
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <NavItem>
          <NavLink to="/Pricing">Pricing</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/Contact">Contact us</NavLink>
        </NavItem>
        {localStorage.getItem('payment') === '4' ?
        <NavItem>
          <NavLink to="" className="nav-btn green-btn mr-lg-3" >Upgrade</NavLink>
        </NavItem>:
        <NavItem>
          <NavLink to="/Signup" className="nav-btn green-btn mr-lg-3" >Sign Up for Free</NavLink>
        </NavItem>
        }
        {localStorage.getItem('payment') === '4' ?
        <NavItem>
          <NavLink to="/Login" className="nav-btn outline" onClick={getStatus}>Log Out</NavLink>
        </NavItem>:
        <NavItem>
          <NavLink to="/Login" className="nav-btn outline">login</NavLink>
        </NavItem>
        }
        
        
      </Nav>
    </Collapse>
    </Container>
  </Navbar>
</header>
)

    }

  

    </React.Fragment>
  )
}

export default Header;