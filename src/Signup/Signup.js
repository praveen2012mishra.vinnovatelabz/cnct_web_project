import React from 'react';
import { Container} from 'reactstrap';
import {ImagePath} from './../ImagePath';
import './style.css';
import SignupTab from'../SignupTab/SignupTab';

const Signup = () => {
    return (
        <React.Fragment>
            <section className="inner-banner position-relative">
                <img src={ImagePath.signupBanner} alt="slider" className="w-100"></img>
                <div className="caption">
                    <Container>
                        <div className="text-center">
                            <h1 className="font-weight-exlight mb-0">Welcome to <span className="text-green font-weight-normal">CNCT</span></h1>
                            <h6 className="font-weight-exlight">Get started with simple steps</h6>
                            <h5 className="text-uppercase font-weight-light">SIGN UP / CREATE PROFILE </h5>
                        </div>
                    </Container>
                </div>
            </section>
            
            <SignupTab/>

        </React.Fragment>
    )
}


export default Signup;