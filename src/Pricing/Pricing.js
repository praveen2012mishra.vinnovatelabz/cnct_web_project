
import React from "react";
import { Container, Col, CardBody, Button } from "reactstrap";
import { ImagePath } from "./../ImagePath";

const Pricing = () => {
  return (
    <React.Fragment>
      <section className="inner-banner position-relative">
        <img src={ImagePath.pricingBanner} alt="slider" className="w-100" />
        <div className="caption">
          <Container>
            <div className="text-center">
              <h1 className="font-weight-exlight">Pricing</h1>
              <h2 className="font-weight-light">
                Simple, low-cost and upgradable
              </h2>
            </div>
          </Container>
        </div>
      </section>

      <section className="common-padd pricing-sec pt-0">
        <Container>
          <div className="price-box">
            <div className="shadow border"></div>
            <div className="price-box-headers">
              <div className="pbox1st price-table-header w30">
                <div className="clearfix">
                  <label className="p-label">
                    Pick the plan that works for you
                  </label>
                </div>
              </div>
              <div className="pbox2nd price-table-header ">
                <div className="clearfix">
                  <label className="h-label mb40">Starter</label>
                  <label className="h-label text-uppercase">FREE</label>
                  <button type="button" className="btn btn-round border-dark">
                    Sign Up
                  </button>
                </div>
              </div>
              <div className="pbox3rd price-table-header ">
                <div className="clearfix">
                  <label className="h-label">Standard</label>
                  <label className="h-label text-black">Most Popular</label>
                  <label className="h-label ">£1 per month</label>
                  <button
                    type="button"
                    className="btn btn-round bg-white text-green shadow"
                  >
                    Sign Up
                  </button>
                </div>
              </div>
              <div className="pbox4th price-table-header ">
                <div className="clearfix">
                  <label className="h-label">Enterprise</label>
                  <img
                    src={ImagePath.chimni}
                    className="img-fluid mx-auto d-block mb-2"
                    alt="chimni"
                  />
                  <button type="button" className="btn btn-round text-white">
                    Contact Us
                  </button>
                </div>
              </div>
            </div>
            <div className="price-box-list">
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    At a glance
                  </label>
                </div>
                <div className="p-box-col glance-box">
                  <label for="" className="p-label glance-mb">
                    Outsource work, upload project data and protect it with a
                    NDA
                  </label>
                  <label for="" className="p-label glance-mb">
                    List your capabilities and match with subcontracting
                    opportunities
                  </label>
                  <label for="" className="p-label glance-mb">
                    {" "}
                    Get paid instantly upon completion of subcontract work
                  </label>
                </div>
                <div className="p-box-col glance-box">
                  <label for="" className="p-label glance-mb">
                    50% reduction on transaction fees
                  </label>
                  <label for="" className="p-label glance-mb">
                    Sign NDAs to access more opportunities
                  </label>
                  <label for="" className="p-label glance-mb">
                    {" "}
                    Agree custom terms - ideal for larger projects
                  </label>
                </div>
                <div className="p-box-col glance-box">
                  <label for="" className="p-label glance-mb">
                    Flexible business models and monthly billing
                  </label>
                  <label for="" className="p-label glance-mb">
                    Create custom filters for outsourcing work
                  </label>
                  <label for="" className="p-label glance-mb">
                    {" "}
                    Dedicated account management
                  </label>
                </div>
              </div>
              {/* <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Users
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    1
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    1
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    Multiple
                  </label>
                </div>
              </div> */}
              {/* <div className="p-box-row gray-col">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Profile Basics
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
              </div> */}
              <div className="p-box-row gray-col">
                <div className="p-box-col main-box">
                  <label for="" className="p-label">
                  Profile Basics
                  </label>
                </div>
                <div className="under-box-row">
                  <div className="extra-box">
                    <div>
                      <div className="extra-box-row">
                      <div className="p-box-col under-box">
                    <label for="" className="p-label">
                      Users
                    </label>
                  </div>
                  <div className="p-box-col">
                    <label for="" className="p-label">
                      1
                    </label>
                  </div>
                  <div className="p-box-col">
                    <label for="" className="p-label">
                      1
                    </label>
                  </div>
                  <div className="p-box-col">
                    <label for="" className="p-label">
                      Multiple
                    </label>
                  </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* <div className="p-box-row gray-col">
                
                <div className="p-box-col main-box">
                  <label for="" className="p-label">
                    Profile Basics
                  </label>
                </div>
                <div className="under-box-row">
                  <div className="p-box-col under-box">
                    <label for="" className="p-label">
                      Users
                    </label>
                  </div>
                  <div className="p-box-col">
                    <label for="" className="p-label">
                      1
                    </label>
                  </div>
                  <div className="p-box-col">
                    <label for="" className="p-label">
                      1
                    </label>
                  </div>
                  <div className="p-box-col">
                    <label for="" className="p-label">
                      Multiple
                    </label>
                  </div>
                </div>
              </div> */}
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Create Profile
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Full Transaction History
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Ability to Outsource (Buy) and Subcontract (Sell)
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Dynamic Rating
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="info icon" />
                      <div className="popout">
                        <smal>
                          Upon project completion, both parties rate each other
                          out of 10. The weighted average rating is then
                          displayed on your profile to provide a further
                          indication of your credibility to potential
                          customers/suppliers. Weighted average calculated as:
                          average of last 5 projects, plus average of all
                          previous projects, divided by two.
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row gray-col">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Outsourcing (Buying)
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Post Jobs{" "}
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Upload Accompanying Data
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="info icon" />
                      <div className="popout">
                        <smal>
                          Upload all accompanying data to support specification
                          of the project such as CAD, Post Processors or Tool
                          Databases.
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    See Full Capabilities of Bidding Subcontractors
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="info icon" />
                      <div className="popout">
                        <smal>
                          Make educated decisions about which potential vendor
                          is right for your business. When a subcontractor bids
                          on your job, look up their ratings, capabilities and
                          experience to pick the best provider for your project.
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              {/* <div className="p-box-row">
                    <div className="p-box-col w30">
                          <label for="" className="p-label">Priority Listing on 
                            Relevant Jobs</label>
                    </div>  
                    <div className="p-box-col">
                     
                    </div>
                    <div className="p-box-col">
                      <label for="" className="p-label">
                          <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                      </label>
                    </div>
                    <div className="p-box-col">
                      <label for="" className="p-label">
                          <img src={ImagePath.loaderCircle} className="p-circle" alt="CNCT" />
                      </label>
                    </div>
              </div> */}
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Require Non-Disclosure Agreements
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="incon" />
                      <div className="popout">
                        <smal>
                          {" "}
                          Handling sensitive data? Safeguard it with a NDA and
                          it will be hidden from matched subcontractors until
                          they sign your documentation.
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row gray-col">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Subcontracting (Selling)
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Match to Relevant Jobs
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Priority Matching
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Sign Non-Disclosure Agreements
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    List People Capabilities (Skills)
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    List Machine Capabilities
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Upload Certificates
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Upload Photos of Your Machine Assets
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              {/* <div className="p-box-row gray-col">
                <div className="p-box-col w30">
                      <label for="" className="p-label">Transaction Fees</label>
                      <div className="position-relative ml-4">
                        <span className="popoup"><img src={ImagePath.info} alt="incon" />
                        <div className="popout">
                            <smal> It is a long established fact that a reader will be distracted by the readable content</smal>
                        </div>
                        </span>
                    </div>
                </div>  
                <div className="p-box-col">                           
                </div>
                <div className="p-box-col">                            
                </div>
                <div className="p-box-col">                            
                </div>
          </div> */}
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Transaction Fees
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="incon" />
                      <div className="popout">
                        <smal>
                          {" "}
                          Transaction Fees deducted from seller's fee. For
                          example, if a project is worth £500, and transaction
                          fee is 10%, £500 is paid by the buyer and the seller
                          receives £400. Transaction fees are determined by the
                          seller's profile level.
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    10%
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    5%
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    Included
                  </label>
                </div>
              </div>
              {/* <div className="p-box-row">
                    <div className="p-box-col w30">
                          <label for="" className="p-label">Transaction Fees – 
                            Machine Projects</label>
                            <div className="position-relative ml-4">
                                <span className="popoup"><img src={ImagePath.info} alt="incon" />
                                <div className="popout">
                                    <smal> It is a long established fact that a reader will be distracted by the readable content</smal>
                                </div>
                                </span>
                            </div>
                    </div>  
                    <div className="p-box-col">
                      <label for="" className="p-label">
                        5%
                      </label>
                    </div>
                    <div className="p-box-col">
                      <label for="" className="p-label">
                       2%
                      </label>
                    </div>
                    <div className="p-box-col">
                      <label for="" className="p-label">
                        Included
                      </label>
                    </div>
              </div> */}
              <div className="p-box-row gray-col">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Payment Terms
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Instant CNCT Payments
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="incon" />
                      <div className="popout">
                        <smal>
                          {" "}
                          INSTANT CNCT is our immediate payment method.
                          Electronic payment is taken from the buyer at the
                          point that the project is agreed. Once the project is
                          completed, both parties confirm that it has been
                          completed correctly and payment is automatically sent
                          to the seller's account (minus the transaction fee).
                          Buyer's receive a sales invoice from CNCT and seller's
                          receive a purchase invoice from CNCT
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Special Terms
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="incon" />
                      <div className="popout">
                        <smal>
                          {" "}
                          Sometimes, on larger projects, we appreciate that
                          extended payment terms are needed. Special Terms
                          allows the buyers and seller to agree their own
                          payment terms and even upload terms and conditions of
                          business. When the project is agreed, we take a
                          payment equal to the seller's transaction fee from the
                          buyer (akin to a deposit). When the project is
                          completed, we send a completion note to both the buyer
                          and seller and the seller invoices the buyer
                          independently and directly for the balance (agreed
                          project fee, mins transaction fee).
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row gray-col">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Partnerships
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Payments Powered via
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.stripe}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.stripe}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.stripe}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Data Hosting and Storage via
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img src={ImagePath.aws} className="p-circle" alt="CNCT" />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img src={ImagePath.aws} className="p-circle" alt="CNCT" />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img src={ImagePath.aws} className="p-circle" alt="CNCT" />
                  </label>
                </div>
              </div>
              {/* <div className="p-box-row gray-col">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Extras
                  </label>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
                <div className="p-box-col"></div>
              </div> */}
              <div className="p-box-row gray-col">
                <div className="p-box-col main-box">
                  <label for="" className="p-label">
                    Extras
                  </label>
                </div>
                <div className="under-box-row">
                  <div className="extra-box">
                    <div>
                      <div className="extra-box-row">
                        <div className="p-box-col under-box">
                          <label for="" className="p-label">
                            Service and Support
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            &nbsp;
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            &nbsp;
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            &nbsp;
                          </label>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div className="extra-box-row">
                        <div className="p-box-col under-box">
                          <label for="" className="p-label">
                            24/7 Email Support
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            <img
                              src={ImagePath.loaderCircle}
                              className="p-circle"
                              alt="CNCT"
                            />
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            <img
                              src={ImagePath.loaderCircle}
                              className="p-circle"
                              alt="CNCT"
                            />
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            <img
                              src={ImagePath.loaderCircle}
                              className="p-circle"
                              alt="CNCT"
                            />
                          </label>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div className="extra-box-row">
                        <div className="p-box-col under-box">
                          <label for="" className="p-label">
                            Dedicated Account Manager
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            &nbsp;
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            &nbsp;
                          </label>
                        </div>
                        <div className="p-box-col">
                          <label for="" className="p-label">
                            <img
                              src={ImagePath.loaderCircle}
                              className="p-circle"
                              alt="CNCT"
                            />
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <div className="under-box-row">
                  <div className="p-box-col under-box  glance-box">
                    <label for="" className="p-label glance-mb">
                      Service and Support
                    </label>
                    <label for="" className="p-label glance-mb">
                      24/7 Email Support
                    </label>
                    <label for="" className="p-label glance-mb">
                      Dedicated Account Manager
                    </label>
                  </div>
                  <div className="p-box-col  glance-box">
                    <label for="" className="p-label glance-mb">
                      hi
                    </label>
                    <label for="" className="p-label glance-mb">
                      <img
                        src={ImagePath.loaderCircle}
                        className="p-circle"
                        alt="CNCT"
                      />
                    </label>
                    <label for="" className="p-label glance-mb">
                      hi
                    </label>
                  </div>
                  <div className="p-box-col  glance-box">
                    <label for="" className="p-label glance-mb">
                      &nbsp;
                    </label>
                    <label for="" className="p-label glance-mb">
                      <img
                        src={ImagePath.loaderCircle}
                        className="p-circle"
                        alt="CNCT"
                      />
                    </label>
                    <label for="" className="p-label glance-mb">
                      &nbsp;
                    </label>
                  </div>
                  <div className="p-box-col  glance-box">
                    <label for="" className="p-label glance-mb">
                      &nbsp;
                    </label>
                    <label for="" className="p-label glance-mb">
                      <img
                        src={ImagePath.loaderCircle}
                        className="p-circle"
                        alt="CNCT"
                      />
                    </label>
                    <label for="" className="p-label glance-mb">
                      <img
                        src={ImagePath.loaderCircle}
                        className="p-circle"
                        alt="CNCT"
                      />
                    </label>
                  </div>
                </div> */}
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Monthly Industry Trend Reports
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="incon" />
                      <div className="popout">
                        <smal>
                          {" "}
                          Sometimes, on larger projects, we appreciate that
                          extended payment terms are needed. Special Terms
                          allows the buyers and seller to agree their own
                          payment terms and even upload terms and conditions of
                          business. When the project is agreed, we take a
                          payment equal to the seller's transaction fee from the
                          buyer (akin to a deposit). When the project is
                          completed, we send a completion note to both the buyer
                          and seller and the seller invoices the buyer
                          independently and directly for the balance (agreed
                          project fee, mins transaction fee).
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
              <div className="p-box-row">
                <div className="p-box-col w30">
                  <label for="" className="p-label">
                    Post Processor Exchange
                  </label>
                  <div className="position-relative ml-4">
                    <span className="popoup">
                      <img src={ImagePath.info} alt="incon" />
                      <div className="popout">
                        <smal>
                          {" "}
                          Gain access to a free online database of post
                          processors, digital twins and virtual machines, to
                          better enable remote programming. This is open source
                          and self-serve and we encourage our customers to
                          contribute to our community but sharing any safe post
                          processors that they have available.
                        </smal>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="p-box-col"></div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
                <div className="p-box-col">
                  <label for="" className="p-label">
                    <img
                      src={ImagePath.loaderCircle}
                      className="p-circle"
                      alt="CNCT"
                    />
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="mobile-price-slide">
            <ul className="list-unstyled price-slide">
              <li>
                <Col
                  xs="12"
                  sm="12"
                  md="12"
                  lg="12"
                  className="princing-item red"
                >
                  <div className="pricing-divider ">
                    <h5 className="">Starter</h5>
                    <h4 className="my-0 font-weight-normal mb-3">FREE</h4>
                    {/* <svg class='pricing-divider-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1' preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
                            <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
                      c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z' fill='#FFFFFF' opacity='0.6'></path>
                            <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
                      c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z' fill='#FFFFFF' opacity='0.6'></path>
                            <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
                      H42.401L43.415,98.342z' fill='#FFFFFF' opacity='0.7'></path>
                            <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
                      c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z' fill='#FFFFFF'></path>
                          </svg> */}
                  </div>
                  <CardBody className="bg-white mt-0 shadow">
                    <ul className="list-unstyled mb-5 position-relative">
                      <li>
                        <b>10</b> users included
                      </li>
                      <li>
                        <b>2 GB</b> of storage
                      </li>
                      <li>
                        <b>Free </b>Email support
                      </li>
                      <li>
                        <b>Help center access</b>
                      </li>
                    </ul>
                    <Button className="btn-round border-dark">Sign Up</Button>
                  </CardBody>
                </Col>
              </li>
              <li>
                <Col
                  xs="12"
                  sm="12"
                  md="12"
                  lg="12"
                  className="princing-item green"
                >
                  <div class="pricing-divider ">
                    <h5 class="text-light">Standard</h5>
                    <h4 class="my-0 text-light font-weight-normal mb-3">
                      <span class="h3">£</span> 19 <span class="h5">/mo</span>
                    </h4>
                    {/* <svg class='pricing-divider-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1' preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
                            <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
                      c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z' fill='#FFFFFF' opacity='0.6'></path>
                            <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
                      c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z' fill='#FFFFFF' opacity='0.6'></path>
                            <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
                      H42.401L43.415,98.342z' fill='#FFFFFF' opacity='0.7'></path>
                            <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
                      c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z' fill='#FFFFFF'></path>
                          </svg> */}
                  </div>

                  <CardBody className="bg-white mt-0 shadow">
                    <ul className="list-unstyled mb-5 position-relative">
                      <li>
                        <b>300</b> users included
                      </li>
                      <li>
                        <b>20 GB</b> of storage
                      </li>
                      <li>
                        <b>Free</b> Email support
                      </li>
                      <li>
                        <b>Help center access</b>
                      </li>
                    </ul>
                    <Button className="btn-round">Sign Up</Button>
                  </CardBody>
                </Col>
              </li>
              <li>
                <Col
                  xs="12"
                  sm="12"
                  md="12"
                  lg="12"
                  className="princing-item blue"
                >
                  <div className="pricing-divider ">
                    <h5 className="text-light">Enterprise</h5>
                    <h6 className="my-0 text-light font-weight-normal mb-3">
                      Sign-up for price
                    </h6>
                    {/* <svg class='pricing-divider-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1' preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
                            <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
                      c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z' fill='#FFFFFF' opacity='0.6'></path>
                            <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
                      c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z' fill='#FFFFFF' opacity='0.6'></path>
                            <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
                      H42.401L43.415,98.342z' fill='#FFFFFF' opacity='0.7'></path>
                            <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
                      c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z' fill='#FFFFFF'></path>
                          </svg> */}
                  </div>

                  <CardBody className="bg-white mt-0 shadow">
                    <ul className="list-unstyled mb-5 position-relative">
                      <li>
                        <b>100 </b>users included
                      </li>
                      <li>
                        <b>10 GB</b> of storage
                      </li>
                      <li>
                        <b>Free</b>Email support
                      </li>
                      <li>
                        <b>Help center access</b>
                      </li>
                    </ul>
                    <Button className="btn-round">Contact Us</Button>
                  </CardBody>
                </Col>
              </li>
            </ul>
          </div>
        </Container>
      </section>
    </React.Fragment>
  );
};

export default Pricing;
