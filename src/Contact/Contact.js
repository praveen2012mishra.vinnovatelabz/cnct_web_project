import React from 'react';
import { Container, Row, Col, Form, Input, FormGroup, Button, Label } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faEnvelope, faPhoneAlt, faAddressBook } from '@fortawesome/free-solid-svg-icons';

import {ImagePath} from './../ImagePath';

const Contact = () =>{
  return(
    <React.Fragment>
      
      <section className="inner-banner position-relative">
        <img src={ImagePath.contactBanner} alt="slider" className="w-100" />
        <div className="caption">
            <Container>
              <div className="text-center">
                <h1 className="font-weight-exlight">Contact <span className="text-green">Us</span></h1>                   
              </div>
            </Container>
        </div>
      </section>

      <section className="common-padd contact-us position-relative">
        <Container>
          <div className="d-flex justify-content-between align-items-center border-bottom pb-3 mb-4">
            <h2 className=" text-green" >Get in touch with us easily</h2>
            <span><img src={ImagePath.loaderCircle} className="img-fluid rotating" alt="loader" /></span>
          </div>
                <Row>
                    <Col md="6">
                        <div className="m-left">
                            <Row>
                                <Col md="6">
                                    <div className="adress">
                                        <div className="d-flex align-items-center">
                                            <h6 className="text-green"><FontAwesomeIcon icon={faAddressBook} /></h6>
                                            <h6 className="ml-1 text-black">Address</h6>
                                        </div>
                                        <p>Street 221B Baker Street, London, UK</p>
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div className="adress">
                                        <div className="d-flex align-items-center">
                                            <h6 className="text-green"><FontAwesomeIcon icon={faPhoneAlt} /></h6>
                                            <h6 className="ml-1 text-black">Give us a call</h6>
                                        </div>
                                        <div className="d-flex">
                                            <p className="text-black mr-1 font-weight-normal">Phone : </p>
                                            <p>+001-4356-643</p>
                                        </div>
                                        <div className="d-flex">
                                            <p className="text-black mr-1 font-weight-normal">Email :  </p>
                                            <p>contact@site.com</p>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <div className="map-container">
                                <iframe title="location" className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.424670498403!2d-0.1607443847990074!3d51.523770317327994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761acf33628211%3A0x445d7677a88322e1!2s221B%20Baker%20St%2C%20Marylebone%2C%20London%20NW1%206XE%2C%20UK!5e0!3m2!1sen!2sin!4v1593071741169!5m2!1sen!2sin" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                        </div>
                    </Col>
                    <Col md="6">
                        <div className="contact-wrap pl-md-3">
                            <h5 className="text-black mb-4">Enquiry Now</h5>
                            <Form className="d-block">
                                <FormGroup>
                                    <Label>Name</Label>
                                    <div className="form-input">
                                      <Input type="text" name="" id="" placeholder="Enter Name" />
                                      <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                                    </div>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Email</Label>
                                    <div className="form-input">
                                      <Input type="email" name="" id="" placeholder="Enter Email Id" />
                                      <span className="icon-wr"><FontAwesomeIcon icon={faEnvelope} /></span>
                                    </div>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Query</Label>
                                    <Input type="textarea" name="" id="" placeholder="Your Query" className="border-0" rows="6" />
                                </FormGroup>
                                <div className="form-group pt-2">
                                    <Button className="btn btn-dark" type="submit">Submit <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 ml-md-5 rotating" /></Button>
                                </div>
                            </Form>
                        </div>
                    </Col>
                </Row>
      </Container>
    </section>

    </React.Fragment>
  )
}

export default Contact;