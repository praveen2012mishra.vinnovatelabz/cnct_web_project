import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Header from './Navbar/Navbar';
import Footer from './Footer/Footer';

const isAuthenticated = () => {
    return (
        (localStorage.getItem('userId'))
    ) || false;
}

export default ({ children, ...rest }) => {
    return (
        <Route {...rest}
            render={() => isAuthenticated() ? <Redirect to="/" /> : (children) } />
    )
}
