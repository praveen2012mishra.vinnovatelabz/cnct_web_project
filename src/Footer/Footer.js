import React from 'react';
import { NavLink } from 'react-router-dom';
import { Container, Row, Col, Form, Input, FormGroup, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faUser, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import {ImagePath} from './../ImagePath';

const Footer = () =>{
  return(
    <React.Fragment>
      {!localStorage.getItem('userId') ? 
      (<>
      <section className="cnct-today common-padd">
      <Container>
        <div className="text-center mb-4">
            <h2 className="text-uppercase font-weight-light">CNCT <strong className="font-weight-bold">Today</strong> </h2>
            <h6 className="font-weight-light">Explore our pricing plans, understand how we can solve your critical business challenges or sign-up for free today</h6>
        </div>
        <div className="black-round">
            <NavLink exact to="/Solution">Solutions </NavLink>
            <NavLink exact to="/Pricing">Pricing </NavLink>
            <NavLink exact to="/Contact">Contact Us</NavLink>
            <NavLink exact to="/Signup" className="navlink">Sign Up for Free </NavLink>
        </div>
      </Container>
    </section>
    
    <footer className="footer text-center text-md-left">
      <Container>
          <Row>
              <Col md="4">
                  <div className="f-form pr-lg-5">
                    <h5 className="text-white text-uppercase">Contact Us</h5>
                    <Form>
                    <div className="f-input">
                      <Input type="text" name="" id="" placeholder="Enter Name" />
                      <span className="icon-wr"><FontAwesomeIcon icon={faUser} /></span>
                    </div>
                    <div className="f-input">
                      <Input type="Email" name="" id="" placeholder="Enter Email" />
                      <span className="icon-wr"><FontAwesomeIcon icon={faEnvelope} /></span>
                    </div>
                    <FormGroup>
                      <Input type="textarea" className="bg-transparent" name="text" id="" placeholder="Your Query" />
                    </FormGroup>
                    <Button className="btn green-btn text-white" type="submit">Submit <img src={ImagePath.loader} alt="loader" className="img-fluid ml-4 rotating" /></Button>
                    </Form>
                  </div>
              </Col>
              <Col md="4">
                  <div className="f-link">
                    <h5 className="text-white text-uppercase">Useful Links</h5>
                    <ul className="list-unstyled">
                      <li><NavLink to="/Solution">Solutions</NavLink></li>
                      <li><NavLink to="/Pricing">Pricing</NavLink></li>
                      <li><NavLink to="/Contact">Contact Us</NavLink></li>
                      <li><NavLink to="">Terms and Conditions</NavLink></li>
                      <li><NavLink to=""> Privacy Policy</NavLink></li>
                    </ul>
                  </div>
              </Col>
              <Col md="4">
                  <div className="f-logo text-white">
                    <NavLink to="/" className="navbar-brand mb-2" ><img src={ImagePath.logob} className="img-fluid" alt="logo" /></NavLink>
                    <p>Copyright 2020 CNCT Manufacturing Ltd</p>
                    <ul className="list-unstyled d-flex nav mb-0">
                      <li><NavLink to=""><FontAwesomeIcon icon={faTwitter} /></NavLink></li>
                      <li><NavLink to=""><FontAwesomeIcon icon={faLinkedin} /></NavLink></li>
                    </ul>
                  </div>
              </Col>
          </Row>
      </Container>
    </footer>

      </>) : <></>
      }
          </React.Fragment>
  )
}

export default Footer;