import banner from './assets/images/banner1.jpg';
import bannerIcon from './assets/images/banner-icon.png';
import icon1 from './assets/images/supply1.png';
import icon1Right from './assets/images/supply2.png';
import icon2 from './assets/images/demand1.png';
import icon2Right from './assets/images/demand2.png';
import ficon1 from './assets/images/human_factor.png';
import ficon2 from './assets/images/machine_factor.png';
import ficon3 from './assets/images/other_factor.png';
import work1 from './assets/images/uploadPic.png';
import work2 from './assets/images/postJob.png';
import work3 from './assets/images/intelligent.png';
import work4 from './assets/images/negotiating.png';
import work5 from './assets/images/completion.png';
import logob from './assets/images/logob.png';
import loader from './assets/images/loader.png';
import logo from './assets/images/logo.png';
import pricingBanner from './assets/images/pricing-banner.jpg';
import chimni from './assets/images/chimni.png';
import loaderCircle from './assets/images/loader-circle.png';
import stripe from './assets/images/stripe.png';
import aws from './assets/images/aws.png';
import info from './assets/images/info.png';
import contactBanner from './assets/images/contact-banner.jpg';
import signupBanner from './assets/images/contact-banner.jpg';
import pay1 from './assets/images/pay1.png';
import pay2 from './assets/images/pay2.png';
import pay3 from './assets/images/pay3.png';
import pay4 from './assets/images/pay4.png';
import bigLoader from './assets/images/big-loader.png';
import solutionBanner from './assets/images/solution-banner.jpg';
import sicon1 from './assets/images/fill_diary.png';
import sicon2 from './assets/images/get_paid.png';
import sicon3 from './assets/images/work_home.png';
import sicon4 from './assets/images/revenue.png';
import sicon5 from './assets/images/sicon5.png';
import sicon6 from './assets/images/sicon6.png';
import sicon7 from './assets/images/sicon7.png';
import sicon8 from './assets/images/sicon8.png';
import sicon9 from './assets/images/sicon9.png';
import sicon10 from './assets/images/sicon10.png';
import sicon11 from './assets/images/sicon11.png';
import sicon12 from './assets/images/sicon12.png';
import sicon13 from './assets/images/sicon13.png';
import sicon14 from './assets/images/sicon14.png';
import sicon15 from './assets/images/sicon15.png';
import sicon16 from './assets/images/sicon16.png';
import camb from './assets/images/camb.png';
import gear from './assets/images/gear_rotate.png';
import compass from './assets/images/compass.png';
import scale from './assets/images/scale.png';
import blacklogo from './assets/images/black-logo.png';
import loaderred from './assets/images/red-loader.png';
import Redcircle from './assets/images/loader-redcircle.png';
import Check from './assets/images/check.png';
import Smloader from './assets/images/sm-loader.png';
import Closered from './assets/images/timesred.png';
import avtar1 from './assets/images/avtar-1.jpg';
import avtar2 from './assets/images/avtar-2.jpg';


export const ImagePath ={
  banner:banner,
  bannerIcon:bannerIcon,
  icon1:icon1,
  icon1Right:icon1Right,
  icon2:icon2,
  icon2Right:icon2Right,
  ficon1:ficon1,
  ficon2:ficon2,
  ficon3:ficon3,
  work1:work1,
  work2:work2,
  work3:work3,
  work4:work4,
  work5:work5,
  logob:logob,
  loader:loader,
  logo:logo,
  pricingBanner:pricingBanner,
  chimni:chimni,
  loaderCircle:loaderCircle,
  stripe:stripe,
  aws:aws,
  info:info,
  contactBanner:contactBanner,
  signupBanner:signupBanner,
  pay1:pay1,
  pay2:pay2,
  pay3:pay3,
  pay4:pay4,
  solutionBanner:solutionBanner,
  sicon1:sicon1,
  sicon2:sicon2,
  sicon3:sicon3,
  sicon4:sicon4,
  sicon5:sicon5,
  sicon6:sicon6,
  sicon7:sicon7,
  sicon8:sicon8,
  sicon9:sicon9,
  sicon10:sicon10,
  sicon11:sicon11,
  sicon12:sicon12,
  sicon13:sicon13,
  sicon14:sicon14,
  sicon15:sicon15,
  sicon16:sicon16,
  camb:camb,
  gear:gear,
  compass:compass,
  scale:scale,
  blacklogo:blacklogo,
  loaderred:loaderred,
  Redcircle:Redcircle,
  Check:Check,
  Smloader:Smloader,
  Closered:Closered,
  bigLoader:bigLoader,
  avtar1:avtar1,
  avtar2:avtar2,
}