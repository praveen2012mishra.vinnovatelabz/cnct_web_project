import React from "react";
import { Route, Redirect, withRouter } from "react-router-dom";
import Header from './Navbar/Navbar';
import Footer from './Footer/Footer';


const isAuthenticated = () => {
  return (
    (localStorage.getItem("userId")) ||
    false
  );
};

const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route {...rest}
      render={() => isAuthenticated() ?  (children) : (<Redirect to="/Home" />)} />
  );
};

export default withRouter(PrivateRoute)
