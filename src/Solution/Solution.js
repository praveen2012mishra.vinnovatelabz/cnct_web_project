import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import {ImagePath} from './../ImagePath';

const Solution = () =>{
  return(
    <React.Fragment>
			<section className="inner-banner position-relative">
        <img src={ImagePath.solutionBanner} alt="slider" className="w-100" />
        <div className="caption">
            <Container>
                <div className="text-center">
                    <h1 className="font-weight-exlight">Use Cases </h1>
                    <h2 className="font-weight-light">CNCT solves critical problems for organisations of every size and users in every job role</h2>
                    <div id="freelancing" style={{display:'block'}}>&nbsp;</div>
                </div>
            </Container>
        </div>
    	</section>

		<section className=" common-padd freelance pb-0">
        <Container>
            <div className="text-md-left text-center pb-4">
              <h2><strong>Freelancing</strong> <span className="d-block font-weight-exlight mt-2">Self-Employed Freelance CNC Programmer</span></h2>
            </div>
            <Row>
            <Col lg="4">
                <figure className="gear-wrap">
                    <img src={ImagePath.gear} className="img-fluid" alt="gear" />
                </figure>
            </Col>
              <Col lg="8" md="12">
                <div className="cat-wrap">
                  <Row>
                    <Col sm="6">
                        <div className="position-relative text-center">
                            <figure><img src={ImagePath.sicon1} className="img-fluid" alt="icon" /></figure>
                            <div className="content">
                                <h5>Fill Your Diary </h5>
                                <p>Need some additional work to sit alongside your regular clients? Use CNCT to open up a wealth of opportunities without spending hours cold-calling or traipsing through LinkedIn</p>
                            </div>
                        </div>
                    </Col>
                    <Col sm="6">
                        <div className="position-relative text-center">
                            <figure><img src={ImagePath.sicon2} className="img-fluid" alt="icon" /></figure>
                            <div className="content">
                                <h5>Get Paid on Time </h5>
                                <p>Cash is king, but unfortunately the smallest businesses are often pushed to the back of the queue for payment. We take payment for your work up-front and pay you instantly upon completion.</p>
                            </div>
                        </div>
                    </Col>
					<Col sm="6">
                        <div className="position-relative text-center">
                            <figure><img src={ImagePath.sicon3} className="img-fluid" alt="icon" /></figure>
                            <div className="content">
                                <h5>Work from Home, Seamlessly</h5>
                                <p>CNCT facilitates true home working for the CNC industry. Vendors upload jobs with CAD, post processors, virtual machines, tooling and fixturing, to take the guess-work out of programming and replicate a physical site visit</p>
                            </div>
                        </div>
                    </Col>
                    <Col sm="6">
                        <div className="position-relative text-center">
                            <figure><img src={ImagePath.sicon4} className="img-fluid" alt="icon" /></figure>
                            <div className="content">
                                <h5>Revenue with Convenience </h5>
                                <p>Prefer on site work? Set your radius and keep your opportunities local. Alternatively, opt for remote work and use our in-built tools to share data and collaborate with your clients</p>
                                <div id="project_management" style={{display:'block'}}>&nbsp;</div>
                            </div>
                        </div>                  
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
        </Container>
    </section>

		<section className="pr-manage mbg">
        <div className="common-padd bg-overlay pb-0">
            <Container>
                <div className="text-md-left text-center pb-4 text-white">
                    <h2><strong>Project Management </strong>
                        <span className="d-block font-weight-exlight mt-2">Production Manager at a Large Manufacturer</span>
                    </h2>
                </div>
                <Row className="flex-md-row-reverse overflow-hidden">
                    <Col lg="4" className="animatedParent">
                        <figure className="gear-wrap animated bounceInRight animate-2">
                            <img src={ImagePath.compass} className="img-fluid" alt="gear" />
                        </figure>
                    </Col>
                    <Col md="12" lg="8">
                        <div className="cat-wrap">
                            <Row className="mx-md-0">
                                <Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon5} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>Control Your Critical Path</h5>
                                            <p>Need resource to fill a critical function in your project – we provide a
                                                mechanism to locate the exact capabilities you need to keep your Gantt
                                                Chart on track</p>
                                        </div>
                                    </div>
                                </Col>
                                <Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon6} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>Agile and Easy</h5>
                                            <p>Post jobs in seconds, get prompt responses and confirm work. No need to
                                                chase your purchasing team to setup new suppliers in your purchasing
                                                system, it can all go via CNCT. </p>
                                        </div>
                                    </div>
                                </Col>
                                <Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon7} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>Specialist Machine Capacity</h5>
                                            <p>We understand that special jobs need special machines, sometimes beyond
                                                what you have on-site. Instead of phoning around your network, trying to
                                                locate a subcontractor with the right plant list, simply post your
                                                project and our algorithms will match it to all the facilities with the
                                                right capability </p>
                                        </div>
                                    </div>
                                </Col>
																<Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon8} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>Upload, Save Duplication</h5>
                                            <p>Instead of endless back-and-forth with your subcontractors, CNCT allows
                                                you to upload all relevant resources for your subcontractors. Save
                                                yourself numerous conversations qualifying out suppliers who aren’t
                                                capable of fulfilling the job, by creating one job and letting our
                                                algorithms appraise their suitability</p>
                                                <div id="sales" style={{display:'block'}}>&nbsp;</div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </Container>
            
        </div>
    </section>

		<section className="common-padd sales pb-0">
        <Container>
            <div className="text-md-left text-center pb-4">
                <h2><strong>Sales </strong> 
                    <span className="d-block font-weight-exlight mt-2">Subcontract Machine Shop Owner</span>
                </h2>
            </div>
            <Row className="overflow-hidden">
                <Col lg="4" className="animatedParent">
                    <figure className="gear-wrap animated bounceInLeft animate-2">
                        <img src={ImagePath.camb} className="img-fluid" alt="camera" />
                    </figure>
                </Col>
                <Col md="12" lg="8">
                    <div className="cat-wrap">
                        <Row>
                            <Col sm="6">
                                <div className="position-relative text-center">
                                    <figure><img src={ImagePath.sicon9} className="img-fluid" alt="icon" /></figure>
                                    <div className="content">
                                        <h5>Eliminate Idle Time  </h5>
                                        <p>Whether it is paying operators when you don’t have productive work, or keeping up with your finance payments for your machines when the spindles aren’t turning, idle time is perhaps the biggest challenge facing subcontractors. Fixing this problem was the reason CNCT Manufacturing was conceived.</p>
                                    </div>
                                </div>
                            </Col>
                            <Col sm="6">
                                <div className="position-relative text-center">
                                    <figure><img src={ImagePath.sicon10} className="img-fluid" alt="icon" /></figure>
                                    <div className="content">
                                        <h5>Filling your Pipeline </h5>
                                        <p>Forecasting a quiet fortnight after a large order? Use CNCT as and when suits and find work when you need it, rather than being at the mercy of your existing customers and their peaks and troughs.</p>
                                    </div>
                                </div>
                            </Col>
                            <Col sm="6">
                                <div className="position-relative text-center">
                                    <figure><img src={ImagePath.sicon11} className="img-fluid" alt="icon" /></figure>
                                    <div className="content">
                                        <h5>More Diversity, Less Risk</h5>
                                        <p>Relying on work from a small pool of local customers? Use CNCT to open you open to national or global opportunities. Get paid instantly upon completion and reduce your reliance on existing clients, who may control a large proportion of your workload, squeeze your margins or regularly pay late.</p>
                                    </div>
                                </div>
                            </Col>
                            <Col sm="6">
                                <div className="position-relative text-center">
                                    <figure><img src={ImagePath.sicon12} className="img-fluid" alt="icon" /></figure>
                                    <div className="content">
                                        <h5>Fairer Competition </h5>
                                        <p>Instead of relying on scraps from businesses further up the chain, allow our matching process to open your business up to opportunities with larger organisations, well beyond your existing network. If your work is good, our dynamic rating system will showcase your credentials.</p>
                                        <div id="procurement" style={{display:'block'}}>&nbsp;</div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
        </Container>
    </section>


    <section className="Procurement sol-bg2">
        <div className="common-padd overlay pb-0">
            <Container>
                <div className="text-md-left text-center pb-4 text-white">
                    <h2><strong>Procurement </strong> <span className="d-block font-weight-exlight mt-2">Head of Procurement at an Enterprise OEM</span></h2>
                </div>
                <Row className="flex-md-row-reverse overflow-hidden">
                    <Col lg="8" md="12">
                        <div className="cat-wrap">
                            <Row className="mx-md-0">
                                <Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon13} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>Slash Outsourcing Costs</h5>
                                            <p>Targeted with reducing costs by a set %? Upload your projects and let capable suppliers bid for the work. By seeing the entire relevant market, you can understand equilibrium rate and ensure you do not overpay.</p>
                                        </div>
                                    </div>
                                </Col>
                                <Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon14} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>And Dependency </h5>
                                            <p>Ensure major production projects are safeguarded by opening up to a wider array of trusted, capable suppliers, to safeguard against the failure of your small existing tier-1 pool.  </p>
                                        </div>
                                    </div>
                                </Col>
                                <Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon15} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>Security and Visibility</h5>
                                            <p>Ensure you only use suppliers that meet your own strict criteria. Stipulate requisite certification and protect your projects with non-disclosure agreements. Our entire infrastructure is based on the security of the Amazon-Web-Services cloud for hosting and data transfer for peace of mind.
                                            </p>
                                        </div>
                                    </div>
                                </Col>
                                <Col sm="6">
                                    <div className="position-relative text-center">
                                        <figure><img src={ImagePath.sicon16} className="img-fluid" alt="icon" /></figure>
                                        <div className="content">
                                            <h5>More Supply Options, Less Vendor Accounts </h5>
                                            <p>Seamlessly access a wider source of suppliers to reduce your costs, many of whom are likely already tier-2 into your organisation. However, eliminate the need for multiple supplier accounts, by processing it all via CNCT.</p>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    <Col lg="4" className="animatedParent">
                        <figure className="gear-wrap animated bounceInRight animate-2">
                            <img src={ImagePath.scale} className="img-fluid" alt="scale" />
                        </figure>
                    </Col>
                </Row>
            </Container>
        </div>
    </section>

    

    </React.Fragment>
  )
}

export default Solution;