import React,{ useEffect } from 'react';
import Header from './Navbar/Navbar';
import Home from './Home/Home';
import Solution from './Solution/Solution';
import Contact from './Contact/Contact';
import Signup from './Signup/Signup';
import Login from './Login/Login';
import Pricing from './Pricing/Pricing';
import Footer from './Footer/Footer';
import TabContentTwo from './SignupTab/TabContentTwo';
import Dashboard from './Dashboard/Dashboard';
import NewMatchedOpportunities from './Dashboard/NewMatchedOpportunities';
import Balance from './Dashboard/Balance';
import ChangePassword from './Dashboard/ChangePassword';
import ConnectLinkedin from './Dashboard/ConnectLinkedin';
import Notification from './Dashboard/Notification';
import TransactionHistory from './Dashboard/TransactionHistory';
import EditProfile from './Dashboard/EditProfile';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './assets/css/responsive.css';
import { Switch, Route,withRouter } from 'react-router-dom';
import Capabilities from './Dashboard/Capabilities';
import OutsourcingMyJobs from './Dashboard/OutsourcingMyJobs';
import ApplyJob from './Dashboard/ApplyJob';
import AuthRoute from './AuthRoute';
import PrivateRoute from './PrivateRoute';
import SubconductingApplyJob from './Dashboard/SubconductingApplyJob';
import SubconductingMyJobs from './Dashboard/SubconductingMyJobs';

const App = (props) => {

  useEffect(() => {
    const unlisten = props.history.listen(() => {
      window.scrollTo(0, 0);
    });
    return () => {
      unlisten();
    }
  }, []);

  if (window.performance) {
    if (performance.navigation.type == 1) {
      if (localStorage.getItem('payment') !== '4') {
        localStorage.setItem('pageType', '');
        localStorage.setItem('payment', '');
        localStorage.setItem('tempUserId', '');
      }else{
        
      }
      
    } else {
      
    }
  }

  return (
    <React.Fragment>
      <Header/>
        <AuthRoute path="/Home">
          <Home />
        </AuthRoute>
        <AuthRoute path="/Signup">
          <Signup />
        </AuthRoute>
        <AuthRoute path="/Login">
          <Login />
        </AuthRoute>
        <PrivateRoute path="/" exact>
          <Dashboard />
        </PrivateRoute>
        <PrivateRoute path="/NewMatchedOpportunities" exact>
          <NewMatchedOpportunities />
        </PrivateRoute>
        <PrivateRoute path="/Balance" exact>
          <Balance />
        </PrivateRoute>
        <PrivateRoute path="/ChangePassword" exact>
          <ChangePassword />
        </PrivateRoute>
        <PrivateRoute path="/ConnectLinkedin" exact>
          <ConnectLinkedin />
        </PrivateRoute>
        <PrivateRoute path="/Notification" exact>
          <Notification />
        </PrivateRoute>
        <PrivateRoute path="/TransactionHistory" exact>
          <TransactionHistory />
        </PrivateRoute>
        <PrivateRoute path="/EditProfile" exact>
          <EditProfile />
        </PrivateRoute>
        <PrivateRoute path="/Capabilities" exact>
          <Capabilities />
        </PrivateRoute>
        <PrivateRoute path="/OutsourcingMyJobs" exact>
          <OutsourcingMyJobs />
        </PrivateRoute>
        <PrivateRoute path="/ApplyJob/:Id" exact>
          <ApplyJob />
        </PrivateRoute>
        <Route path="/Contact" exact>
          <Contact />
        </Route>
        <Route path="/Solution">
          <Solution />
        </Route>
        <Route path="/Pricing">
          <Pricing />
        </Route>
        <PrivateRoute path="/SubconductingApplyJob/:Id" exact>
          <SubconductingApplyJob />
        </PrivateRoute>
        <PrivateRoute path="/SubconductingMyJobs" exact>
          <SubconductingMyJobs />
        </PrivateRoute>
        
        
<Footer />

    </React.Fragment>
  )
}

export default withRouter(App);
